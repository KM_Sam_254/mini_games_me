﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public abstract class GameController : MonoBehaviour
{
    public TextMeshProUGUI currentStageText, totalStageText;
    
    protected int stageCount = 0;
    protected int totalStageCount = 10;
    protected int rightAnswerCount = 0;
    protected int difficulty = 0;
    protected bool isCheckingResult = false;
    protected bool hasGameStarted = false;   // 전체게임
    protected bool hasStageStarted = false;  // 게임 안에서 스테이지
    protected int chooseDifficultyPanelCount = 3;

    [Space]
    public GameObject btnCheckResult;
    public GameObject btnDifficulty;
    public GameObject btnNext;
    public GameObject btnHome;

    public virtual void SetGame()
    {
        /// 디버그 모드일 경우 주로 에디터에서 씬 편집후 실행해보는 경우가 많으므로
        /// 씬에서 바로 실행하면 Gamemager에 OnSceneLoaded 가 호출되지 않으므로
        /// GetGameManagerDebug 를 호출해준다
        if (GameManager.ISDEBUG) GetGameMangerDebug();
        SetBtnsListener();
        RegistGameStartProcedure();
    }

    protected void GetGameMangerDebug()
    {
        GameManager.Instance.OnSceneLoadedDebug();
    }

    IEnumerator SetGameCoroutine()
    {
        yield return new WaitForEndOfFrame();
        SetGame();
    }

    protected abstract void RegistGameStartProcedure();

    /// <summary>
    /// Client game start by this function calling from GameManager
    /// </summary>
    public abstract void StartGame();

    protected void FinishGame()
    {
        GameManager.Instance.OnGameFinished();
        ShowGameResult();
    }

    protected void ShowGameResult()
    {
        //GameResultPanel.ShowResultCanvas(new ResultForm.GameResultData(totalStageCount, rightAnswerCount, GameManager.Instance.GetGameInfoTime(SceneMgr.Instance.GetActiveSceneName())), () => {
        //    SceneMgr.Instance.GoMenu();
        //});
    }

    public int GetDifficulty()
    {
        return this.difficulty;
    }

    protected void SetTotalStageCount(int count)
    {
        this.totalStageCount = count;
        if (totalStageText != null)
        {
            totalStageText.text = "" + totalStageCount;
        }
        else Debug.LogWarning("Stage Count Text is NULL");
    }

    protected void SetCurrentStageCount(int count)
    {
        if (currentStageText != null)
        {
            currentStageText.text = "" + stageCount;
        }
        else Debug.LogWarning("Current Stage Text is NULL");
    }
    
    /// <summary>
    /// Default setting for the buttons. Override if want to change
    /// </summary>
    protected virtual void SetBtnsListener()
    {
        // Home button
        btnHome.GetComponent<Button>().onClick.AddListener(() => {
            //SceneMgr.Instance.GoGroupMenu();
            HomeBtnClicked();
        });

        // Set Difficulty choose button
        btnDifficulty.GetComponent<DifficultyButton>().AddListener(DifficultyBtnClicked);

        // Check result button
        btnCheckResult.GetComponent<Button>().onClick.AddListener(CheckResultClicked);
        btnCheckResult.GetComponent<Button>().interactable = false;

        // Next button
        btnNext.GetComponent<ButtonNext>().AddListener(NextBtnClicked);
    }

    protected virtual void DifficultyBtnClicked(int difficulty)
    {
        SetDifficulty(difficulty);
    }

    protected virtual void CheckResultClicked()
    {
        if (isCheckingResult || !hasStageStarted) return;

        Verify();

        //difficultyBtn.SetActive(true);
        isCheckingResult = true;
        hasStageStarted = false;

        btnNext.GetComponent<ButtonNext>().SetInteractable(true);
        btnCheckResult.GetComponent<Button>().interactable = false;
    }

    protected virtual void NextBtnClicked()
    {
        if (hasStageStarted) return;

        Next();

        //difficultyBtn.SetActive(false);
        isCheckingResult = false;
        hasStageStarted = true;

        btnNext.GetComponent<ButtonNext>().SetInteractable(false);
        btnCheckResult.GetComponent<Button>().interactable = true;
    }

    public abstract void HomeBtnClicked();
    protected abstract void Next();
    protected abstract void Verify();
    protected abstract void SetDifficulty(int difficulty);
    public virtual void SetNextInteractable(bool b) { btnNext.GetComponent<ButtonNext>().SetInteractable(b); }
    public virtual void SetVerifyInteractable(bool b) { btnCheckResult.GetComponent<Button>().interactable = b; }
    public virtual void SetDiffcultyBtnInteractable(bool b) { btnDifficulty.GetComponent<Button>().interactable = b; }
}
