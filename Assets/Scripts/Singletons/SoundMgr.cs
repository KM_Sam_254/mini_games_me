﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundMgr : MonoBehaviour
{
    private static SoundMgr instance = null;
    bool clickSoundMute = false;
    public bool ClickSoundMute { get { return clickSoundMute; } set { clickSoundMute = value; } }

    public enum Sounds {
              SOUND_CLICK
            , SOUND_BGM_1
            , SOUND_BGM_2
            , SOUND_BGM_3
             };

    
    private AudioClip[] audioClip;
    private AudioSource audioSource, bgmAudioSource;
    bool start = true;
    bool intro_Scene = false;

    public static SoundMgr Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(SoundMgr)) as SoundMgr;
                if (!instance) Create();
            }
            return instance;
        }
    }

    public static void Create()
    {
        if(instance==null)
        {
            var obj = new GameObject("SoundMgr");
            instance = obj.AddComponent<SoundMgr>();
        }
    }

    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        InitSound();
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
        //Application.runInBackground = true;

        // FOR DEBUG
        if(GameManager.ISDEBUG)
        {
            Stop_BGM();
            ClickSoundMute = true;
        }
    }

    //private void Start()
    //{
    //    ClickSoundMute = false;
    //}

    void InitSound()
    {
        audioSource = gameObject.AddComponent<AudioSource>();
        bgmAudioSource = gameObject.AddComponent<AudioSource>();

        int length = (int)Sounds.SOUND_BGM_3 + 1;
        //Debug.Log("Sound Length : " + length);
        audioClip = new AudioClip[length];
        audioClip[0] = Resources.Load("Sound/ClickSound") as AudioClip;
        audioClip[1] = Resources.Load("Sound/Bgm1") as AudioClip;
        audioClip[2] = Resources.Load("Sound/Bgm2") as AudioClip;
        audioClip[3] = Resources.Load("Sound/Bgm3") as AudioClip;

        Play_BGM();
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0) && !ClickSoundMute)
        {
            PlaySoundOnce(Sounds.SOUND_CLICK);
        }
    }

    public void PlaySoundOnce(Sounds sounds)
    {
        int soundIdx = (int)sounds;
        if (soundIdx >= audioClip.Length) Debug.LogWarning("Sound index exception");
        if (!audioSource.isPlaying && !audioSource.mute && soundIdx < audioClip.Length)
        {
            audioSource.clip = audioClip[soundIdx];
            audioSource.loop = false;
            audioSource.Play();
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //if (start)
        //{
        //    start = false;
        //    return;
        //}
        //else if(scene.buildIndex >= SceneManager.sceneCountInBuildSettings - 3 || scene.buildIndex == 1)
        //{
        //    if (intro_Scene)
        //    {
        //        intro_Scene = false;
        //        audioSource.Stop();
        //        Play_BGM(scene.buildIndex);
        //        return;
        //    }
        //}
        //else
        //{
        //    audioSource.Stop();
        //    intro_Scene = true;
        //}

        //Play_BGM(scene.buildIndex);
    }

    public void Play_BGM()
    {
        bgmAudioSource.loop = true;
        int soundIdx = (int)Sounds.SOUND_BGM_1;
        bgmAudioSource.clip = audioClip[soundIdx];
        bgmAudioSource.Play();
    }

    public void Stop_BGM()
    {
        if(bgmAudioSource!=null)
        {
            bgmAudioSource.Stop();
        }
    }

    public void Mute_BGM()
    {
        if (bgmAudioSource != null)
        {
            bgmAudioSource.mute = true;
        }
    }

    public void Resume_BGM()
    {
        if (bgmAudioSource != null)
        {
            bgmAudioSource.mute = false;
        }
    }
}