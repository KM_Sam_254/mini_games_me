﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;

    public const int EASY = 0, NORMAL = 1, HARD = 2, SUPERHARD = 3;
    public const int SCREEN_WIDTH = 1920, SCREEN_HEIGHT = 1080;  // 게임 기준 해상도 FHD
    public const int HORIZONTAL = 1, VERTICAL = 2;
    public const int NONE = 0;

    GameTimer _timer;

    public static bool ISDEBUG
    {
        get
        {
            bool isDebug = false;
#if _DEBUG || UNITY_EDITOR
            isDebug = true;
#endif
            return isDebug;
        }
    }

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(GameManager)) as GameManager;
                if (!instance) Create();
            }
            return instance;
        }
    }

    public static void Create()
    {
        if (instance == null)
        {
            var obj = new GameObject("GameManager");
            instance = obj.AddComponent<GameManager>();
        }
    }

    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

        SceneManager.sceneLoaded += OnSceneLoaded;

        DontDestroyOnLoad(gameObject);

        Init();
    }

    void Init()
    {
        // Create singleton instances
        SceneMgr.Create();
        SoundMgr.Create();
        UserInput.Create();
        UserGameData.Create();
        UICanvas.Create();

        _timer = new GameTimer();
    }

    private void Update()
    {
        SetScreenOrientation();
        _timer.UpdateTimer();
    }

    public static SystemLanguage GetSystemLanguage()
    {
        SystemLanguage sl = Application.systemLanguage;
        return sl;
    }

    private void SetScreenOrientation()
    {
        if (Screen.orientation == ScreenOrientation.LandscapeLeft && Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            Screen.orientation = ScreenOrientation.LandscapeRight;
        }
        else if (Screen.orientation == ScreenOrientation.LandscapeRight && Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
        }
    }

    GameController currentController;
    IKTouchRecognizer currentTouchRecognizer;
    List<System.Action> Tasks = new List<System.Action>();

    public void RegisterGameStartProcedure(System.Action procedure)
    {
        Tasks.Add(procedure);
    }

    int popIndex = 0;
    bool PopTask()
    {
        Debug.Log("PopTask Count : " + Tasks.Count + "  index : " + popIndex);
        if (Tasks == null || Tasks.Count < 1 || popIndex > Tasks.Count-1) return false;
        Tasks[popIndex++]();
        //Tasks.RemoveAt(0);
        return true;
    }

    public void NextProcedure()
    {
        if (!PopTask()) UserGameStart();
    }

    // Call from client
    public void OnClientGameReady()
    {
        Debug.Log("GameManager OnClientGameReady");
        
        NextProcedure();
    }

    /// <summary>
    /// Editor 모드에서 각 씬에서 테스트할때 OnSceneLoaded 를 강제로 호출해 주기 위함
    /// </summary>
    public void OnSceneLoadedDebug()
    {
        Debug.Log("GameManager OnSceneLoadedDebug");

        OnSceneLoaded(SceneManager.GetActiveScene(), LoadSceneMode.Additive);
    }

    // Call from client
    public void OnGameFinished()
    {
        // Stop Game timer
        _timer.StopTimer();

        // Save game data
        UserGameData.Instance.SaveUserGameData(_timer.GetTimeLapsedString());

        if (currentTouchRecognizer != null)
            UserInput.Instance.UnRegistTouchRecognizer(currentTouchRecognizer);
    }

    // Call from client
    public void UserGameStart()
    {
        Debug.Log("User Game Start");
        // Start Game timer
        _timer.StartTimer();

        // Start Game
        if(currentController!=null) currentController.StartGame();
    }

    // Recognition for scene change
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("GameManager OnSceneLoaded : " + scene.name);
        GameObject gc = GameObject.Find("GameController");
        if(gc==null)
        {
            Debug.Log("GameController does not exist");
            return;
        }

        currentController = gc.GetComponent<GameController>();
        if (currentController == null) Debug.LogError("Can not find game controller");
        //else currentController.SetGame();

        if (gc)
        {
            // User touch
            currentTouchRecognizer = gc.GetComponent<IKTouchRecognizer>();
            if (currentTouchRecognizer != null)
            {
                UserInput.Instance.RegistTouchRecognizer(currentTouchRecognizer);
            }
        }
    }
}
