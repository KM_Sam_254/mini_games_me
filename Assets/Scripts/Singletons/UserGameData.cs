﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UserGameData : MonoBehaviour
{
    static UserGameData instance;

    public static UserGameData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(UserGameData)) as UserGameData;
                if (!instance) Create();
            }
            return instance;
        }
    }

    public static void Create()
    {
        if(instance==null)
        {
            var obj = new GameObject("UserGameData");
            instance = obj.AddComponent<UserGameData>();
        }
    }


    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// 게임완료에 걸린 시간과 날짜 저장
    /// </summary>
    public void SaveUserGameData(string lapsedTime)
    {
        string sceneName = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetString(sceneName + "_time", lapsedTime); // 게임완료에 걸린 시간
        string date = System.DateTime.Now.Date.ToString();   // 완료날짜?
        PlayerPrefs.SetString(sceneName + "_date", date);
    }

    public string GetGameInfoTime(string sceneName)
    {
        return PlayerPrefs.GetString(sceneName + "_time", "00 : 00");
    }

    public string GetGameInfoDate(string sceneName)
    {
        string date = System.DateTime.Now.Date.ToString();
        return PlayerPrefs.GetString(sceneName + "_date", date);
    }
    
    // 유저가 게임할때 시작했던 난이도를 저장했다가 다음에 다시 진입했을때 사용
    public void SetUserDifficultyPref(int difficulty)
    {
        string key = SceneManager.GetActiveScene().name + "_difficulty";
        PlayerPrefs.SetString(key, "" + difficulty);
    }

    public int GetUserGameDifficultyPref()
    {
        string key = SceneManager.GetActiveScene().name + "_difficulty";
        return ParseUtil.ParseStringToInt(PlayerPrefs.GetString(key, "0"));
    }
}
