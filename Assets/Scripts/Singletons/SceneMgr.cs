﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMgr : MonoBehaviour {
    
    private static SceneMgr instance;
    static bool isTransitioning = false;
    public int GameMenuIndex = 1;  // 어느 학년 메뉴를 열 것인지 인덱스
    int groupdCategory = -1;
    public int GroupCategory { get { return groupdCategory; } set { groupdCategory = value; } }

    public enum SceneIndex {
              SCENE_PORTAL   // 0
            , SCENE_MENU   
            , SCENE_FIRST
            , SCENE_SECOND
    };

    public static SceneMgr Instance
    {
        get
        {
            instance = FindObjectOfType(typeof(SceneMgr)) as SceneMgr;
            if (instance==null) Create();
            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null) instance = this;

        else if (instance != this) Destroy(gameObject);
        
        DontDestroyOnLoad(gameObject);
    }

    public static void Create()
    {
        if (instance == null)
        {
            var obj = new GameObject("SceneMgr");
            instance = obj.AddComponent<SceneMgr>();
        }
    }

    public void GoGroupMenu()
    {
        TransitionToScene("MenuGroupEducation");
    }

    public void GoPersonalMenu()
    {
        TransitionToScene("MenuPersonal");
    }

    public void ChangeScene(string sceneName)
    {
        TransitionToScene(sceneName);
    }

    public string GetActiveSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public static void TransitionToScene(string sceneName)
    {
        if (isTransitioning) return;
        Instance.StartCoroutine(Instance.Transition(sceneName));
    }
    
    protected IEnumerator Transition(string newSceneName)
    {
        yield return null;

        Debug.Log("Transition : " + newSceneName);
        isTransitioning = true;
        //PersistentDataManager.SaveAllData();

        // Scene transition animation
        //yield return StartCoroutine(ScreenFader.FadeSceneOut(ScreenFader.FadeType.Loading));

        // --- DATA
        //PersistentDataManager.ClearPersisters();
        //PersistentDataManager.LoadAllData();
        
        yield return SceneManager.LoadSceneAsync(newSceneName);

        // Scene transition animation
        //yield return StartCoroutine(ScreenFader.FadeSceneIn());

        isTransitioning = false;
    }
}
