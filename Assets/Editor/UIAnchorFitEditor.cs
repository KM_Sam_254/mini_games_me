﻿
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UIAnchorFitter))]   // The component which gotta be the Target. The button below would be attached to the target component
public class UIAnchorFitEditor : Editor
{
    float minX, minY, maxX, maxY, width, height;
    int SCREEN_WIDTH = 1920, SCREEN_HEIGHT = 1080; // 기준 해상도 : 기준해상도와 게임뷰 사이즈가 다르면 작동하지 않음
    int PARENT_WIDTH = 1920, PARENT_HEIGHT = 1080;
    GameObject gameObject;
    Vector3 rectPosition;
    UIAnchorFitter fitter;
    RectTransform rt;

    public override void OnInspectorGUI()
    {        
        base.OnInspectorGUI();

        if (GUILayout.Button("Set Anchor Fit this only"))
        {
            Debug.Log("Set Anchor Fit this only");

            GetTarget();

            if (!CheckScreenResolution())
            {
                return;
            }

            if (rt==null)
            {
                Debug.LogWarning("RectTransform NULL");
                return;
            }
            
            width = rt.rect.width;
            height = rt.rect.height;

            Debug.Log("Name : " + rt.name + "  pos : " + rt.position + "   Width : " + width + "   height: " + height);

            if (width < 1f || height < 1f)
            {
                Debug.LogWarning("UIAnchorFitter : Can not set the anchor Fit. Check the current anchor bro!");
                return;
            }

            CheckParent(rt);

            SetAnchor(rt);
        }

        if(GUILayout.Button("Set Anchor Fit For Children"))
        {
            Debug.Log("Set Anchor Fit For Children");

            GetTarget();

            if (!CheckScreenResolution())
            {
                return;
            }

            if (gameObject==null)
            {
                Debug.Log("UIAnchorFit : GameObject NULL");
                return;
            }

            Transform[] allChildren = gameObject.GetComponentsInChildren<Transform>();

            foreach (Transform child in allChildren)
            {
                PARENT_WIDTH = SCREEN_WIDTH;
                PARENT_HEIGHT = SCREEN_HEIGHT;

                rt = child.GetComponent<RectTransform>();
                if (rt == null)
                {
                    Debug.Log("SetAnchorAllChildren : RectTransform NULL");
                    continue;
                }

                width = rt.rect.width;
                height = rt.rect.height;

                Debug.Log("Child : " + rt.name + "  pos : " + rt.position + "   Width : " + width + "   height: " + height);

                if (width < 1f || height < 1f)
                {
                    Debug.LogWarning("UIAnchorFitter : Can not set the anchor Fit. Check the current anchor bro!");
                    return;
                }

                rectPosition = rt.position;

                if(CheckParent(rt))
                    SetAnchor(rt);
            }

        }
    }

    void GetTarget()
    {
        SCREEN_WIDTH = GameManager.SCREEN_WIDTH;
        SCREEN_HEIGHT = GameManager.SCREEN_HEIGHT;
        fitter = target as UIAnchorFitter;
        rt = fitter.GetRectTransform();
        gameObject = fitter.GetGameObject();
        rectPosition = rt.position;
    }

    bool CheckScreenResolution()
    {
        Vector2Int screenSize = ScreenSize.GetEditorGameViewSize();
        if (screenSize.x != SCREEN_WIDTH || screenSize.y != SCREEN_HEIGHT)
        {
            Debug.LogWarning("Screen Resolution is not 1920 by 1080");
            return false;
        }
        return true;
    }

    bool CheckParent(RectTransform childRectTransform)
    {
        // 부모가 캔버스 하위의 오브젝트인 경우
        Transform parentTransform = childRectTransform.parent;
        if(parentTransform==null)
        {
            Debug.Log("No Parent UI");
            return false;
        }
        GameObject parentObj = parentTransform.gameObject;
        if (parentObj != null)
        {
            RectTransform parent = parentObj.GetComponent<RectTransform>();
            
            Debug.Log("CheckParent Name : " + parent.name + "  Width : " + parent.rect.width + "  Height : " + parent.rect.height);
            
            if (parent.rect.width==0 || parent.rect.height==0)
            {
                Debug.LogWarning("Check CanvasScaler and Reference resolution");
                
            }
            else if (parent.rect.width < SCREEN_WIDTH || parent.rect.height < SCREEN_HEIGHT)
            {

                if (parent == null)
                {
                    Debug.Log("Parent NULL");
                   
                }
                else
                {
                    PARENT_WIDTH = (int)parent.rect.width;
                    PARENT_HEIGHT = (int)parent.rect.height;
                    rectPosition = childRectTransform.localPosition;
                    rectPosition += new Vector3(PARENT_WIDTH / 2, PARENT_HEIGHT / 2);

                    Debug.Log(childRectTransform.name + " is child of " + parent.name);
                }

            }
        }

        return true;
    }

    void SetAnchor(RectTransform rt)
    {
        minX = (rectPosition.x - (width / 2)) / PARENT_WIDTH;
        minY = (rectPosition.y - (height / 2)) / PARENT_HEIGHT;
        maxX = (rectPosition.x + (width / 2)) / PARENT_WIDTH;
        maxY = (rectPosition.y + (height / 2)) / PARENT_HEIGHT;

        rt.anchorMin = new Vector2(minX, minY);
        rt.anchorMax = new Vector2(maxX, maxY);
        rt.offsetMin = Vector2.zero;
        rt.offsetMax = Vector2.zero;
    }
}
