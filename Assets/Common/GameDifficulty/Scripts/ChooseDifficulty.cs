﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventSystem))]
public class ChooseDifficulty : MonoBehaviour
{
    public GameObject[] startObjects;
    Button easy, normal, hard;
    public int Difficulty { get; set; }
    Action<int> callback;
    public GameObject btnsPanel_4, btnsPanel_3, btnsPanel_2, viewTutoBtn;
    public Sprite[] tutoStatusSprite;

    // 튜토리얼보기 선택버튼 체크박스이미지
    public Image tutoBtnImg;

    private void Start()
    {
        GameObject eventSystem = GameObject.Find("EventSystem");
        if (eventSystem == null)
        {
            eventSystem = Instantiate(Resources.Load("Prefabs/EventSystem") as GameObject);
            eventSystem.name = "EventSystem";
        }
        
        if(CheckTutorial())
        {
            // 튜토리얼보기 체크박스 상태갱신
            viewTutoBtn.SetActive(true);
            SetViewTutoStatusCheckboxImg();
        }
        else
        {
            viewTutoBtn.SetActive(false);
        }
    }

    void StartGame()
    {
        if (startObjects != null && startObjects.Length > 0)
        {
            for (int i = 0; i < startObjects.Length; i++)
            {
                if (startObjects[i] != null)
                {
                    startObjects[i].SetActive(false);
                    startObjects[i].SetActive(true);
                }
            }
        }

        if (callback != null) callback(Difficulty);
        Destroy(gameObject);
    }

    public void OnEasy()
    {
        Difficulty = GameManager.EASY;
        StartGame();
    }

    public void OnNormal()
    {
        Difficulty = GameManager.NORMAL;
        StartGame();
    }

    public void OnHard()
    {
        Difficulty = GameManager.HARD;
        StartGame();
    }

    public void OnSuperHard()
    {
        Difficulty = GameManager.SUPERHARD;
        StartGame();
    }

    void ResetBtncolor()
    {

    }

    public void SetBtnsCount(int count)
    {
        if (count == 2)
        {
            btnsPanel_4.SetActive(false);
            btnsPanel_3.SetActive(false);
            btnsPanel_2.SetActive(true);
        }
        else if(count == 4)
        {
            btnsPanel_4.SetActive(true);
            btnsPanel_3.SetActive(false);
            btnsPanel_2.SetActive(false);
        }
        else   // 버튼3개 기본
        {
            btnsPanel_4.SetActive(false);
            btnsPanel_3.SetActive(true);
            btnsPanel_2.SetActive(false);
        }
    }

    public void ShowTutorialBtn(bool b)
    {
        if (b) viewTutoBtn.SetActive(true);
        else viewTutoBtn.SetActive(false);
    }

    public void SetCallback(Action<int> cb)
    {
        callback = cb;
    }

    public void ViewTutoBtnClicked()
    {
        SetViewTuto();
    }

    public static bool CheckTutorial()
    {
        GameObject o = Resources.Load("Prefabs/tutorial/" + TutorialSetPanel.TUTORIAL_PREFIX + SceneMgr.Instance.GetActiveSceneName()) as GameObject;
        if (o == null)
        {
            Debug.Log("Tutorial prefab doesnt exist");
            return false;
        }
        return true;
    }

    void SetViewTutoStatusCheckboxImg()
    {
        if (IsViewTutorial(SceneMgr.Instance.GetActiveSceneName())) tutoBtnImg.sprite = tutoStatusSprite[1];
        else tutoBtnImg.sprite = tutoStatusSprite[0];
    }

    void SetViewTuto()
    {
        if (IsViewTutorial(SceneMgr.Instance.GetActiveSceneName()))
            PlayerPrefs.SetInt(GetViewTutoPrefKey(SceneMgr.Instance.GetActiveSceneName()), 0);
        else PlayerPrefs.SetInt(GetViewTutoPrefKey(SceneMgr.Instance.GetActiveSceneName()), 1);

        SetViewTutoStatusCheckboxImg();
    }

    static public bool IsViewTutorial(string sceneName)
    {
        if (PlayerPrefs.GetInt(GetViewTutoPrefKey(sceneName), 1) > 0) return true;
        return false;
    }

    static public string GetViewTutoPrefKey(string sceneName)
    {
        return "view_tuto_" + sceneName;
    }

    public void SetViewTutorialBtnVisible(bool isVisible)
    {
        if (isVisible) viewTutoBtn.SetActive(true);
        else viewTutoBtn.SetActive(false);
    }
}
