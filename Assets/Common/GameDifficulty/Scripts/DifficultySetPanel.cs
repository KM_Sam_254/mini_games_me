﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DifficultySetPanel : MonoBehaviour
{
	public static ChooseDifficulty ShowDiffcultyCanvas(Action<int> cb)
    {
        ChooseDifficulty cd = GetDifficulty();
        cd.SetCallback(cb);
        return cd;
	}

    public static ChooseDifficulty ShowDiffcultyCanvas(Action<int> cb, int btnCount)
    {
        ChooseDifficulty cd = GetDifficulty();
        cd.SetBtnsCount(btnCount);
        cd.SetCallback(cb);
        return cd;
    }

    public static ChooseDifficulty ShowDiffcultyCanvas(Action<int> cb, int btnCount, bool showTutoBtn)
    {
        ChooseDifficulty cd = GetDifficulty();
        cd.SetBtnsCount(btnCount);
        cd.SetCallback(cb);
        cd.ShowTutorialBtn(showTutoBtn);
        return cd;
    }

    static ChooseDifficulty GetDifficulty()
    {
        GameObject go = Instantiate(Resources.Load("Prefabs/DifficultyChooseCanvas") as GameObject);
        return go.GetComponent<ChooseDifficulty>();
    }
}
