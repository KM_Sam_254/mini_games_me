﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyButton : MonoBehaviour
{
    public GameObject[] difficulties;
    bool difficultyShow = false;
    int difficulty = 1;  // default value of Difficulty
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }
    Action<int> _callback;
    public Image num;
    public Sprite[] numSprites;

    void Start ()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(OnClick);

        for (int i = 0; i < difficulties.Length; i++)
        {
            difficulties[i].GetComponent<Button>().GetComponent<ButtonEventDelegator>().AddListener((btnName) => {
                difficulty = ParseUtil.ParseStringToInt(btnName, 1);
                HideDifficultyBtns();
                Debug.Log("Difficulty Choose : " + difficulty);
                if (_callback != null) _callback(difficulty);
                num.sprite = numSprites[difficulty-1];
            });
        }
	}

    void OnClick()
    {
        if (!difficultyShow)
        {
            ShowDifficultyBtns();
            //if(_callback!= null) _callback(-1);
        }
        else HideDifficultyBtns();
    }

    void ShowDifficultyBtns()
    {
        for (int i = 0; i < difficulties.Length; i++)
        {
            difficulties[i].SetActive(true);
        }

        difficultyShow = true;
    }

    public void HideDifficultyBtns()
    {
        for (int i = 0; i < difficulties.Length; i++)
        {
            difficulties[i].SetActive(false);
        }

        difficultyShow = false;
    }

    public void AddListener(Action<int> action)
    {
        _callback = action;
    }
}
