﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonNext : MonoBehaviour
{
    public GameObject btnNext, btnStart;
    Action _calllback; 

	void Start ()
    {
        btnNext.GetComponent<Button>().onClick.AddListener(()=> {
            if (_calllback != null) _calllback();
        });

        btnStart.GetComponent<Button>().onClick.AddListener(() => {
            if (_calllback != null) _calllback();
            btnStart.SetActive(false);
            btnNext.SetActive(true);
        });
    }
	
    public void AddListener(Action action)
    {
        _calllback = action;
    }

    public void SetInteractable(bool b)
    {
        if (btnNext != null) btnNext.GetComponent<Button>().interactable = b;
        if (btnStart != null) btnStart.GetComponent<Button>().interactable = b;
    }
}
