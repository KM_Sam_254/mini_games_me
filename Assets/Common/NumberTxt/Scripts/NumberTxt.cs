﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberTxt : MonoBehaviour
{
    public Sprite[] sprites;
    Image numImg;
    int num = 0;

	void Start ()
    {
        numImg = GetComponent<Image>();
        numImg.sprite = sprites[num];
	}

    public void SetNumber(int num)
    {
        if (numImg == null) return;
        if (num < 10)
        {
            this.num = num;
            numImg.sprite = sprites[num];
        }
        else Debug.LogWarning("Number can not exceed 10");
    }
}
