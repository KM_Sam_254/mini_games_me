﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    Action<int> cb;
    public GameObject[] panelsEasy, panelsNormal, panelsHard;
    GameObject[] panel;
    int currentPanelIndex = 0;
    int difficulty = 0;
    bool shouldStartGame = false;
    GameObject nextBtn;
    Color32 backGroundTintColor = new Color32(0, 0, 0, 150);
    public const int TUTORIAL_END = 1000;

    private void Start()
    {
        nextBtn = transform.Find("NextButton").gameObject;
        if (!nextBtn) Debug.LogError("NextButton on tutorial is NULL");
    }

    void SetBackgroundColor()
    {
        InvertedMaskImage[] imm = transform.GetComponentsInChildren<InvertedMaskImage>();
        foreach(InvertedMaskImage i in imm)
        {
            i.color = backGroundTintColor;
        }
    }

    public void SetCallback(Action<int> c)
    {
        cb = c;
    }

    public void StartTutorial(int diff)
    {
        difficulty = diff;
        if (difficulty == GameManager.EASY)
        {
            panel = panelsEasy;
        }
        else if (difficulty == GameManager.NORMAL)
        {
            panel = panelsNormal;
        }
        else if (difficulty == GameManager.HARD)
        {
            panel = panelsHard;
        }

        if (panelsEasy == null || panelsEasy.Length < 1) Debug.LogError("Tutorial panel Easy NULL");
        if (panelsNormal == null || panelsNormal.Length < 1) Debug.LogError("Tutorial panel Normal NULL");
        if (panelsHard == null || panelsHard.Length < 1) Debug.LogError("Tutorial panel Hard NULL");

        if (panel == null || panel.Length < 1)
        {
            FinishTutorial();
            return;
        }
        ShowNextTutoPanel();
    }

    public void OnNextBtn()
    {
        ShowNextTutoPanel();
    }

    void ShowNextTutoPanel()
    {
        if(currentPanelIndex > panel.Length - 1)
        {
            ShowStartImage();
            return;
        }

        if (panel.Length < 1) return;

        InvalidatePanels();

        for (int i = 0; i < panel.Length; i++)
        {
            if (i == currentPanelIndex)
            {
                panel[i].SetActive(true);
                if(cb != null) cb(currentPanelIndex);
                break;
            }
        }

        currentPanelIndex++;

        SetBackgroundColor();
    }

    void InvalidatePanels()
    {
        for (int i = 0; i < panelsEasy.Length; i++)
        {
            panelsEasy[i].SetActive(false);
        }

        for (int i = 0; i < panelsNormal.Length; i++)
        {
            panelsNormal[i].SetActive(false);
        }

        for (int i = 0; i < panelsHard.Length; i++)
        {
            panelsHard[i].SetActive(false);
        }
    }

    // 튜토리얼을 내용 다 보여준후 게임 시작된다는 표시 해주기 위한 이미지 보여주기
    void ShowStartImage()
    {
        InvalidatePanels();
        nextBtn.SetActive(false);

        //  add start panel
        GameObject so = Resources.Load("Prefabs/tutorial/startCanvas") as GameObject;
        if (so == null)
        {
            Debug.LogError("Tutorial start game image is null");
            shouldStartGame = true;
            if (cb != null) cb(TUTORIAL_END);
            return;
        }

        GameObject startCanvas = Instantiate(so);
        
        startCanvas.transform.SetParent(transform);
        startCanvas.name = "startPanel";
        startCanvas.transform.Find("startPanel").GetComponent<Image>().color = backGroundTintColor;

        shouldStartGame = true;
        if (cb != null) cb(currentPanelIndex);
    }

    private void Update()
    {
        if(GetInput.InputDown() && shouldStartGame) FinishTutorial();
    }

    void FinishTutorial()
    {
        if (cb != null) cb(TUTORIAL_END);
        Destroy(gameObject);
    }

    public int GetTutorialCount()
    {
        return panel.Length;
    }
}
