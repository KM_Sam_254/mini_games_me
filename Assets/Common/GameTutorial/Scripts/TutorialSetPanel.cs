﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TutorialSetPanel : MonoBehaviour
{
    public static string TUTORIAL_PREFIX = "Tutorial_";

    public static void ShowTutorial(int difficulty, Action<int> cb)
    {
        string sceneName = SceneMgr.Instance.GetActiveSceneName();
        if (!ChooseDifficulty.IsViewTutorial(sceneName) || !ChooseDifficulty.CheckTutorial())
        {
            cb(Tutorial.TUTORIAL_END);
            return ;
        }
        
        GameObject go = Instantiate(Resources.Load("Prefabs/tutorial/" + TUTORIAL_PREFIX + sceneName) as GameObject);
        Tutorial t = go.GetComponent<Tutorial>();

        t.SetCallback(cb);
        t.StartTutorial(difficulty);
    }
}
