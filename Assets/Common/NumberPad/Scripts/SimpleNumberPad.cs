﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class SimpleNumberPad : MonoBehaviour
{
    public int STRING_LENGTH_LIMIT = 2;   // Limit of string length
    public int VALUE_LIMIT = 9999999;     // Limit of input value
    public int DEFUALT_VALUE = 0;         // Default key value

    string keyString = string.Empty;
    Action<int> callBack;
    public const int KEY_ENTER = 100;
    public const int KEY_CLEAR = 101;
    
    public TextMeshProUGUI textMeshProUGUI;

    public Button[] keys;

    private void Start()
    {
        SetKeys();
        SetDefaultKeyString(DEFUALT_VALUE);
        UpdateText();
    }

    void SetKeys()
    {
        for (int i = 0; i < keys.Length; i++)
        {
            keys[i].GetComponent<ButtonEventDelegator>().AddListener((key) => {
                KeyPressed(key);
            });
        }
    }

    public void KeyPressed(string key)
    {
        if ((string.IsNullOrEmpty(keyString) || keyString.Equals("0")) && key.Equals("0")) return;
        else if (keyString.Equals("0") && !key.Equals("0")) keyString = string.Empty;

        //Debug.Log("Key Pressed : " + key);

        if (key.Equals("ok"))
        {
            if (callBack != null) callBack(KEY_ENTER);
        }
        else if(key.Equals("clear"))
        {
            keyString = string.Empty;

            if (callBack != null) callBack(KEY_CLEAR);
        }
        else
        {
            int k = ParseUtil.ParseStringToInt(key);
            //Debug.Log("KeyPressedString : " + key + "  keyInt : " + k);
            if (k < 10)
            {
                keyString += key;
                if (keyString.Length > STRING_LENGTH_LIMIT)
                {
                    keyString = key;
                }

                int value = ParseUtil.ParseStringToInt(keyString);
                if (value > VALUE_LIMIT) keyString = "" + VALUE_LIMIT;

                if (callBack != null) callBack(value);
            }
        }

        UpdateText();
    }

    public int GetCurrentKeyValueInt()
    {
        return ParseUtil.ParseStringToInt(keyString);
    }

    public string GetCurrentKeyValueString()
    {
        return keyString;
    }

    public void SetStringLengthLimit(int limit)
    {
        STRING_LENGTH_LIMIT = limit;
    }

    public void SetCallback(Action<int> cb)
    {
        callBack = cb;
    }

    public void SetDefaultKeyString(int value)
    {
        //if (string.IsNullOrEmpty(keyString) && value == 0) return;
        if (value == 0) keyString = string.Empty;
        else keyString = "" + value;
    }

    public void SetTextmeshPro(TextMeshProUGUI tmPro)
    {
        this.textMeshProUGUI = tmPro;
    }

    void UpdateText()
    {
        if (textMeshProUGUI != null) textMeshProUGUI.text = keyString;
    }

    public void SetValueLimit(int limit)
    {
        this.VALUE_LIMIT = limit;
    }
}
