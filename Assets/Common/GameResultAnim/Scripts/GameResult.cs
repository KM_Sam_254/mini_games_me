﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameResult : MonoBehaviour
{
    static GameResult sInstance;
    
    public delegate void OnResultComplete();
    public OnResultComplete listener, resultDele;
    readonly int CORRECT=1;
    ParticleSystem particle;
    private float PARTICLE_SCALE = 35f;
    private float ANIM_UP_SCALEFROM = 0.5f;
    private static bool isShowingResult = false;
    public GameObject correctPanel, wrongPanel;
    public CanvasGroup blackFader;
    GameObject resultPanel;
    public Image correctImg, wrongImg;
    Image resultImg;
    Animator animator;
    Material starAnimParticleMaterial;
    public Texture animTexture;
    public UIParticleSystem uiAnimParticleSystem;
    public ParticleSystem particleSystem;

    public static GameResult Instance
    {
        get
        {
            if (sInstance == null) Create();
            return sInstance;
        }
        set
        {
            sInstance = value;
        }
    }

    static void Create()
    {
        Instance = Instantiate(Resources.Load("Prefabs/GameResultAnimCanvas") as GameObject).GetComponent<GameResult>();
    }

    public static void ShowCorrectAnimation(OnResultComplete callback)
    {
        if (isShowingResult) return;
        Instance.ShowCorrectResult(callback);
    }

    public static void ShowWrongAnimation(OnResultComplete callback)
    {
        if (isShowingResult) return;
        Instance.ShowWrongResult(callback);
    }

    public static void FinishAnimation()
    {
        Instance.Finish();
    }
    
    public void ShowCorrectResult(OnResultComplete callback)
    {
        if (isShowingResult) return;
        listener = callback;
        this.resultDele = callback;
        correctPanel.SetActive(true);
        wrongPanel.SetActive(false);
        resultImg = correctImg;
        resultPanel = correctPanel;
        ShowAnswerAnim(CORRECT);
    }

    public void ShowWrongResult(OnResultComplete callback)
    {
        if (isShowingResult) return;
        listener = callback;
        this.resultDele = callback;
        wrongPanel.SetActive(true);
        correctPanel.SetActive(false);
        resultImg = wrongImg;
        resultPanel = wrongPanel;
        ShowAnswerAnim(-1);
    }

    public void ShowAnswerAnim(int status)
    {
        StartCoroutine(ScreenFadeIn(1f, blackFader));

        // Sound
        string answerSpriteName = string.Empty, answerSoundName = string.Empty;
        if (status==CORRECT) answerSoundName = "right_answer_cheer";
        else answerSoundName = "Not_Answer";
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load("Sound/"+answerSoundName) as AudioClip;
        audioSource.Play();

        // Material
        
        starAnimParticleMaterial = new Material(Shader.Find("Particles/Additive"));
        starAnimParticleMaterial.mainTexture = animTexture;
        uiAnimParticleSystem.material = starAnimParticleMaterial;

        // particle speed
        var PSmain = particleSystem.main;
        PSmain.simulationSpeed = 3f;

        ScaleUp();

        isShowingResult = true;
    }

    public void OnAnimComplete()
    {
        // Start Animation
        animator = resultPanel.GetComponentInChildren<Animator>();
        float delayTime = 3f;
        if (animator != null)
        {
            animator.SetTrigger("animate");
            delayTime = animator.GetCurrentAnimatorStateInfo(0).length + animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        }
        else Debug.LogWarning("Anmator NULL");

        // Destroy object
        StartCoroutine(DestroyObjectWithDelay(delayTime));
    }

    private void ScaleUp()
    {
        if (resultImg == null) return;
        iTween.ScaleFrom(resultImg.gameObject, iTween.Hash("scale", new Vector3(ANIM_UP_SCALEFROM, ANIM_UP_SCALEFROM, 0f),
            "easeType", "easeOutElastic", "time", 1.8f, "onComplete", "OnAnimComplete", "onCompleteTarget", gameObject));
    }

    IEnumerator DestroyObjectWithDelay(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        Finish();
    }

    void Finish()
    {
        if (listener != null)
        {
            listener();
            listener -= resultDele;
        }

        isShowingResult = false;

        GameObject.Destroy(gameObject);
    }

    protected bool m_IsFading = false;
    public float fadeDuration = 1f;
    protected IEnumerator ScreenFadeIn(float finalAlpha, CanvasGroup canvasGroup)
    {
        m_IsFading = true;
        canvasGroup.blocksRaycasts = true;
        float fadeSpeed = Mathf.Abs(canvasGroup.alpha - finalAlpha) / fadeDuration;
        while (!Mathf.Approximately(canvasGroup.alpha, finalAlpha))
        {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, finalAlpha,
                fadeSpeed * Time.deltaTime);
            yield return null;
        }
        canvasGroup.alpha = finalAlpha;
        m_IsFading = false;
        canvasGroup.blocksRaycasts = false;
    }
}
