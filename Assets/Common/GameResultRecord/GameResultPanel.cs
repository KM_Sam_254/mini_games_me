﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameResultPanel : MonoBehaviour
{
    public static void ShowResultCanvas(ResultForm.GameResultData data, Action cb)
    {
        GameObject go = Instantiate(Resources.Load("Prefabs/GameResultCanvas") as GameObject);
        ResultForm rf = go.GetComponent<ResultForm>();
        rf.ShowInfo(data);
        rf.AddButtonListener(cb);
    }
}
