﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultForm : MonoBehaviour {

    public Text totalQuestion;
    public Text totalTime;
    public Text rightAnswer;
    public Text rightAnswerRate;
    public Text achievement;
    private Action callback;

    public void ShowInfo(GameResultData data)
    {
        float rateOfRightAnswer = (float)data.rightAnswer / (float)data.totalQuestion;
        rateOfRightAnswer = Mathf.Round(rateOfRightAnswer * 100f) / 100f;   // 소숫점 3번째자리에서 반올림
        totalQuestion.text = "" + data.totalQuestion;
        totalTime.text = data.totalTime;
        rightAnswer.text = "" + data.rightAnswer;

        rateOfRightAnswer *= 100;

        rightAnswerRate.text = rateOfRightAnswer + "%";

        if (rateOfRightAnswer > 89)
            achievement.text = "A";
        else if (rateOfRightAnswer > 79 && rateOfRightAnswer < 90)
            achievement.text = "B";
        else if (rateOfRightAnswer > 69 && rateOfRightAnswer < 80)
            achievement.text = "C";
        else if (rateOfRightAnswer > 59 && rateOfRightAnswer < 70)
            achievement.text = "D";
        else achievement.text = "F";
    }

    public struct GameResultData
    {
        public int totalQuestion, rightAnswer;
        public string totalTime;
        public GameResultData(int totalQueston, int rightAnswer, string totalTime)
        {
            this.totalQuestion = totalQueston;
            this.rightAnswer = rightAnswer;
            this.totalTime = totalTime;
        }
    }

    public void AddButtonListener(Action cb)
    {
        this.callback = cb;
    }

    public void OKBtnClicked()
    {
        if (this.callback != null) callback();
    }
}
