﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorUtil
{
    public static Vector2 GetVector2(Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }

    /// <summary>
    /// Returns Vector3 with the z value of 0
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public static Vector3 GetVector3WithoutZ(Vector3 pos)
    {
        return new Vector3(pos.x, pos.y, 0);
    }

    public static Vector2 GetVector2Position(GameObject go)
    {
        return new Vector2(go.transform.position.x, go.transform.position.y);
    }

    public static bool IsApproximatePosition(Vector2 pos1, Vector2 pos2)
    {
        float d = Mathf.Abs(Vector2.Distance(pos1, pos2));
        if (d < 0.05f)
        {
            return true;
        }
        return false;
    }

    public static bool IsPositionInRange(Vector2 objPos, Vector2 targetPos, float range)
    {
        float d = Mathf.Abs(Vector2.Distance(objPos, targetPos));
        if (d <= range)
        {
            return true;
        }
        return false;
    }
}
