﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
   DialogManager.Instance.ShowDialog(new DialogData(DialogType.Confirm, "Title", "Message", (bool result) =>{});
   DialogManager.Instance.ShowDialog(new DialogData(DialogType.Toast, "Message", 2f, delegate (bool result) {);
*/

public enum DialogType
{
	Alert,
	Confirm,
	Toast
}

public sealed class DialogManager
{
    List<DialogData> dialogQueue;
    Dictionary<DialogType, DialogController> _dialogMap;
	DialogController dialogController;
    GameObject dialogCanvas;
    private float disappearTime = 2;    // For toast message disappear

    private static DialogManager instance;

	public static DialogManager Instance
	{
		get
		{
            if (instance == null) instance = new DialogManager();
			return instance;
		}
	}
	
    private DialogManager()
    {
		dialogQueue = new List<DialogData>();
		//_dialogMap = new Dictionary<DialogType, DialogController>();
    }
    

	public void Prepare()
	{
        dialogCanvas =  GameObject.Instantiate(Resources.Load("Prefabs/DialogCanvas") as GameObject);
        dialogController = dialogCanvas.GetComponent<DialogController>();
    }

    // 일반 다이얼로그 : 확인버튼을 눌러야 사라진다
    public void ShowDialog(DialogData data)
	{
        if(!IsShowing())
        {
            Prepare();
            dialogController.SetDialogData(data, () => {
                ShowNext();
            });
        }
        
        dialogQueue.Add(data);		
    }

    /// <summary>
    /// Delay 를 준 후에 Dialog 띄우는 함수
    /// 주의 : 이 함수는 Dialog 띄우기 전에 전에 있던 Dialog 를 Clear 함
    /// </summary>
    /// <param name="data"></param>
    /// <param name="delay"></param>
    /// <param name="instance"></param>
    public void ShowDialogWithDelay(DialogData data, float delay, MonoBehaviour instance)
    {
        instance.StartCoroutine(DialogWithDelay(data, delay));
    }

    IEnumerator DialogWithDelay(DialogData data, float delay)
    {
        ClearDialog();
        yield return new WaitForSeconds(delay);
        ShowDialog(data);
    }

    public void ShowNext()
    {
        if(dialogQueue.Count>0) dialogQueue.RemoveAt(0);

        if (dialogQueue.Count>0)
        {
            dialogController.SetDialogData(dialogQueue[0], () => {
                ShowNext();
            });
        }
        else ClearDialog();
    }

    public void ClearDialog()
    {
        GameObject.Destroy(dialogCanvas);
        dialogQueue.Clear();
        dialogController = null;
    }

    bool IsShowing()
    {
        if (dialogController != null && dialogQueue.Count > 0) return true;
        else return false;
    }
}
