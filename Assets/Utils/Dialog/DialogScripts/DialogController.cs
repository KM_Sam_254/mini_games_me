﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogController : MonoBehaviour
{
    private DialogData dialogData;
	public GameObject alertWindow, confirmWindow, toastWindow;
    public delegate void OnDisable();
    private OnDisable disableListener;
    

    public void SetDialogData(DialogData data, OnDisable listener)
    {
        Button btnOk;
        disableListener = listener;
        dialogData = data;

        alertWindow.SetActive(false);
        confirmWindow.SetActive(false);
        toastWindow.SetActive(false);

        switch (data.Type)
        {
            case DialogType.Alert:
                alertWindow.SetActive(true);
                GameObject bgAlert = alertWindow.transform.Find("BgAlert").gameObject;
                bgAlert.transform.Find("AlertTitle").GetComponent<Text>().text = dialogData.title;
                bgAlert.transform.Find("AlertMessage").GetComponent<Text>().text = dialogData.message;
                btnOk = bgAlert.transform.Find("ButtonOK").GetComponent<Button>();
                btnOk.onClick.RemoveAllListeners();
                btnOk.onClick.AddListener(() => {
                    if (data.resultCallback != null) data.resultCallback(true);
                    StartCoroutine(DestroyWindow(0));
                });
                break;
            case DialogType.Confirm:
                confirmWindow.SetActive(true);
                GameObject bgConfirm = confirmWindow.transform.Find("BgConfirm").gameObject;
                bgConfirm.transform.Find("ConfirmTitle").GetComponent<Text>().text = dialogData.title;
                bgConfirm.transform.Find("ConfirmMessage").GetComponent<Text>().text = dialogData.message;
                btnOk = bgConfirm.transform.Find("ButtonOK").GetComponent<Button>();
                btnOk.onClick.RemoveAllListeners();
                btnOk.onClick.AddListener(() => {
                    if (data.resultCallback != null)  data.resultCallback(true);
                    StartCoroutine(DestroyWindow(0));
                });
                Button btnCancel = bgConfirm.transform.Find("ButtonCancel").GetComponent<Button>();
                btnCancel.onClick.RemoveAllListeners();
                btnCancel.onClick.AddListener(()=> {
                    if (data.resultCallback != null)  data.resultCallback(false);
                    StartCoroutine(DestroyWindow(0));
                });
                break;
            case DialogType.Toast:
                toastWindow.SetActive(true);
                GameObject bgToast = toastWindow.transform.Find("BgToast").gameObject;
                if (bgToast == null) Debug.LogWarning("bgToast null");
                GameObject toastMessage = bgToast.transform.Find("ToastMessage").gameObject;
                if (toastMessage != null)
                {
                    toastMessage.GetComponent<Text>().text = dialogData.message;
                    StartCoroutine(DestroyWindow(data.toastLength));
                }
                break;
        }
    }
    
    IEnumerator DestroyWindow(float time)
    {
        yield return new WaitForSeconds(time);
        if (disableListener != null)
        {
            disableListener();
        }
    }
    
}
