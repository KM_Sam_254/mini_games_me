﻿using System;

public class DialogData 
{
	public DialogType Type { get; set; }
    public string  title, message;
    public Action<bool>  resultCallback;
    public float toastLength = 0f;

    public DialogData(DialogType type, string msg, Action<bool> callback)
    {
        this.Type = type;
        this.message = msg;
        this.resultCallback = callback;
    }

    public DialogData(DialogType type, string msg, float toastLength, Action<bool> callback)
    {
        this.Type = type;
        this.message = msg;
        this.toastLength = toastLength;
        this.resultCallback = callback;
    }

    public DialogData(DialogType type, string title, string message, Action<bool> callback)
    {
        this.Type = type;
        this.title = title;
        this.message = message;
        this.resultCallback = callback;
        this.resultCallback = callback;
    }
}
