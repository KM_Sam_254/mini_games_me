﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICanvas : MonoBehaviour
{
    static UICanvas instance;
    GameObject uiWindowCanvas;
    GameObject currentShowingWindow;
    GameObject[] windowObjects;

    public string TestWindow = "testWindow";

    public static UICanvas Instance
    {
        get
        {
            instance = FindObjectOfType(typeof(UICanvas)) as UICanvas;
            if (instance == null) Create();
            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null) instance = this;

        else if (instance != this) Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        // Get window objects
        uiWindowCanvas = Instantiate(Resources.Load("Prefabs/UICanvas") as GameObject);
        Windows w = uiWindowCanvas.GetComponent<Windows>();
        if(w.windows != null && w.windows.Length > 0)
        {
            windowObjects = new GameObject[w.windows.Length];
            for (int i = 0; i < w.windows.Length; i++)
            {
                windowObjects[i] = w.windows[i];
            }
        }
    }

    public static void Create()
    {
        if (instance == null)
        {
            var obj = new GameObject("UICanvas");
            instance = obj.AddComponent<UICanvas>();
        }
    }

    public void ShowWindow(string window)
    {
        uiWindowCanvas.SetActive(true);
        currentShowingWindow = GetWindowToShow(window);
    }

    GameObject GetWindowToShow(string window)
    {
        if (windowObjects == null || windowObjects.Length < 1)
        {
            Debug.LogError("Window to show is null");
            return null;
        }
        for (int i = 0; i < windowObjects.Length; i++)
        {
            if (windowObjects[i].gameObject.name.Equals(window))
            {
                windowObjects[i].SetActive(true);
                return windowObjects[i];
            }
            else windowObjects[i].SetActive(false);
        }
        return null;
    }


}
