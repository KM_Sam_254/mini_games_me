﻿using UnityEngine;

public class DragDropUtil
{
    /// <summary>
    /// Parameters must be UI Element that has RectTransform
    /// </summary>
    public static bool CheckObjectBoundaryOverlapped(Vector3 objPos, GameObject objToCheck)
    {
        bool b = false;
        float halfWidth = objToCheck.GetComponent<RectTransform>().sizeDelta.x / 2;
        float halfHeight = objToCheck.GetComponent<RectTransform>().sizeDelta.y / 2;
        float boundLeft = objToCheck.transform.position.x - halfWidth;
        float boundRight = objToCheck.transform.position.x + halfWidth;
        float boundTop = objToCheck.transform.position.y + halfHeight;
        float boundBottom = objToCheck.transform.position.y - halfHeight;
        float objPosX = objPos.x;
        float objPosY = objPos.y;
        //Debug.Log("objPos : " + objPos  + "  left: " + boundLeft + "  right: " + boundRight+ "  top: " + boundTop + "  bottom: " + boundBottom );

        if(      objPosX > boundLeft
            && objPosX < boundRight
            && objPosY > boundBottom
            && objPosY < boundTop)
        {
            b = true;
        }

        return b;
    }
    
}
