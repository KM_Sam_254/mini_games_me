﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 라인이 가지고 있는 꼭지점
/// </summary>
public class LineVertex
{
    public int Id { get; set; }
    public Vector2 vertexPosition;
    public int[] gridVertexIndex;
    public LineInVertex[] adjoinedLines;  // 양쪽에 닿아있는 라인 오브젝트 2개
    public LineVertex[] adjoinedVertice;  // 양 옆의 꼭지점
    Vector2 beforeVertexPosition;

    public LineVertex()
    {
        gridVertexIndex = new int[2];

        adjoinedLines = new LineInVertex[2];
        adjoinedLines[0] = new LineInVertex(null, -1);
        adjoinedLines[1]= new LineInVertex(null, -1);

        adjoinedVertice = new LineVertex[2];
        adjoinedVertice[0] = null;
        adjoinedVertice[1] = null;
    }

    public bool PushLine(Line line_, int draggablePointOfLine_)
    {
        for (int i = 0; i < adjoinedLines.Length; i++)
        {
            if(adjoinedLines[i].line == null)
            {
                adjoinedLines[i].line = line_;
                adjoinedLines[i].draggablePointOfLine = draggablePointOfLine_;
                return true;
            }
        }

        return false;
    }

   

    public void Grabbed()
    {
        beforeVertexPosition = vertexPosition;
    }

    public void Release()
    {
        // Do something before release
        beforeVertexPosition = vertexPosition;
    }

    public void RollbackPosition()
    {
        UpdateVertexPosition(beforeVertexPosition);
    }

    public void UpdateVertexPosition(Vector3 dragPos)
    {
       if(adjoinedLines[0].line != null)
        {
            if (adjoinedLines[0].draggablePointOfLine == Line.START)
                adjoinedLines[0].line.start = dragPos;
            else if (adjoinedLines[0].draggablePointOfLine == Line.END)
                adjoinedLines[0].line.end = dragPos;
        }

        if(adjoinedLines[1].line != null)
        {
            if (adjoinedLines[1].draggablePointOfLine == Line.START)
                adjoinedLines[1].line.start = dragPos;
            else if (adjoinedLines[1].draggablePointOfLine == Line.END)
                adjoinedLines[1].line.end = dragPos;
        }

        vertexPosition = dragPos;
    }

    /// <summary>
    /// Line object + indicator if the end point with this vertex is start or end point of line?
    /// </summary>
    public struct LineInVertex
    {
        public Line line;
        public int draggablePointOfLine;  // 꼭지점에 닿아 있는 라인이 start or end 인지 : 드래그 할때 필요
        public LineInVertex(Line line_, int draggablePointOfLine_)
        {
            line = line_;
            draggablePointOfLine = draggablePointOfLine_;
        }
    }
}
