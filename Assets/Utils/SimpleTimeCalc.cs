﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTimeCalc : MonoBehaviour {

    /// <summary>
    /// 주어진 시간값에 시간을 빼서 리턴
    /// </summary>
    /// <param name="hour">주어진 시간</param>
    /// <param name="sub">빼는 시간</param>
    /// <returns></returns>
    public static int subtractHour(int hour, int sub)
    {
        for (int i = 0; i < sub; i++)
        {
            hour--;
            if (hour < 1) hour = 12;
        }
        return hour;
    }

    public static int subtractMinute(int minute, int sub)
    {
        for (int i = 0; i < sub; i++)
        {
            minute--;
            if (minute < 0) minute = 59;
        }
        return minute;
    }

    /// <summary>
    /// 주어진 시간값에 시간을 더해서 리턴
    /// </summary>
    /// <param name="hour">주어진 시간</param>
    /// <param name="add">더하는 시간</param>
    /// <returns></returns>
    public static int AddHour(int hour, int add)
    {
        for (int i = 0; i < add; i++)
        {
            hour++;
            if (hour > 12) hour = 1;
        }
        return hour;
    }

    public static int AddMinute(int minute, int add)
    {
        for (int i = 0; i < add; i++)
        {
            minute++;
            if (minute > 59) minute = 0;
        }
        return minute;
    }

    //public static SimpleClockController.TimeData GetAddedTime(SimpleClockController.TimeData time1, SimpleClockController.TimeData time2)
    //{
    //    SimpleClockController.TimeData addedTime = 
    //        new SimpleClockController.TimeData(AddHour(time1.hour, time2.hour), AddMinute(time1.minute, time2.minute));
    //    return addedTime;
    //}
}
