﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidToast {

    private static AndroidToast instance = null;
    private string toastString;
    AndroidJavaObject currentActivity;

    public static AndroidToast Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new AndroidToast();
            }

            return instance;
        }
    }

    public void ShowAndroidToast(string msg)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            showToastOnUiThread(msg);
        }
    }

    void showToastOnUiThread(string toastString)
    {
        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        this.toastString = toastString;

        currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
    }

    void showToast()
    {
        AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_SHORT"));
        toast.Call("show");
    }

}
