﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTweener : MonoBehaviour {

    public float scaleFrom = 0.8f;
    public float animSpeed = 0.8f;

    private void OnEnable()
    {
        SimpleMoveBy();
    }

    void SimpleMoveBy()
    {
        iTween.ScaleBy(gameObject, iTween.Hash("x", scaleFrom, "y", scaleFrom, "speed", animSpeed, "easetype", "easeoutcubic"
            , "looptype", iTween.LoopType.pingPong));
    }
}
