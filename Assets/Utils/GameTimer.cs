﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer
{
    private bool canUpdateTimer = false;
    float second = 0f;
    int minute = 0;

    int MAX_MINUTE = 60;
    float MAX_SECOND = 60f;

    public void UpdateTimer()
    {
        if (canUpdateTimer)
        {
            second += Time.deltaTime;
            if (second > MAX_SECOND)
            {
                minute++;
                second = 0;
            }
            if (minute > 60) GotMaxMinute();
        }
    }

    public void StartTimer()
    {
        second = 0;
        minute = 0;
        canUpdateTimer = true;
    }

    public void StopTimer()
    {
        canUpdateTimer = false;
    }

    public float GetSecond()
    {
        return second;
    }

    public string GetTimeLapsedString()
    {
        return minute + "분 : " + Mathf.Round(second) + "초";
    }

    public int GetMinute()
    {
        return minute;
    }

    void GotMaxMinute()
    {
        // 60분 넘으면 타이머 멈춤
        StopTimer();
    }
}
