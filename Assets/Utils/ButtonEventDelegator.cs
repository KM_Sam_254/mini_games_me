﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEventDelegator : MonoBehaviour
{
    Button _btn;
    Action<string> _onClick;

	void Awake()
    {
        _btn = gameObject.GetComponent<Button>();
        if (_btn == null)
        {
            Debug.LogError("Btn is Null");
            return;
        }

        _btn.onClick.AddListener(()=> {
            if (_onClick != null) _onClick(_btn.name);
        });
	}

    public void AddListener(Action<string> onClick)
    {
        _onClick = onClick;
    }

    private void OnDisable()
    {
        //_btn.onClick.RemoveAllListeners();
    }
}
