﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameSetter : MonoBehaviour {

    public string namePrefix = string.Empty;
    public int startIndex = 0;

    public int GetStartIndex()
    {
        return startIndex;
    }

    public string GetNamePrefix()
    {
        return namePrefix;
    }

    public Transform[] GetChildren()
    {
        return gameObject.GetComponentsInChildren<Transform>();
    }
}
