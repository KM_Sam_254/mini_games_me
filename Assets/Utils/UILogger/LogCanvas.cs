﻿using System.Collections;
using TMPro;
using UnityEngine;

public class LogCanvas : MonoBehaviour
{
    public TextMeshProUGUI logText;

    public void SetLogText(string log)
    {
        logText.text = log;
    }
	
}
