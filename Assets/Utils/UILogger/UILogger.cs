﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// UILogger.Instance.SetLog("Log message");
/// </summary>
public class UILogger : MonoBehaviour
{
    private static UILogger instance=null;
    private GameObject logCanvas;
    private LogCanvas logger;

    public static UILogger Instance
    {
        get
        {
            if (instance == null)
            {
                if (GameManager.ISDEBUG)
                {
                    instance = new GameObject("UILogger").AddComponent<UILogger>();
                }
            }
            return instance;
        }
    }

    private void Awake()
    {
        logCanvas = Instantiate(Resources.Load("Prefabs/LogCanvas")) as GameObject;
        if (instance == null) instance = this;
        logger = logCanvas.GetComponent<LogCanvas>();
        if (logger != null) Debug.Log("UILogger Created");
    }

    private void OnDestroy()
    {
        logCanvas = null;
    }

    public void SetLog(string log)
    {
        logCanvas.SetActive(true);
        logger.SetLogText(log);
    }

    public void InvalidateLogger()
    {
        if (logCanvas != null) logCanvas.SetActive(false);
    }
}
