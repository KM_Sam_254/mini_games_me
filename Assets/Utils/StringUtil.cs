﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringUtil
{
    

    public static string[] SplitString(string str)
    {
        string[] splittedString = new string[5];
        if(!string.IsNullOrEmpty(str))
        {
            splittedString = str.Split('.');
        }

        return splittedString;
    }

    public static string[] SplitString(string str, char splitter)
    {
        string[] splittedString = new string[5];
        if (!string.IsNullOrEmpty(str))
        {
            splittedString = str.Split(splitter);
        }

        return splittedString;
    }

    public static int[] SplitStringToIntArray(string str)
    {
        if (!string.IsNullOrEmpty(str))
        {
            string[] splittedString = str.Split('_');
            if (splittedString.Length > 0)
            {
                int[] array = new int[splittedString.Length];
                for (int i = 0; i < splittedString.Length; i++)
                {
                    array[i] = ParseUtil.ParseStringToInt(splittedString[i]);
                }
                return array;
            }
        }

        return null;
    }

    public static int[] SplitStringToIntArray(string str, char splitter)
    {
        if (!string.IsNullOrEmpty(str))
        {
            string[] splittedString = str.Split(splitter);
            if(splittedString.Length > 0)
            {
                int[] array = new int[splittedString.Length];
                for (int i = 0; i < splittedString.Length; i++)
                {
                    array[i] = ParseUtil.ParseStringToInt(splittedString[i]);
                }
                return array;
            }
        }
        
        return null;
    }
}
