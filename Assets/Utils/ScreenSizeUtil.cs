﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenSizeUtil : MonoBehaviour
{
    public const float STANDARD_SCREEN_WIDTH = 1920;
    public const float STANDARD_SCREEN_HEIGHT = 1080;
    /// <summary>
    /// 스크린기준사이즈(1920x1080)에 대한 현재 디바이스의 스크린사이즈 비율
    /// </summary>
    /// <returns></returns>
    public static float GetScreenWidthRatio()
    {
        return (float)Screen.width / STANDARD_SCREEN_WIDTH;
    }

    public static Vector3 GetScreenSizeScaleRatioV3()
    {
        return new Vector3(GetScreenWidthRatio(), GetScreenHeightRatio(), 0);
    }

    public static float GetScreenHeightRatio()
    {
        return (float)Screen.height / STANDARD_SCREEN_HEIGHT;
    }

    public static Vector3 GetScreenRightBottomEndPosition()
    {
        return Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0));
    }

    public static Vector3 GetScreenLeftTopEndPosition()
    {
        return Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
    }
}
