﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FractionUtil
{
    // 인자로 전달되는 2개의 분수가 같은 수인지 판별
    public static bool IsRCDSameFraction(int[] LF, int[] RF)
    {
        int[] L = GetIrreducibleFraction(LF);
        int[] R = GetIrreducibleFraction(RF);
        Debug.Log("IsRcdSame test1 : " + L[0] + "/" + L[1]
            + "  test2 : " + R[0] + "/" + R[1]);
        if (L[1] == R[1] && L[0] == R[0]) return true;

        return false;
    }

    // 기약분수 구하기(약분을 다한 분수)
    public static int[] GetIrreducibleFraction(int[] fraction)
    {
        if (fraction.Length < 2) return null;

        int[] rcdFraction = new int[2];
        for (int i = 9; i > 1; i--)
        {
            if (fraction[0] % i == 0 && fraction[1] % i == 0)
            {
                rcdFraction[0] = fraction[0] / i;
                rcdFraction[1] = fraction[1] / i;
                return rcdFraction;
            }
        }

        return fraction;
    }

    /// <summary>
    /// 인자로 전달된 두 분수의 분모의 최소공배수를 구하기
    /// </summary>
    /// <param name="num1"></param>
    /// <param name="num2"></param>
    /// <returns></returns>
    public static int GetLeastCommonMultiple(int num1, int num2)
    {
        int i;
        for (i = 1; !(i % num1 == 0 && i % num2 == 0); i++) ;
        return i;
    }

    /// <summary>
    /// 전달된 분모(최소공배수LCM)를 가진 분수로 변환하기 위한 함수
    /// </summary>
    /// <param name="fraction">대상 분수</param>
    /// <param name="LCM">최소공배수</param>
    /// <returns></returns>
    public static int[] GetRCDFraction(int[] fraction, int LCM)
    {
        if (fraction.Length != 2) return null;
        int[] f = new int[fraction.Length];
        int factor = LCM / fraction[1];
        f[0] = fraction[0] * factor;
        f[1] = fraction[1] * factor;
        return f;
    }

    public static int[] GetFractionIntArray(string strFraction)
    {
        int[] f = StringUtil.SplitStringToIntArray(strFraction, '_');
        if (f.Length != 2)
        {
            Debug.LogWarning("GetFraction array NULL");
            return null;
        }
        return f;
    }

    /// <summary>
    /// 인자로 전달된 분수가 대분수인지 체크해서 대분수이면 array 배열의 3번째 값으로 넣어줌
    /// 진분수는 그냥 원래의 분수를 리턴
    /// 분모와 분자가 같을 경우는 자연수를 리턴(배열 3개중 마지막 요소에 자연수만 넣어줌)
    /// 대분수의 경우는 배열 0:분자, 1:분모, 2:대분수값
    /// </summary>
    public static int[] CheckIfFractionMixed(int[] fraction)
    {
        //Debug.Log("CheckIfMixed " + fraction[0] + "/" + fraction[1]);
        if (fraction[0] == 0 || fraction[1] == 0) return fraction;
        int[] f;
        if (fraction[0] % fraction[1] == 0)
        {
            f = new int[3];
            f[0] = 0;
            f[1] = 0;
            f[2] = fraction[0] / fraction[1];
            return f;
        }
        else if (fraction[0] < fraction[1]) return fraction;
        else
        {
            f = new int[3];
            f[0] = fraction[0] % fraction[1];
            f[1] = fraction[1];
            f[2] = fraction[0] / fraction[1];
            return f;
        }
    }

    /// <summary>
    /// string으로 된 분수 2개를 넣어서 통분수 합을 구하는 함수
    /// 인자로 전달되는 string 은 "0_0" 의 형태("분자_분모")여야 함
    /// </summary>
    /// <param name="left">통분수를 구하고자 하는 분수중 하나</param>
    /// <param name="right">통분수를 구하고자 하는 분수중 하나</param>
    /// <returns></returns>
    public static int[] GetFractionAdded(string left, string right)
    {
        int[] fL = StringUtil.SplitStringToIntArray(left);
        int[] fR = StringUtil.SplitStringToIntArray(right);

        if (fL.Length != 2 || fR.Length != 2)
        {
            Debug.LogWarning("Inappropriate fraction Int array");
            return null;
        }

        int lcm = FractionUtil.GetLeastCommonMultiple(fL[1], fR[1]);

        fL = FractionUtil.GetRCDFraction(fL, lcm);
        fR = FractionUtil.GetRCDFraction(fR, lcm);

        int[] f = new int[2];
        f[0] = fL[0] + fR[0];  // 분자끼리 더함
        f[1] = fL[1];

        return f;
    }

    public static int[] GetMultipliedFraction(int[] f1, int[] f2)
    {
        int[] ansFraction = new int[2];
        ansFraction[0] = f1[0] * f2[0];
        ansFraction[1] = f1[1] * f2[1];
        return ansFraction;
    }
}
