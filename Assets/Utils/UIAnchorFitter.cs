﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAnchorFitter : MonoBehaviour {

	public RectTransform GetRectTransform()
    {
        return gameObject.GetComponent<RectTransform>();
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }
}
