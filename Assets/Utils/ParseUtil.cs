﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParseUtil
{
    public static int ParseStringToInt(string v)
    {
        int value = 0;
        if (!int.TryParse(v, out value)) Debug.LogWarning("ParseStringToInt FALSE : " + v);
        return value;
    }

    public static int ParseStringToInt(string v, int defaultValue)
    {
        int value = defaultValue;
        if (!int.TryParse(v, out value))
        {
            //Debug.LogWarning("ParseStringToInt FALSE");
            return defaultValue;
        }
        return value;
    }
}
