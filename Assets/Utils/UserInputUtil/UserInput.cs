﻿
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 사용설명
/// Must Regist drag handler on its Start()
/// alse must UnRegist handler on its OnDestroy or OnDisable
/// </summary>
public class UserInput : MonoBehaviour
{
    protected static UserInput s_Instance;
    public static UserInput Instance
    {
        get
        {
            if (s_Instance == null)
            {
                s_Instance = FindObjectOfType(typeof(UserInput)) as UserInput;

                if(!s_Instance) Create();

                // prep the scalers. for the distance scaler we just use an average of the width and height scales
                var aCamera = Camera.main ?? Camera.allCameras[0];
                if (aCamera.orthographic)
                {
                    setupPixelsToUnityUnitsMultiplierWithCamera(aCamera);
                }
                else
                {
                    s_Instance.pixelsToUnityUnitsMultiplier = Vector2.one;
                }

                s_Instance.setupRuntimeScale();

                // You can add gestures recognizer whatever u want
                s_Instance.AddSingleTouchGesture();

            }
            return s_Instance;
        }
    }

    void Awake()
    {
        if (s_Instance == null) s_Instance = this;

        else if (s_Instance != this) Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        // prep our TKTouch cache so we avoid excessive allocations
        _touchCache = new TKTouch[maxTouchesToProcess];
        for (int i = 0; i < maxTouchesToProcess; i++)
            _touchCache[i] = new TKTouch(i);
    }

    public static void Create()
    {
        if(s_Instance == null)
        {
            var obj = new GameObject("UserInput");
            s_Instance = obj.AddComponent<UserInput>();
        }
    }

    void AddSingleTouchGesture()
    {
        var recognizer = new TKSingleTouchRecognizer(new TKRect(0, 0, Screen.width, Screen.height));
        recognizer.zIndex = 10;
        recognizer.onEnteredEvent += (r) =>
        {
            if(shouldAutoUpdateTouches)
            {
                //Debug.Log("touch entered: " + r);
                foreach (var tr in m_TouchRecognizer)
                {
                    tr.OnInputBegin(GetBeginEventData(r.touchLocation()));
                }
            }
        };
        recognizer.onMovedEvent += (r) =>
        {
            if(shouldAutoUpdateTouches)
            {
                //Debug.Log("touch moved : " + r);
                foreach (var tr in m_TouchRecognizer)
                {
                    tr.OnInputDrag(GetDraggingEventData(r.touchLocation()));
                }
            }
        };
        recognizer.onEndedEvent += (r) =>
        {
            if(shouldAutoUpdateTouches)
            {
                //Debug.Log("UserInput touch ended: " + r);
                foreach (var tr in m_TouchRecognizer)
                {
                    tr.OnInputEnd(GetEndEventData(r.touchLocation()));
                }
            }
            
        };
        addGestureRecognizer(recognizer);
    }

    /// <summary>
	/// if false, TouchKit will not do anything in Update. You will need to call updateTouches yourself.
	/// </summary>
	public bool shouldAutoUpdateTouches = true;

    void Update ()
    {
        if (schedule != null)
        {
            schedule();
            schedule = null;
        }

        if (!shouldAutoUpdateTouches) return;

#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER || UNITY_WEBGL
        if (Input.GetMouseButtonUp(0) || Input.GetMouseButton(0))
            _liveTouches.Add(_touchCache[0].populateFromMouse());
#endif

        // get all touches and examine them. only do our touch processing if we have some touches
        if (Input.touchCount > 0)
        {
            _shouldCheckForLostTouches = true;

            var maxTouchIndexToExamine = Mathf.Min(Input.touches.Length, maxTouchesToProcess);
            for (var i = 0; i < maxTouchIndexToExamine; i++)
            {
                var touch = Input.touches[i];
                if (touch.fingerId < maxTouchesToProcess)
                    _liveTouches.Add(_touchCache[touch.fingerId].populateWithTouch(touch));
            }
        }
        else
        {
            // we guard this so that we only check once after all the touches are lifted
            if (_shouldCheckForLostTouches)
            {
                addTouchesUnityForgotToEndToLiveTouchesList();
                _shouldCheckForLostTouches = false;
            }
        }

        // pass on the touches to all the recognizers
        if (_liveTouches.Count > 0)
        {
            for (var i = 0; i < _gestureRecognizers.Count; i++)
                _gestureRecognizers[i].recognizeTouches(_liveTouches);
            _liveTouches.Clear();
        }
    }

    private List<TKAbstractGestureRecognizer> _gestureRecognizers = new List<TKAbstractGestureRecognizer>(5);
    private TKTouch[] _touchCache;
    private List<TKTouch> _liveTouches = new List<TKTouch>(2);
    private bool _shouldCheckForLostTouches = false; // used internally to ensure we dont check for lost touches too often
    public int maxTouchesToProcess = 2;

    public static void addGestureRecognizer(TKAbstractGestureRecognizer recognizer)
    {
        // add, then sort and reverse so the higher zIndex items will be on top
        s_Instance._gestureRecognizers.Add(recognizer);

        if (recognizer.zIndex > 0)
        {
            s_Instance._gestureRecognizers.Sort();
            s_Instance._gestureRecognizers.Reverse();
        }
    }

    /// <summary>
    /// Unity often misses the Ended phase of touches so this method will look out for that
    /// </summary>
    private void addTouchesUnityForgotToEndToLiveTouchesList()
    {
        for (int i = 0; i < _touchCache.Length; i++)
        {
            if (_touchCache[i].phase != TouchPhase.Ended)
            {
                Debug.LogWarning("found touch Unity forgot to end with phase: " + _touchCache[i].phase);
                _touchCache[i].phase = TouchPhase.Ended;
                _liveTouches.Add(_touchCache[i]);
            }
        }
    }

    GameObject uiTouchedObject = null;
    RectTransform canvasRectTransform;

    /// <summary>
    /// For dragging UI object
    /// </summary>
    public void OnPointerDown(GameObject obj, RectTransform rt)
    {
        uiTouchedObject = obj;
        canvasRectTransform = rt;
    }

    PointEventData GetBeginEventData(Vector3 inputPosition)
    {
        PointEventData eventData = new PointEventData();
        eventData.InputBeginWorldPosition = GetInput.GetWorldPositionV3(inputPosition);
        eventData.InputBeginLocalPosition = inputPosition;

        eventData.InputBeginSelctedDraggableObjects = null;
        RaycastHit2D[] hits = Physics2D.RaycastAll(eventData.InputBeginWorldPosition, Vector3.forward, 10f);
        if (hits.Length > 0)
        {
            eventData.InputBeginSelctedDraggableObjects = new GameObject[hits.Length];
            for (int i = 0; i < hits.Length; i++)
            {
                eventData.InputBeginSelctedDraggableObjects[i] = hits[i].collider.gameObject;
            }
            eventData.InputBeginDragPosition = GetInput.GetWorldPositionV3(inputPosition);
        }

        // recognizing if ui element touched when input began
        if (uiTouchedObject != null)
        {
            eventData.InputBeginSelctedDraggableObjects = new GameObject[1];
            eventData.InputBeginSelctedDraggableObjects[0] = uiTouchedObject;
            eventData.InputBeginDragPosition = GetInput.GetWorldToScreenPosition(inputPosition, canvasRectTransform);
        }

        return eventData;
    }

    PointEventData GetEndEventData(Vector3 inputPosition)
    {
        eventData = new PointEventData();
        eventData.InputEndWorldPosition = GetInput.GetWorldPositionV3(inputPosition);

        // recognizing button click event
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject != null)
        {
            eventData.InputEndSelectedUIButtonName = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
            eventData.InputEndSelectedUIButton = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;
        }

        if (uiTouchedObject != null)
        {
            uiTouchedObject = null;
            canvasRectTransform = null;
        }

        return eventData;
    }

    PointEventData GetDraggingEventData(Vector3 inputPosition)
    {
        eventData = new PointEventData();
        eventData.InputDraggingWorldPosition = GetInput.GetWorldPositionV3(inputPosition);
        eventData.InputDraggingLocalPosition = inputPosition;

        if(uiTouchedObject != null)
        {
            eventData.InputDraggingWorldPosition = GetInput.GetWorldToScreenPosition(inputPosition, canvasRectTransform);
        }

        return eventData;
    }

    protected HashSet<IKTouchRecognizer> m_TouchRecognizer = new HashSet<IKTouchRecognizer>();
    event System.Action schedule = null;
    PointEventData eventData;

    public void RegistTouchRecognizer(IKTouchRecognizer tr)
    {
        schedule += () =>
        {
            m_TouchRecognizer.Add(tr);
        };
    }

    public void UnRegistTouchRecognizer(IKTouchRecognizer tr)
    {
        schedule += () =>
        {
            m_TouchRecognizer.Remove(tr);
        };
    }

    public Vector2 runtimeScaleModifier { get; private set; }
    private Vector2 _designTimeResolution = new Vector2(320, 180); // 16:9 is a decent starting point for aspect ratio
    public Vector2 designTimeResolution
    {
        get
        {
            return _designTimeResolution;
        }
        set
        {
            _designTimeResolution = value;
            setupRuntimeScale();
        }
    }
    public float runtimeDistanceModifier { get; private set; }
    public bool autoScaleRectsAndDistances = true;

    protected void setupRuntimeScale()
    {
        s_Instance.runtimeScaleModifier = new Vector2(Screen.width / s_Instance.designTimeResolution.x, Screen.height / s_Instance.designTimeResolution.y);
        s_Instance.runtimeDistanceModifier = (s_Instance.runtimeScaleModifier.x + s_Instance.runtimeScaleModifier.y) / 2f;

        if (!s_Instance.autoScaleRectsAndDistances)
        {
            s_Instance.runtimeScaleModifier = Vector2.one;
            s_Instance.runtimeDistanceModifier = 1f;
        }
    }

    public Vector2 pixelsToUnityUnitsMultiplier { get; private set; }
    public static void setupPixelsToUnityUnitsMultiplierWithCamera(Camera cam)
    {
        if (!cam.orthographic)
        {
            Debug.LogError("Attempting to setup unity pixel-to-units modifier with a non-orthographic camera");
            return;
        }

        var screenSizeUnityUnits = new Vector2(cam.aspect * cam.orthographicSize * 2f, cam.orthographicSize * 2f);
        s_Instance.pixelsToUnityUnitsMultiplier = new Vector2
        (
            screenSizeUnityUnits.x / (float)Screen.width,
            screenSizeUnityUnits.y / (float)Screen.height
        );
    }

    private const float inchesToCentimeters = 2.54f;

    public float ScreenPixelsPerCm
    {
        get
        {
            float fallbackDpi = 72f;
#if UNITY_ANDROID
            // Android MDPI setting fallback
            // http://developer.android.com/guide/practices/screens_support.html
            fallbackDpi = 160f;
#elif (UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA || UNITY_WSA_8_0)
				// Windows phone is harder to track down
				// http://www.windowscentral.com/higher-resolution-support-windows-phone-7-dpi-262
				fallbackDpi = 92f;
#elif UNITY_IOS
				// iPhone 4-6 range
				fallbackDpi = 326f;
#endif

            return Screen.dpi == 0f ? fallbackDpi / inchesToCentimeters : Screen.dpi / inchesToCentimeters;
        }
    }
}
