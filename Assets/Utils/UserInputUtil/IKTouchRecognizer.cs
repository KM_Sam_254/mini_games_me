﻿public interface IKTouchRecognizer
{
    void OnInputBegin(PointEventData eventData);
    void OnInputEnd(PointEventData eventData);
    void OnInputDrag(PointEventData eventData);
}
