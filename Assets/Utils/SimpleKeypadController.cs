﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class SimpleKeypadController
{
    int STRING_LENGTH_LIMIT = 2;
    int VALUE_LIMIT = 100;
    string keyString = string.Empty;
    Action<int> callBack;
    public const int KEY_ENTER = 100;
    public const int KEY_CLEAR = 101;

    // text 를 등록해놓으면 값 갱신될때 text 도 업데이트
    TextMeshPro textMeshPro;
    TextMeshProUGUI textMeshProUGUI;

    public SimpleKeypadController(int stringLengthLimit, Action<int> cb)
    {
        STRING_LENGTH_LIMIT = stringLengthLimit;
        callBack = cb;
    }

    public void KeyPressed(string key)
    {
        if ((string.IsNullOrEmpty(keyString) || keyString.Equals("0")) && key.Equals("0")) return;
        else if (keyString.Equals("0") && !key.Equals("0")) keyString = string.Empty;

        //Debug.Log("Key Pressed : " + key);

        if (key.Equals("enter"))
        {
            if (callBack != null) callBack(KEY_ENTER);
        }
        else if(key.Equals("clear"))
        {
            keyString = string.Empty;

            if (callBack != null) callBack(KEY_CLEAR);
        }
        else
        {
            int k = ParseUtil.ParseStringToInt(key);
            //Debug.Log("KeyPressedString : " + key + "  keyInt : " + k);
            if (k < 10)
            {
                keyString += key;
                if (keyString.Length > STRING_LENGTH_LIMIT)
                {
                    keyString = key;
                }

                int value = ParseUtil.ParseStringToInt(keyString);
                if (value > VALUE_LIMIT) keyString = "" + VALUE_LIMIT;

                if (callBack != null) callBack(value);
            }
        }

        UpdateText();
    }

    public int GetCurrentKeyValueInt()
    {
        return ParseUtil.ParseStringToInt(keyString);
    }

    public string GetCurrentKeyValueString()
    {
        return keyString;
    }

    public void SetStringLengthLimit(int limit)
    {
        STRING_LENGTH_LIMIT = limit;
    }

    public void SetCallback(Action<int> cb)
    {
        callBack = cb;
    }

    public void SetDefaultKeyString(int value)
    {
        //if (string.IsNullOrEmpty(keyString) && value == 0) return;
        if (value == 0) keyString = string.Empty;
        else keyString = "" + value;
    }

    /// <summary>
    /// UserInput 에서 오는 오브젝트 배열을 때려 넣으면 알아서 키패드 눌렸는지 찾아서 전달해주는 함수
    /// </summary>
    public void PushSelectedObjects(GameObject[] objects)
    {
        if (objects == null || objects.Length < 1) return;
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].CompareTag("Key"))
            {
                KeyPressed(objects[i].name);
                break;
            }
        }
    }

    public void SetTextMeshPro(TextMeshPro tmPro)
    {
        this.textMeshPro = tmPro;
    }

    public void SetTextmeshPro(TextMeshProUGUI tmPro)
    {
        this.textMeshProUGUI = tmPro;
    }

    void UpdateText()
    {
        if (textMeshPro != null) textMeshPro.text = keyString;
        else if (textMeshProUGUI != null) textMeshProUGUI.text = keyString;
    }

    public void SetValueLimit(int limit)
    {
        this.VALUE_LIMIT = limit;
    }
}
