﻿using System.IO;
using UnityEngine;

public class FileUtil : MonoBehaviour
{
    /// <summary>
    /// 폴더에 찾는 파일이 있는지 확인할수 있는 함수
    /// </summary>
    /// <param name="folderPath">찾는 폴더 경로</param>
    /// <param name="filePath">찾는 파일의 경로이름</param>
    /// <returns></returns>
    public static bool IsFileExist(string folderPath, string filePath)
    {
        DirectoryInfo dir = new DirectoryInfo(folderPath);
        FileInfo[] fileInfo = dir.GetFiles("*.*", SearchOption.AllDirectories);
        Debug.Log("FileInfo : " + fileInfo.Length);
        foreach (FileInfo file in fileInfo)
        {
            if (File.Exists(filePath)) return true;
        }
        return false;
    }
}
