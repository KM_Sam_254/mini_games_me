﻿using System;
using UnityEngine;

public class DateTimeUtil : MonoBehaviour
{
    /// <summary>
    /// targetDate 이 startDate 에 dueMonth(사용기간)를 더한 날짜보다 지났다면 false
    /// 아직 안지났다면  true
    /// </summary>
    /// <param name="startDate">시작날짜</param>
    /// <param name="targetDate">비교할 날짜</param>
    /// <param name="dueDate">사용기간</param>
    /// <returns></returns>
    public static bool IsTargetDateValid(DateTime startDate, DateTime targetDate, int dueDate)
    {
        startDate = startDate.AddDays(dueDate);
        // 왼쪽날짜가 나중이면 1, 같으면 0, 오른쪽이 나중이면 -1
        int compare = DateTime.Compare(startDate, targetDate);
        Debug.Log("CheckUserValidation startDate : " + startDate + "     targetDate : " + targetDate + "  compare : " + compare);
        if (compare >= 0) return true;
        return false;
    }

    public static bool GetUserValidationOfCurrentime()
    {
        SetUserStartDate();

        DateTime startDate = new DateTime();
        DateTime.TryParse(GetUserStartDate(), out startDate);
        if (IsTargetDateValid(startDate, DateTime.Now, 14)) return true;
        return false;            
    }

    public static string GetDayString()
    {
        return DateTime.Now.Day.ToString();
    }

    public static int GetDayInt()
    {
        return DateTime.Now.Day;
    }

    public static string GetMonthString()
    {
        return DateTime.Now.Month.ToString();
    }

    public static int GetMonthInt()
    {
        return DateTime.Now.Month;
    }

    public static string GetYearString()
    {
        return DateTime.Now.Year.ToString();
    }

    public static int GetYearInt()
    {
        return DateTime.Now.Year;
    }

    public static string GetYearMonthDayString()
    {
        return DateTime.Now.ToString("yyyy/MM/dd");
    }

    public static string GetDateAndTimeString()
    {
        return DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
    }

    public static void SetUserStartDate()
    {
        if (string.IsNullOrEmpty(GetUserStartDate()))
        {
            Debug.Log("SetUserStartDate : " + DateTime.Now.ToString());
            PlayerPrefs.SetString("userstartdate", DateTime.Now.ToString());
        }
    }

    public static string GetUserStartDate()
    {
        return PlayerPrefs.GetString("userstartdate", "");
    }
}
