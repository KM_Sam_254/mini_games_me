﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalBgScroller : MonoBehaviour
{
    public GameObject[] bgObjects;
    public float speed = 2f;
    float xMoveOffset;
    GameObject lastBgObj;

    void Start()
    {
        lastBgObj = bgObjects[bgObjects.Length - 1];
    }

    float x;
    private void Update()
    {
        for (int i = 0; i < bgObjects.Length; i++)
        {
            x = bgObjects[i].transform.position.x - (speed * 1000) * Time.deltaTime;
            iTween.MoveUpdate(bgObjects[i], iTween.Hash("x", x));

            // check end position
            if (bgObjects[i].transform.position.x <= -(bgObjects[i].GetComponent<Image>().sprite.texture.width / 2) - 50f)
            {
                xMoveOffset = lastBgObj.GetComponent<Image>().sprite.texture.width / 2
                    + bgObjects[i].GetComponent<Image>().sprite.texture.width / 2;

                bgObjects[i].transform.position = new Vector3(
                    lastBgObj.transform.position.x + xMoveOffset,
                    bgObjects[i].transform.position.y, 0);
                lastBgObj = bgObjects[i];
            }
        }
    }

}
