﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineAnswer : MonoBehaviour {

    public int a;
    public Color[] color;

    public void GetAnswer()
    {
        if (a < 3)
        {
            this.gameObject.GetComponent<Image>().color = color[a];
        }
        else
        {
            a = 0;
            this.gameObject.GetComponent<Image>().color = color[a];
        }
        a++;
    }



}
