﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineQustion : MonoBehaviour {

    private Button next;
    public List<Image> green;
    public List<Image> yellow;
    public List<Image> red;
    public Image[] lastLine;
    public Image[] ansCircle;
    public Color[] color;

    public void ShowLine()
    {
        next = GameObject.Find("ButtonNext").GetComponent<Button>();
        next.interactable = false;
        StartCoroutine(LineColor());
    }

    void CheckAnswer()
    {
        for (int i = 0; i < lastLine.Length; i++)
        {
            if (ansCircle [i].color == lastLine[i].color)
            {
                AnswerSound.answer = "right";
                Result.right += 1;
            }
            else
            {
                AnswerSound.answer = "wrong";
            }
        }
    }

    IEnumerator LineColor()
    {
        for (int i = 0; i < green.Count; i++)
        {
            green[i].color = color[0];
            yield return new WaitForSeconds(0.5f);
        }
        for (int i = 0; i < yellow.Count; i++)
        {
            yellow[i].color = color[1];
            yield return new WaitForSeconds(0.5f);
        } 
        for (int i = 0; i < red.Count; i++)
        {
            red[i].color = color[2];
            yield return new WaitForSeconds(0.5f);
        } 
        CheckAnswer();
        yield return new WaitForSeconds(3f);
        next.interactable = true;
    }
}
