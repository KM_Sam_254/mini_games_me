﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LinePlay: MonoBehaviour {

    private GameObject[] question;
    private LineQustion[] qus;
    private Transform origin;
    private Transform hidePos;
    public int index = 0;
    private int touch_count = 0;

    void Start()
    {
        question = new GameObject[this.transform.childCount];
        qus = new LineQustion[this.transform.childCount];
        origin = this.gameObject.transform;
        hidePos = GameObject.Find("HidePos").transform;
        AddChild();
        GameStart();
    }

    void AddChild()
    {
        for (int i = 0; i < question.Length; i++)
        {
            question[i] = this.gameObject.transform.GetChild(i).gameObject;
            qus[i] = this.gameObject.transform.GetChild(i).GetComponent<LineQustion>();
        }
    }

    void GameStart()
    {
        if (index < 15)
        {       
            ShowQuestion();
        }
        else
        {
            GameEnd();
        }
    }

    void ShowQuestion()
    {
        iTween.MoveTo(question[index], origin.position, 1f);
        if (index > 0)
        {
            iTween.MoveTo(question[index - 1], hidePos.position, 1f);
        }
    }

    public void Next()
    {
        touch_count++;
        if (touch_count == 1)
        {
            qus[index].ShowLine();
        }
        else
        {       
            index++;
            GameStart();
            touch_count = 0;
        }
        Debug.Log(index);
    }

    void GameEnd()
    {
        Result.image = 7;
        Result.title = 7;
        SceneManager.LoadScene("Complete");
    }
}
