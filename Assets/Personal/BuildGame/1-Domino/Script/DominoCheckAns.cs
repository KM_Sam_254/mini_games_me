﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DominoCheckAns : MonoBehaviour {

    public int ans;

    private void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Tile"))
        {
            for (int i = 0; i < 10; i++)
            {
                if (other.GetComponent<Image>().sprite.name == "dom_"+ i)
                {
                    ans = i;
                }
            }
        }
    }

}
