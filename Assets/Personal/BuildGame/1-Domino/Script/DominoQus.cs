﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DominoQus : MonoBehaviour {

   // [HideInInspector]
    public Image[] questions;
   // [HideInInspector]
    public Image[] answers;
   // [HideInInspector]
    public Sprite[] sprite;
   // [HideInInspector]
    public GameObject[] domObjs;
   // [HideInInspector]
    public GameObject[] qusSlots;
   // [HideInInspector]
    public GameObject[] ansSlots;
    public Transform[] domPos;
   // [HideInInspector]
    public Button[] swap;
  //  [HideInInspector]
    public Button next;
  //  [HideInInspector]
	public OldDemantia.QuestionHandler qus;
   // [HideInInspector]
    public OldDemantia.QuestionHandler ans;
  //  [HideInInspector]
    public Dropdown dropdown;
    public GameObject payment;
    public string[] easy;
    public string[] normal;
    public string[] hard;
    public string[] easyAns;
    public string[] normalAns;
    public string[] hardAns;
    public int index = 0;
    private int right;

    private void Start()
    {
        dropdown.value = 2;
        GameStart();
    }

    public void GameStart()
    {
        if (index < 15)
        {
            Difficulty();
        }
        else
        {
            GameEnd();
        }
        index++;
    }

    void Difficulty()
    {
        if (dropdown.value == 0)
        {
            qus.Split(easy, index);
            ans.Split(easyAns, index);
        }
        else if (dropdown.value == 1)
        {
            qus.Split(normal, index);
            ans.Split(normalAns, index);
        }
        else if (dropdown.value == 2)
        {
            qus.Split(hard, index);
            ans.Split(hardAns, index);
        }
        AssignQusAndAnswer();
    }

    void AssignQusAndAnswer()
    {
        HideAll();
        ShowDomi();
        for (int i = 0; i < qus.q.Count; i++)
        {
            questions[i].sprite = sprite[qus.q[i]];
            answers[i].sprite = sprite[ans.q[i]];
        }
    }   

    public void CheckAnswer()
    {
        next.interactable = false;
        EnableChecking();   
        StartCoroutine(Next());
    }

    public void Swap(int btn)
    {
        qusSlots[btn].transform.Rotate(0f, 0f, -90f);
        domObjs[btn].transform.Rotate(0f, 0f, -90f);
    }

    void EnableChecking()
    {
        for (int i = 0; i < qus.q.Count; i++)
        {
            questions[i].GetComponent<CircleCollider2D>().enabled = true;
            answers[i].GetComponent<DominoCheckAns>().enabled = true;
            answers[i].GetComponent<CircleCollider2D>().enabled = true;
            answers[i].GetComponent<Rigidbody2D>().simulated = true;
        }
    }

    void ShowDomi()
    {
        for (int i = 0; i < qus.q.Count / 2; i++)
        {
            qusSlots[i].gameObject.SetActive(true);
            ansSlots[i].gameObject.SetActive(true);
            domObjs[i].gameObject.SetActive(true);
            swap[i].gameObject.SetActive(true);            
        }
        for (int j = 0; j < 2; j++)
        {
            if (index % 2 == 0)
            {
                ansSlots[j].transform.rotation = domPos[j].rotation;
                ansSlots[j].transform.position = domPos[j].position;
            }
            else
            {
                ansSlots[j].transform.rotation = domPos[j + 2].rotation;
                ansSlots[j].transform.position = domPos[j + 2].position;
            }
        }
    }

    void HideAll()
    {
        for (int i = 0; i < qusSlots.Length; i++)
        {
            qusSlots[i].gameObject.SetActive(false);
            ansSlots[i].gameObject.SetActive(false);
            domObjs[i].gameObject.SetActive(false);
            swap[i].gameObject.SetActive(false);
        }
    }

    void ResetDominoes()
    {      
        for (int i = 0; i < qus.q.Count / 2; i++)
        {
            qusSlots[i].transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            domObjs[i].transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            domObjs[i].transform.position = qusSlots[i].transform.position;
           // domObjs[i].transform.rotation = qusSlots[i].transform.rotation;
        }
        for (int i = 0; i < qus.q.Count; i++)
        {
            questions[i].GetComponent<CircleCollider2D>().enabled = false ;
            answers[i].GetComponent<DominoCheckAns>().enabled = false;
            answers[i].GetComponent<CircleCollider2D>().enabled = false;
            answers[i].GetComponent<Rigidbody2D>().simulated = false;
        }
        right = 0;
    }

    void GameEnd()
    {
		SceneManager.LoadScene ("Complete");
        Result.image = 0;
        Result.title = 0;
       // payment.SetActive(true);
    }

    IEnumerator Next()
    {
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < qus.q.Count; i++)
        {
            if (answers[i].GetComponent<DominoCheckAns>().ans == ans.q[i])
            {
                right += 1;
            }
        }
        if (right == qus.q.Count)
        {
            AnswerSound.answer = "right";
        }
        else
        {
			AnswerSound.answer = "wrong";
        }
        yield return new WaitForSeconds(3f);
        next.interactable = true;
        ResetDominoes();
        GameStart();
    }
    
}
