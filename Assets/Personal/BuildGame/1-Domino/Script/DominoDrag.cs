﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DominoDrag : MonoBehaviour {

    private GameObject dragObj;
   // [HideInInspector]
    public Transform parent;
    [HideInInspector]
    public Transform tempPos;
    public bool isSlot = false;

    public void Drag()
    {
        dragObj = this.gameObject;
        dragObj.transform.position = Input.mousePosition;
        dragObj.transform.SetAsLastSibling();
    }

    public void Drop()
    {        
        if (isSlot == true)
        {            
            dragObj.transform.position = tempPos.position;
         //   dragObj.transform.rotation = tempPos.rotation;
        }
        else if (isSlot == false)
        {            
            dragObj.transform.position = parent.position;
         //   dragObj.transform.rotation = parent.rotation;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Slot"))
        {            
            isSlot = true;
            tempPos = other.transform;
        //    tempPos.rotation = other.transform.rotation;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isSlot = false;
        tempPos = parent.transform;
     //   tempPos.rotation = parent.transform.rotation;
    }
}
