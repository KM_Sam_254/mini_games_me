﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BallCompareInputMgr : MonoBehaviour
{
    private bool draggingItem = false;
    private GameObject draggedObject;
    public GameObject star;
    public Button next;

    void Update()
    {  
        if (HasInput)
        {            
            DragOrPickUp();
        }
        else
        {
            if (draggingItem)
            {                
                DropItem();
            }
                
        }
    }
    Vector2 CurrentTouchPosition
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }
    private void DragOrPickUp()
    {
        var inputPosition = CurrentTouchPosition;
        if (draggingItem)
        {
        }
        else
        {
            RaycastHit2D[] touches = Physics2D.RaycastAll(inputPosition, inputPosition, 0.5f, LayerMask.GetMask("PickUp"));
            if (touches.Length > 0)
            {
                var hit = touches[0];
                if (hit.transform != null && hit.transform.tag == "Balls" )
                {
                    draggingItem = true;
                    draggedObject = hit.transform.gameObject;
                    star.transform.position = draggedObject.transform.position;
                    star.GetComponent<ParticleSystem>().Play();
                    draggedObject.SetActive(false);
                    PopBall("Left");
                    StartCoroutine("Hide");
                }
                else if (hit.transform != null && hit.transform.tag == "Balls2")
                {
                    draggingItem = true;
                    draggedObject = hit.transform.gameObject;
                    star.transform.position = draggedObject.transform.position;
                    star.GetComponent<ParticleSystem>().Play();
                    draggedObject.SetActive(false);
                    PopBall("Right");
                    StartCoroutine("Hide");
                }
            }
        }
    }

    private bool HasInput
    {
        get
        {
            return Input.GetMouseButton(0);
        }
    }
    void DropItem()
    {
        draggingItem = false;       
    }

    void PopBall(string side)
    {
        next.interactable = true;
        for (int i = 1; i <= 5; i++)
        {
            if (draggedObject.GetComponent<SpriteRenderer>().sprite.name == "ballsNo_"+ i && side == "Left")
            {
                BallCompare.total_left -= i;
                Debug.Log(BallCompare.total_left);
            }
            else if (draggedObject.GetComponent<SpriteRenderer>().sprite.name == "ballsNo_" + i && side == "Right")
            {
                BallCompare.total_right -= i;
                Debug.Log(BallCompare.total_right);
            }
        }
    }

    IEnumerator Hide()
    {
        yield return new WaitForSeconds(0.5f);
        star.GetComponent<ParticleSystem>().Stop();
    }

   
}
