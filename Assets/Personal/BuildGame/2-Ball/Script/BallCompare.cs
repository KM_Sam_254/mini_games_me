﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BallCompare : MonoBehaviour {

    private GameObject[] ball1;
    private GameObject[] ball2;
    [HideInInspector]
    public GameObject next;
    public int index = 0;
    public balls easy;
    public balls normal;
    public balls hard;
    public Sprite[] ballColor;   
    public static int currentball;
    public Text currentRd;
    public Text totalRd;
   // [HideInInspector]
    public Dropdown dropdown;
    public GameObject payment;   
    public OldDemantia.QuestionHandler qusLeft;
	public  OldDemantia.QuestionHandler qusRight;
    public static int total_left;
    public static int total_right;

    void Start()
    {
        ball1 = new GameObject[10];
        ball1 = GameObject.FindGameObjectsWithTag("Balls");
        ball2 = new GameObject[10];
        ball2 = GameObject.FindGameObjectsWithTag("Balls2");       
        Score.TimerStart = true;
        next.GetComponent<Button>().interactable = false;
        Difficulty();
    }

    public void GameStart()
    {
		if (index < 15) {
			Difficulty();
		} else {
			GameEnd ();
		}
        
    }


    void Difficulty()
    {
        if (dropdown.value == 0)
        {
            qusLeft.Split(easy.left, index);
            qusRight.Split(easy.right, index);
        }
        else if (dropdown.value == 1)
        {
            qusLeft.Split(normal.left, index);
            qusRight.Split(normal.right, index);
        }
        else if (dropdown.value == 2)
        {
            qusLeft.Split(hard.left, index);
            qusRight.Split(hard.right, index);
        }
        AssignBalls();
    }

    void AssignBalls()
    {
        for (int i = 0; i < 5; i++)
        {
            ball1[i].SetActive(true);
            ball2[i].SetActive(true);
            ball1[i].GetComponent<SpriteRenderer>().sprite = ballColor[qusLeft.q[i]];
            ball2[i].GetComponent<SpriteRenderer>().sprite = ballColor[qusRight.q[i]];
            float left_size = Random.Range(1f, 2f);
            float right_size = Random.Range(1f, 2f);
            ball1[i].transform.localScale = new Vector2(left_size, left_size);
            ball2[i].transform.localScale = new Vector2(right_size, right_size);
        }
        total_left = qusLeft.q[5];
        total_right = qusRight.q[5];
        Debug.Log(total_left);
        Debug.Log(total_right);
    }

    void HideBalls()
    {
        for (int i = 0; i < 10; i++)
        {
            ball1[i].SetActive(false);
            ball2[i].SetActive(false);
            ball1[i].transform.position = new Vector2(Random.Range(-7f, -1f), 7f);
            ball2[i].transform.position = new Vector2(Random.Range(0f, 5f), 7f);
            ball1[i].transform.localScale = new Vector2(1f, 1f);
            ball2[i].transform.localScale = new Vector2(1f, 1f);
        }
    }

    public void CheckANS()
    {
        if (total_left == total_right)
        {
            AnswerSound.answer = "right";
        }
        else
        {
            AnswerSound.answer = "wrong";
        }
        StartCoroutine(Next());
    }

    IEnumerator Next()
    {
        yield return new WaitForSeconds(3f);
		index++;
        HideBalls();
        GameStart();
    }

	void GameEnd()
	{
		SceneManager.LoadScene ("Complete");
        Result.image = 1;
        Result.title = 1;
		// payment.SetActive(true);
	}

    

//    void End()
//    {
//        Score.TimerStart = false;
//        next.SetActive(false);
//        payment.SetActive(true);
//        Score.TotalQuestion = easy.left.Length;
//        PlayerPrefs.SetString("1-2_Title_1stPt", Score.RightAnswer.ToString());
//        string date = System.DateTime.Now.Date.ToString();
//        PlayerPrefs.SetString("1-2_Title_1stDt", date);
//    }
}

[System.Serializable]
public class balls
{
    [HideInInspector]
    public int no;
    public string[] left;
    public string[] right;
}
 