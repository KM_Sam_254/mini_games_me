﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetroTile : MonoBehaviour
{
    private Vector2 startingPosition;
    private List<Transform> touchingTiles;
//    private Transform myParent;
    //   private AudioSource audSource;

    private void Awake()
    {
        startingPosition = transform.position;
        touchingTiles = new List<Transform>();
//        myParent = transform.parent;
        //        audSource = SpriteRenderer.GetComponent<AudioSource>();
    }

    public void PickUp()
    {
        transform.localScale = new Vector3(2f,2f,2f);
        gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder = 2;
    }
    public void Drop()
    {
        transform.localScale = new Vector2(2f, 2f);
        gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder = 1;

        Vector2 newPosition;
        if (touchingTiles.Count == 0)
        { 
            transform.position = startingPosition;
            transform.localScale = new Vector3(1f, 1f, 1f);
            //     transform.parent = myParent;
            return;
        }

        var currentCell = touchingTiles[0];
        if (touchingTiles.Count == 1)
        {
            newPosition = currentCell.position;
        }
        else
        {
            var distance = Vector2.Distance(transform.position, touchingTiles[0].position);

            foreach (Transform cell in touchingTiles)
            {
                if (Vector2.Distance(transform.position, cell.position) < distance)
                {
                    currentCell = cell;
                    distance = Vector2.Distance(transform.position, cell.position);
                }
            }
            newPosition = currentCell.position;
        }
        if (currentCell.childCount != 0)
        {
            //     transform.position = startingPosition;
 //           transform.parent = myParent;
            return;
        }
        else
        {
  //          transform.parent = currentCell;
            StartCoroutine(SlotIntoPlace(transform.position, newPosition));
        }

    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Slot") return;
        if (!touchingTiles.Contains(other.transform))
        {
            touchingTiles.Add(other.transform);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag != "Slot") return;
        if (touchingTiles.Contains(other.transform))
        {
            touchingTiles.Remove(other.transform);
        }
    }

    IEnumerator SlotIntoPlace(Vector2 startingPos, Vector2 endingPos)
    {
        float duration = 0.02f;
        float elapsedTime = 0;
        //        audSource.Play();
        while (elapsedTime < duration)
        {
            transform.position = Vector2.Lerp(startingPos, endingPos, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        transform.position = endingPos;
    }
}
