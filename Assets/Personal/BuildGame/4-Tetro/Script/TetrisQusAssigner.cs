﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TetrisQusAssigner : MonoBehaviour {

    public TetrisGrid[] tetrisGrid;
    public TetrisGrid[] normal;
    public TetrisGrid[] hard;
  //  private GameObject payment;
    [HideInInspector]
    public string[] combineRow;
    [HideInInspector]
    public string[] splitRow;
    [HideInInspector]
    public int[] grid;
    [HideInInspector]
    public GameObject[] tableAssign;
    [HideInInspector]
    public GameObject[] tetris;
    public TetroInputManager reset;

    public int index = 0;
    private Dropdown dropdown;
    private int rightAns;
    private List<int> tempAns;

    void Start()
    {
        dropdown = GameObject.Find("Dropdown").GetComponent<Dropdown>();
//        payment = GameObject.Find("PaymentPanel").transform.GetChild(0).gameObject;
        tableAssign = new GameObject[this.transform.childCount];
        tempAns = new List<int>();
        for (int i = 0; i < tableAssign.Length; i++)
        {
            tableAssign[i] = this.transform.GetChild(i).gameObject;
        }
        dropdown.value = 2;
        Next();
    }

    void Difficulty()
    {
        if (dropdown.value == 0)
        {
            CombineAll(tetrisGrid);
            ShowTetris(tetrisGrid);
        }
        else if (dropdown.value == 1)
        {
            CombineAll(normal);
            ShowTetris(normal);
        }
        else if (dropdown.value == 2)
        {
            CombineAll(hard);
            ShowTetris(hard);
        }
    }

    public void Next()
    {
        HideAll();
        HideTetris();        
        if (index < 15)
        {
            Difficulty();
            SplitAll();
            AssignTable();
            index++;
        }
        else
        {
			GameEnd ();
        }
    }

    void CombineAll(TetrisGrid[] mode)
    {
        for (int i = 0; i < mode[index].row.Length; i++)
        {
            combineRow[index] += mode[index].row[i];
        }       
    }

    void SplitAll()
    {
        splitRow = combineRow[index].Split('.');
        for (int i = 0; i < splitRow.Length; i++)
        {
            grid[i] = int.Parse(splitRow[i]);
        }
    }

    void AssignTable()
    {
        for (int i = 0; i < splitRow.Length; i++)
        {
            tableAssign[i].SetActive(true);
            if (grid[i] == 1)
            {
                tableAssign[i].transform.GetChild(0).gameObject.SetActive(true);              
                tempAns.Add(i);
            }
        }               
    }

    void HideAll()
    {
        for (int i = 0; i < tableAssign.Length; i++)
        {
            tableAssign[i].transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    void ShowTetris(TetrisGrid[] mode)
    {
        tetris[mode[index].drag1].tag = "Drag";
        tetris[mode[index].drag1].transform.GetChild(0).GetComponent<SpriteRenderer>().color =
                new Color(1f, 1f, 1f, 1f);

        tetris[mode[index].drag2].tag = "Drag";        
        tetris[mode[index].drag2].transform.GetChild(0).GetComponent<SpriteRenderer>().color =
                new Color(1f, 1f, 1f, 1f);
    }

    void HideTetris()
    {
        for (int i = 0; i < tetris.Length; i++)
        {
            tetris[i].tag = "Untagged";
            tetris[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color =
                new Color(1f, 1f, 1f, 0.05f);
        }
    }

    public void CheckAnswer()
    {        
        for (int i = 0; i < grid.Length; i++)
        {
            if (tableAssign[i].transform.GetChild(0).gameObject.activeInHierarchy == true
                && tableAssign[i].GetComponent<Button>().interactable == true)
            {
                rightAns += 1;
            }
            if (tableAssign[i].transform.GetChild(0).gameObject.activeInHierarchy == false
                && tableAssign[i].GetComponent<Button>().interactable == true)
            {
                rightAns -= 1;
            }
        }        
        if (rightAns == tempAns.Count )
        {
            AnswerSound.answer = "right";
        }
        else
        {
            AnswerSound.answer = "wrong";
        }        
        StartCoroutine(NextRound());
    }

    IEnumerator NextRound()
    {
        yield return new WaitForSeconds(3f);
        tempAns.Clear();
        rightAns = 0;
        Next();
        reset.Reset();              
    }

	void GameEnd()
	{
        Result.title = 3;
        Result.image = 3;
		SceneManager.LoadScene ("Complete");
		// payment.SetActive(true);
	}
}

[System.Serializable]
public class TetrisGrid
{
    public string[] row;
    public int drag1;
    public int drag2;
}



