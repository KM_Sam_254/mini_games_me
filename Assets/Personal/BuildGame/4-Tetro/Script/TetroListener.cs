﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TetroListener : MonoBehaviour {

    private Button qusBtn;
    private string tile;
    public static bool[] isTrue;

    private void Start()
    {
        tile = "QusTile ("+this.gameObject.name+")";
        qusBtn = GameObject.Find(tile).GetComponent<Button>();
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Drag"))
        {
            qusBtn.GetComponent<Image>().color = new Color(0.9f, 0.8f, 0.7f, 1f);
            if (qusBtn.transform.GetChild(0).gameObject.activeInHierarchy == true)
            {
                qusBtn.interactable = true;
            }
            
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        qusBtn.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
        qusBtn.interactable = false;
    }
}
