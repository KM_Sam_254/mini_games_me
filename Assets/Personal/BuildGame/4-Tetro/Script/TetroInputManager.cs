﻿using UnityEngine;
using System.Collections;

public class TetroInputManager : MonoBehaviour
{
    private bool draggingItem = false;
    private GameObject draggedObject;
    private Vector2 touchOffset;
    public GameObject[] startingPento1;
    public GameObject[] startingPento2;
    public GameObject[] startingPento3;
    public GameObject[] startingPento4;
    public Transform[] allPos;

    void Update()
    {
        if (HasInput)
        {
            DragOrPickUp();
        }
        else
        {
            if (draggingItem)
                DropItem();
        }
    }

    Vector2 CurrentTouchPosition
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    } 

    private void DragOrPickUp()
    {
        var inputPosition = CurrentTouchPosition;
        if (draggingItem)
        {
            draggedObject.transform.position = inputPosition + touchOffset;
        }
        else
        {
            RaycastHit2D[] touches = Physics2D.RaycastAll(inputPosition, inputPosition, 0.5f, LayerMask.GetMask("PickUp"));
            if (touches.Length > 0)
            {
                var hit = touches[0];
                if (hit.transform != null && hit.transform.tag == "Drag")
                {
                    draggingItem = true;
                    draggedObject = hit.transform.gameObject;                
                    touchOffset = (Vector2)hit.transform.position - inputPosition;
                    draggedObject.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 2;
                    hit.transform.GetComponent<TetroTile>().PickUp();
                }
            }            
        }
    }

    private bool HasInput
    {
        get
        {
            return Input.GetMouseButton(0);
        }
    }

    public void Rotate()
    {
        if (draggedObject != null)
        {
            draggedObject.transform.Rotate(0, 0, -90);
        }
    }

    public void Reverse()
    {
        if (draggedObject != null)
        {
            draggedObject.transform.Rotate(0, -180, 0);
        }
    }

    public void DropItem()
    {
        draggingItem = false;
        draggedObject.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 1;
        draggedObject.GetComponent<TetroTile>().Drop();
    }

    public void Reset()
    {    
        for (int i = 0; i < startingPento1.Length; i++)
        {
            startingPento1[i].transform.position = allPos[i].position;
            startingPento1[i].transform.rotation = Quaternion.identity;
            startingPento1[i].transform.localScale = new Vector2(1f, 1f);

            //startingPento2[i].transform.position = allPos[i].position;
            //startingPento2[i].transform.rotation = Quaternion.identity;
            //startingPento2[i].transform.localScale = new Vector2(1f, 1f);

            //startingPento3[i].transform.position = allPos[i].position;
            //startingPento3[i].transform.rotation = Quaternion.identity;
            //startingPento3[i].transform.localScale = new Vector2(1f, 1f);

            //startingPento4[i].transform.position = allPos[i].position;
            //startingPento4[i].transform.rotation = Quaternion.identity;
            //startingPento4[i].transform.localScale = new Vector2(1f, 1f);

        }

    }
}






