﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MtileChecker : MonoBehaviour
{
    public GameObject next;
    public Transform[] fruitsBox;
    public GameObject[] destFruits;
    public FruitQus fruitQus;
    public static int isFull = 0 ;
    public static int rightCount;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Tile") && 
            this.GetComponent<SpriteRenderer>().sprite == other.GetComponent<SpriteRenderer>().sprite)
        {
            this.GetComponent<SpriteRenderer>().sprite = other.GetComponent<SpriteRenderer>().sprite;
            this.GetComponent<SpriteRenderer>().sortingOrder = 2;
            other.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            other.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            isFull += 1;
            rightCount += 1;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = other.GetComponent<SpriteRenderer>().sprite;
            this.GetComponent<SpriteRenderer>().sortingOrder = 2;
            other.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            other.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            isFull += 1;
        }
        CheckAns();
    }

    void ReturnOriginBox()
    {        
        isFull = 0;
        for (int i = 0; i < fruitsBox.Length; i++)
        {
            fruitsBox[i].transform.GetChild(0).transform.position = fruitsBox[i].position;
        }
    }

    void CheckAns()
    {
        if (isFull == 2 && rightCount == 2)
        {
            AnswerSound.answer = "right";
            Score.RightAnswer += 1;
            StartCoroutine(ShowRight());
        }
        else if (isFull == 2 && rightCount != 2)
        {
            AnswerSound.answer = "wrong";
            StartCoroutine(ShowRight());
        }
    }

    IEnumerator ShowRight()
    {       
        yield return new WaitForSeconds(3f);
        for (int i = 0; i < fruitsBox.Length; i++)
        {
            iTween.MoveTo(fruitsBox[i].transform.GetChild(0).gameObject,
                iTween.Hash("y", -10f, "easeType", "linear", "time", 0.2f));
        }
        for (int i = 0; i < destFruits.Length; i++)
        {
            destFruits[i].GetComponent<SpriteRenderer>().enabled = false;
        }
        fruitQus.no = 6;
        yield return new WaitForSeconds(1f);
        ReturnOriginBox();  
       // next.GetComponent<Button>().interactable = true;
    }
}