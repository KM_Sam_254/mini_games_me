﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class FruitQus : MonoBehaviour {

    public Dropdown difficulty;
    public string[] easyRound;
    public string[] normalRound;
    public string[] round;
    public GameObject payment;
    private string[] temp_round;
    private int[] from_round;
    public int index = 0;
    public Slider[] slider;
    public GameObject[] slide;
    public GameObject[] qusFruits;
    public GameObject[] ansFruits1;
    public GameObject[] ansFruits2;
    public GameObject[] destFruits;
    public Transform[] destPOS;
    public Sprite[] fruits;
    public Text currentRd;
    public Text totalRd;
    public GameObject fruitQ;
    private int[] temp;
    private float speed = 15f;
    float delay = 2f;
    public float timer;
    public int no;
    private int count1;
    private int count2;
    public List<int> shoot_fruit;
    public int shoot_count = 5;

    void Start()
    {
        temp = new int[3];
        temp_round = new string[8];
        from_round = new int[8];
        Score.TimerStart = true;
        shoot_fruit = new List<int>();
        AssignFruits();
    }

    void Update()
    {
		slide[0].transform.position = 
			new Vector2(slide[0].transform.position.x, slider[0].value);
		slide[1].transform.position = 
			new Vector2(slide[1].transform.position.x, slider[1].value);  

    }

    void Speed_Change()
    {
        if (difficulty.value == 0)
        {
            delay = 5f;
            SplitQus(index, easyRound);
        }
        else if (difficulty.value == 1)
        {
            delay = 4f;
            SplitQus(index, normalRound);
        }
        else if (difficulty.value == 2)
        {
            delay = 3f;
            SplitQus(index, round);
        }
        currentRd.text = (index + 1).ToString();
        totalRd.text = round.Length.ToString();
    }

    public void AssignFruits()
    {
        Debug.Log("Here");
        if (index < 15)
        {
            timer = 0f;
            Speed_Change();
            Assign();
        }
        else
        {
            GameEnd();            
            UnsortAll();
        }
        index++;
    }

    void SplitQus(int no, string[] round)
    {
        if (!string.IsNullOrEmpty(round[no]))
        {
            temp_round = round[no].Split('.');
            for (int i = 0; i < temp_round.Length; i++)
            {
                int.TryParse(temp_round[i], out from_round[i]);
            }
        }
        else
        {
            Debug.Log("Insert Round");
        }
    }

    void Assign()
    {
        for (int i = 0; i < qusFruits.Length; i++)
        {
            qusFruits[i].GetComponent<SpriteRenderer>().sprite = fruits[from_round[i]];
            destFruits[i].GetComponent<SpriteRenderer>().sprite = fruits[from_round[i]];                        
        }
        
        AssignAns();
        InvokeRepeating("ShootFruit", 1f, delay);
    }

    void AssignAns()
    {
        if (index == 0 || index == 5 || index == 10)
        {
            for (int i = 1; i <= 4; i++)
            {
                ansFruits1[count1].GetComponent<SpriteRenderer>().sprite = fruits[from_round[i]];
                shoot_fruit.Add(i);
                count1++;
            }
            for (int j = 4; j <= 7; j++)
            {
                ansFruits2[count2].GetComponent<SpriteRenderer>().sprite = fruits[from_round[j]];
                shoot_fruit.Add(j);
                count2++;
            }
        }
        else if (index == 1 || index == 6 || index == 11)
        {
            for (int i = 4; i <= 7; i++)
            {
                ansFruits1[count1].GetComponent<SpriteRenderer>().sprite = fruits[from_round[i]];
                shoot_fruit.Add(i);
                count1++;
            }
            for (int j = 1; j <= 4; j++)
            {
                ansFruits2[count2].GetComponent<SpriteRenderer>().sprite = fruits[from_round[j]];
                shoot_fruit.Add(j);
                count2++;
            }
        }
        else if (index == 2 || index == 7 || index == 12)
        {
            for (int i = 2; i <= 5; i++)
            {
                ansFruits1[count1].GetComponent<SpriteRenderer>().sprite = fruits[from_round[i]];
                shoot_fruit.Add(i);
                count1++;
            }
            for (int j = 4; j <= 7; j++)
            {
                ansFruits2[count2].GetComponent<SpriteRenderer>().sprite = fruits[from_round[j]];
                shoot_fruit.Add(j);
                count2++;
            }
        }

        else if (index == 3 || index == 8 || index == 13)
        {
            for (int i = 0; i <= 3; i++)
            {
                ansFruits1[count1].GetComponent<SpriteRenderer>().sprite = fruits[from_round[i]];
                shoot_fruit.Add(i);
                count1++;
            }
            for (int j = 2; j <= 5; j++)
            {
                ansFruits2[count2].GetComponent<SpriteRenderer>().sprite = fruits[from_round[j]];
                shoot_fruit.Add(j);
                count2++;
            }
        }

        else if (index == 4 || index == 9 || index == 14)
        {
            for (int i = 3; i <= 6; i++)
            {
                ansFruits1[count1].GetComponent<SpriteRenderer>().sprite = fruits[from_round[i]];
                shoot_fruit.Add(i);
                count1++;
            }
            for (int j = 1; j <= 4; j++)
            {
                ansFruits2[count2].GetComponent<SpriteRenderer>().sprite = fruits[from_round[j]];
                shoot_fruit.Add(j);
                count2++;
            }
        }
        temp[0] = Random.Range(0, 4);
        temp[1] = Random.Range(0, 4);

        shoot_fruit = shoot_fruit.Distinct().ToList();

        ansFruits1[temp[0]].GetComponent<SpriteRenderer>().sortingOrder = 0;
        ansFruits2[temp[1]].GetComponent<SpriteRenderer>().sortingOrder = 0;
        ansFruits1[temp[0]].GetComponent<BoxCollider2D>().enabled = true;
        ansFruits2[temp[1]].GetComponent<BoxCollider2D>().enabled = true;
    }

    void ShootFruit()
    {
        if (no < shoot_count )
        {
            qusFruits[shoot_fruit[no]].GetComponent<SpriteRenderer>().sortingOrder = 3;
            destFruits[shoot_fruit[no]].GetComponent<SpriteRenderer>().enabled = true;
            destFruits[shoot_fruit[no]].GetComponent<BoxCollider2D>().enabled = true;
            iTween.MoveFrom(destFruits[shoot_fruit[no]], iTween.Hash("position", qusFruits[shoot_fruit[no]].transform.position,
                "easeType", "linear", "time", delay + 5f, "oncomplete", "UnsortAll", "oncompletetarget", fruitQ));
            iTween.RotateBy(destFruits[shoot_fruit[no]], iTween.Hash
                    ("z", -5f, "easeType", "linear", "time", speed, "loopType", "loop"));            
        } 
        no++;
    }

    void UnsortAll()
    {
        Debug.Log(no);
        if (no > shoot_count + 1)
        {            
            for (int i = 0; i < qusFruits.Length; i++)
            {
                qusFruits[i].GetComponent<SpriteRenderer>().sortingOrder = 3;            
                //ansFruits[i % ansFruits.Length].GetComponent<BoxCollider2D>().enabled = false;

                qusFruits[i].GetComponent<SpriteRenderer>().enabled = true;           
            }
            for (int i = 0; i < destFruits.Length; i++)
            {
                iTween.MoveTo(destFruits[i], destPOS[i].position, 0f);
                destFruits[i].GetComponent<BoxCollider2D>().enabled = true;
            }
            for (int i = 0; i < ansFruits1.Length; i++)
            {
                ansFruits1[i].GetComponent<SpriteRenderer>().sortingOrder = 2;
                ansFruits2[i].GetComponent<SpriteRenderer>().sortingOrder = 2;
                ansFruits1[i].GetComponent<BoxCollider2D>().enabled = false;
                ansFruits2[i].GetComponent<BoxCollider2D>().enabled = false;
                count1 = 0;
                count2 = 0;
            }
            shoot_fruit.Clear();
            no = 0;
            CancelInvoke();
            AssignFruits();
        }
    }

	void GameEnd()
	{
		SceneManager.LoadScene ("Complete");
        Result.image = 2;
        Result.title = 2;
		// payment.SetActive(true);
	}

    void End()
    {
        payment.SetActive(true);
       // LockChapter.currentChapter = 1;
        Score.TimerStart = false;
        Score.TotalQuestion = 15;
        PlayerPrefs.SetString("1-1_Title_1stPt", Score.RightAnswer.ToString());
        string date = System.DateTime.Now.Date.ToString();
        PlayerPrefs.SetString("1-1_Title_1stDt", date);
    }        
}  
