﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TruckOld : MonoBehaviour {

    public GameObject truck;
    public GameObject wheelF;
    public GameObject wheelB;
    public Transform endpos;
    public GameObject cube;
    public GameObject backgroundA;
    public GameObject backgroundB;
    private float speed = 0.08f;
    public float wait;
    private bool go = false;
    private bool move = false;
    private float timer = 0f;
    private Dropdown difficulty;

    void Start()
    {
        difficulty = GameObject.FindObjectOfType<Dropdown>();
        Game_Start();
    }

    public void Game_Start()
    {
        StartCoroutine(Drive());
    }

    void Update()
    {
        if (go)
        {            
            wheelF.transform.Rotate(0f, 0f, -15f);
            wheelB.transform.Rotate(0f, 0f, -15f);
            backgroundA.transform.Translate(-0.01f, 0f, 0f);
            backgroundB.transform.Translate(-0.01f, 0f, 0f); 
        }
        if (move)
        {
            cube.transform.Translate(0f, speed * Time.deltaTime, 0f);
            timer += 1f * Time.deltaTime;
            if (timer > 5f)
            {            
                move = false;
                cube.GetComponent<SpriteRenderer>().enabled = true;
                timer = 0f;
            }           
        }
    }

    IEnumerator Drive()
    {
        yield return new WaitForSeconds(wait);
        go = true;
        iTween.MoveAdd(truck,iTween.Hash("y", 0.9f, "easeType", "easeInOutBounce", "loopType", "pingPong", "time", 3f));
        iTween.MoveTo(truck,iTween.Hash("position",endpos.position, "easeType", "linear", "loopType", "loop", "time", 14f));
    }

    public void MoveCube()
    {          
        if (difficulty.value == 0)
        {
            speed = 6f;
        }
        if (difficulty.value == 1)
        {
            speed = 4f;
        }
        if (difficulty.value == 2)
        {
            speed = 2f;
        }
        move = true;
    }
}
