﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OriginCube : MonoBehaviour {

    private GameObject cubeTruckRandom;
    private GameObject temp;

    void Start()
    {
        cubeTruckRandom = GameObject.Find("Allcubes");  
    }

    void OnTriggerEnter2D(Collider2D other)
    {    
        if (other.CompareTag("Cover"))
        {                  
            iTween.PunchScale(gameObject, new Vector3(0.8f, 0.8f, 0f), 1f);
            this.GetComponent<SpriteRenderer>().sprite = other.GetComponent<SpriteRenderer>().sprite;
            temp = other.gameObject;
            temp.GetComponent<BoxCollider2D>().enabled = false;
            other.GetComponent<SpriteRenderer>().enabled = false; 
            StartCoroutine("NewSlot");
        }
    }
    IEnumerator NewSlot()
    {
        if (temp.GetComponent<SpriteRenderer>().sprite ==
            cubeTruckRandom.GetComponent<CubeTruckRandom>().tempSprite)
        {
            AnswerSound.answer = "right";
            Score.RightAnswer += 1;
        }
        else if(cubeTruckRandom.GetComponent<CubeTruckRandom>().is_review == false)
        {  
            AnswerSound.answer = "wrong";
            cubeTruckRandom.GetComponent<CubeTruckRandom>().wrongAns.Add
            (cubeTruckRandom.GetComponent<CubeTruckRandom>().index - 1);
        }

        yield return new WaitForSeconds(2f);
        cubeTruckRandom.GetComponent<CubeTruckRandom>().AssignCubes();
        temp.GetComponent<BoxCollider2D>().enabled = true;
    }
}
