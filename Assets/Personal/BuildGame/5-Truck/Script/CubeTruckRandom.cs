﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CubeTruckRandom : MonoBehaviour {

    public GameObject panel;
    [HideInInspector]
    public GameObject[] allcubes;
 //   [HideInInspector]
    public Sprite[] cubeColor;
    [HideInInspector]
    public Sprite tempSprite;
    public int index;
    [HideInInspector]
    public bool is_review = false;
    [HideInInspector]
    public GameObject review;
    public SpriteRenderer[] cubes;    
    public string[] roundEasy;    
    public string[] roundNormal;
    public string[] roundHard;
    public List<int> wrongAns;
    private int[] from_string;
    private string[] temp_round;
    private int blank;
    private Text current_round;
    private Text total_round;    
    public Dropdown dropdown;

    void Start()
    {
        temp_round = new string[8];
        from_string = new int[8];
        current_round = GameObject.Find("CurrentRound").GetComponent<Text>();
        total_round = GameObject.Find("TotalRound").GetComponent<Text>();
        total_round.text = roundHard.Length.ToString();
        Score.TimerStart = true;
        index = 0;
        AssignCubes();
    }

    public void AssignCubes()
    {
        if (index < roundHard.Length)
        {
            StartGame();
        }
        else
        {
            GameEnd();
        }        
    }   
    
    void StartGame()
    {
        total_round.text = roundHard.Length.ToString();
        current_round.text = (index + 1).ToString();
        if (dropdown.value == 0)
        {
            SplitQus(index, roundEasy);
        }
        else if (dropdown.value == 1)
        {
            SplitQus(index, roundNormal);
        }
        else if (dropdown.value == 2)
        {
            SplitQus(index, roundHard);
        }
        index++;
    }

    void SplitQus(int no, string[] game_mode)
    {        
        if (!string.IsNullOrEmpty(game_mode[no]))
        {
            ClearCubes();
            temp_round = game_mode[no].Split('.');
            for (int i = 0; i < temp_round.Length; i++)
            {
                int.TryParse(temp_round[i], out from_string[i]);
            }
            Assign();
        }
        else
        {
            HideAll();
            Debug.Log("Insert Round");
        }
    }

    void Assign()
    {
        blank = Random.Range(4, 7);        
        for (int i = 0; i < allcubes.Length; i++)
        {
            allcubes[i].GetComponent<SpriteRenderer>().sprite = cubeColor[from_string[i]];
            allcubes[i].GetComponent<BoxCollider2D>().enabled = false;
            cubes[i % cubes.Length].sprite = cubeColor[from_string[i % cubes.Length]];
        }
        tempSprite = cubeColor[from_string[blank]];
        allcubes[blank].GetComponent<SpriteRenderer>().sprite = cubeColor[5];
        allcubes[blank].GetComponent<BoxCollider2D>().enabled = true;
    }

    void EndGame()
    {
        PlayerPrefs.SetString("1-14_Title_1stPt", Score.RightAnswer.ToString());
        string date = System.DateTime.Now.Date.ToString();
        PlayerPrefs.SetString("1-14_Title_1stDt", date);
        Score.TimerStart = false;
        Score.TotalQuestion = roundHard.Length;
        panel.SetActive(true);
        is_review = true;
        index = 0;
    }

	void GameEnd()
	{
        Result.image = 4;
        Result.title = 4;
		SceneManager.LoadScene ("Complete");
		// payment.SetActive(true);
	}

    void HideAll()
    {
        for (int i = 0; i < allcubes.Length; i++)
        {            
            allcubes[i].GetComponent<SpriteRenderer>().sprite = cubeColor[5];
            allcubes[i].GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    void ClearCubes()
    {
        for (int i = 0; i < from_string.Length; i++)
        {
            from_string[i] = 5;
        }
    }

    void Reivew()
    {
        if (index < wrongAns.Count && is_review == true)
        {
            total_round.text = roundHard.Length.ToString();
            current_round.text = (wrongAns[index] + 1).ToString();
            SplitQus(wrongAns[index], roundHard);
            index++;
            
        }
        else if (index == wrongAns.Count && is_review == true)
        {
            Score.TotalQuestion = wrongAns.Count;
            panel.SetActive(true);
            review.SetActive(false);
        }
    }
}

