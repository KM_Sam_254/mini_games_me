﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetrunOrignBG : MonoBehaviour {

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Tile"))
        {
            other.transform.position = new Vector2(25f, 0f);
        } 
        if (other.CompareTag("Cover"))
        {
            other.GetComponent<SpriteRenderer>().enabled = true;
        }
        if (other.CompareTag("Slot"))
        {             
            other.GetComponent<TruckOld>().cube.transform.position = new Vector2((other.GetComponent<TruckOld>().truck.transform.position.x)-0.5f,
                (other.GetComponent<TruckOld>().transform.position.y) + 0.5f);
        } 
    }
}
