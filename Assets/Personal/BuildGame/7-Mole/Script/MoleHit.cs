﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoleHit : MonoBehaviour {

    public Image up;
    public Sprite down;

    public void Hit()
    {
        if (this.GetComponent<Image>().enabled && this.GetComponent<Image>().sprite.name.Equals("Mole_0"))
        {
            //MoleScore.scoreMole += 5;
            iTween.PunchScale(up.gameObject, new Vector3(1.2f, 1.2f, 0f), 0.5f);
            up.sprite = down;
            MLadderLight.instance.AnswerChecker(transform.parent.name);
        }
    }
}
