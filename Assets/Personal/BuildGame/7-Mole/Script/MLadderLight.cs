﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MLadderLight : MonoBehaviour
{

    public GameObject allBtms;
    public GameObject allQusts;
    public GameObject result;

    [HideInInspector]
    public Sprite up;
    [HideInInspector]
    public Dropdown difficulty;
    [HideInInspector]
    public GameObject panelAll;
    [HideInInspector]
    public GameObject panelQus;
    public float score, delay_Time;
    private float yield;
    private int gameMode = 9;
    private int moleNo = 2;
    private int round = 4;
    public int question = 15;
    public int answer = 0;
    public static MLadderLight instance;
    Transform[] allBtm;
    Transform[] allQus;
    bool answer_Check = true;

    void Start()
    {
        instance = this;
        //difficulty.captionText.text = Lean.Localization.LeanLocalization.GetTranslationText("Easy");
        //difficulty.options[0].text = Lean.Localization.LeanLocalization.GetTranslationText("Easy");
        //difficulty.options[1].text = Lean.Localization.LeanLocalization.GetTranslationText("Normal");
        //difficulty.options[2].text = Lean.Localization.LeanLocalization.GetTranslationText("Hard");
		Init();
    }

    public void Init()
    {
        Score.TimerStart = true;
        score = 0;
        allBtm = new Transform[allBtms.transform.childCount];
        allQus = new Transform[allQusts.transform.childCount];

        for (int i = 0; i < allBtms.transform.childCount; i++)
        {
            allBtm[i] = allBtms.transform.GetChild(i);
        }
        for (int i = 0; i < allQusts.transform.childCount; i++)
        {
            allQus[i] = allQusts.transform.GetChild(i);
        }

        foreach (var item in allBtm)
        {
            item.GetComponent<Button>().interactable = false;
        }
        Difficulty();
    }

    void Update()
    {
        if (answer == 15)
        {
            StartCoroutine(Set_Score());
        }           
    }

    IEnumerator Set_Score()
    {
        Score.TimerStart = false;
        StopCoroutine("RandomMoles");
        Score.RightAnswer = score;
        Score.TotalQuestion = question;
        PlayerPrefs.SetString("1-4_Title_1stPt", Score.RightAnswer.ToString());
        string date = System.DateTime.Now.Date.ToString();
        PlayerPrefs.SetString("1-4_Title_1stDt", date);
        Result.image = 6;
        Result.title = 6;
		SceneManager.LoadScene ("Complete");
        yield return new WaitForSeconds(5f);

    }

    public void AnswerChecker(string mole_Name)
    {
        for (int i = 0; i < gameMode; i++)
        {
            if (mole_Name.Equals(allQus[i].transform.name) && allQus[i].transform.GetChild(0).GetComponent<Image>().enabled)
            {
                score++;
                AnswerSound.answer = "right";
                answer_Check = true;
                Result.right += 1;
                StopCoroutine("RandomMoles");
                StartCoroutine("RandomMoles", delay_Time);
                return;
            }
            AnswerSound.answer = "wrong";
            StopCoroutine("RandomMoles");
            StartCoroutine("RandomMoles", delay_Time);
        }       
    }

    public void NextBtn()
    {        
        result.SetActive(false);
        round = 0;
        score = 0;
        answer = 0;        
        StopCoroutine("RandomMoles");
        StartCoroutine("RandomMoles", delay_Time);
    }

    void Difficulty()
    {
        HideAll();
        if (difficulty.value == 0)
        {
            gameMode = 9;
            moleNo = 2;
            yield = 7f;
            for (int i = 0; i < gameMode; i++)
            {
                allBtm[i].transform.gameObject.SetActive(true);
                allQus[i].transform.gameObject.SetActive(true);
            }
            panelAll.GetComponent<GridLayoutGroup>().cellSize = new Vector2(200f, 200f);
            panelQus.GetComponent<GridLayoutGroup>().cellSize = new Vector2(200f, 200f);
        }
        else if (difficulty.value == 1)
        {
            gameMode = 9;
            moleNo = 3;
            delay_Time = 2f;
            yield = 5f;
            for (int i = 0; i < gameMode; i++)
            {
                allBtm[i].transform.gameObject.SetActive(true);
                allQus[i].transform.gameObject.SetActive(true);
            }
            panelAll.GetComponent<GridLayoutGroup>().cellSize = new Vector2(200f, 200f);
            panelQus.GetComponent<GridLayoutGroup>().cellSize = new Vector2(200f, 200f);
        }
        else if (difficulty.value == 2)
        {
            gameMode = 9;
            moleNo = 2;
            delay_Time = 2f;
            yield = 3f;
            for (int i = 0; i < gameMode; i++)
            {
                allBtm[i].transform.gameObject.SetActive(true);
                allQus[i].transform.gameObject.SetActive(true);
            }
            panelAll.GetComponent<GridLayoutGroup>().cellSize = new Vector2(200f, 200f);
            panelQus.GetComponent<GridLayoutGroup>().cellSize = new Vector2(200f, 200f);
        }
        /*
        else if (difficulty.value == 1)
        {
            gameMode = 16;
            moleNo = 4;
            for (int i = 0; i < gameMode; i++)
            {
                allBtm[i].transform.gameObject.SetActive(true);
                allQus[i].transform.gameObject.SetActive(true);
            }
            panelAll.GetComponent<GridLayoutGroup>().cellSize = new Vector2(150f, 150f);
            panelQus.GetComponent<GridLayoutGroup>().cellSize = new Vector2(150f, 150f);
        }
        else if (difficulty.value == 2)
        {
            gameMode = 36;
            moleNo = 5;
            for (int i = 0; i < gameMode; i++)
            {
                allBtm[i].SetActive(true);
                allQus[i].SetActive(true);
            }
            panelAll.GetComponent<GridLayoutGroup>().cellSize = new Vector2(100f, 100f);
            panelQus.GetComponent<GridLayoutGroup>().cellSize = new Vector2(100f, 100f);
        }*/
        NextBtn();
    }

    IEnumerator RandomMoles()
    {        
        round = 4;
        yield return new WaitForSeconds(1f);        
        //while (timer < 60f)
        while (answer < question)
        {
            round++;
            foreach (var item in allBtm)
            {
                item.transform.GetChild(0).GetComponent<Image>().enabled = false;
                item.GetComponent<Button>().interactable = false;
            }

            if (round > 3)
            {                
                foreach (var item in allQus)
                {
                    item.transform.GetChild(0).GetComponent<Image>().enabled = false;
                    item.GetComponent<Button>().interactable = false;
                }
                answer++;
            }

            if (answer_Check)
            {
                for (int i = 0; i < moleNo; i++)
                {
                    int pos = Random.Range(0, gameMode);
                    allBtm[pos].transform.GetChild(0).GetComponent<Image>().sprite = up;
                    allBtm[pos].transform.GetChild(0).GetComponent<Image>().enabled = true;
                    allBtm[pos].GetComponent<Button>().interactable = true;

                    if (round > 3)
                    {
                        for (int s = 0; s < 3; s++)
                        {
                            pos = Random.Range(0, gameMode);
                            allQus[pos].transform.GetChild(0).GetComponent<Image>().enabled = true;
                            allQus[pos].GetComponent<Button>().interactable = true;
                        }
                        round = 0;
                        answer_Check = false;
                    }
                    /*
                    if (round == 1)
                    {
                        int bonus = Random.Range(0, gameMode);
                        allQus[bonus].transform.GetChild(0).GetComponent<Image>().enabled = true;
                        allQus[bonus].GetComponent<Button>().interactable = true;
                    }*/

                }

            }
            else
            {
                for (int i = 0; i < moleNo; i++)
                {
                    int pos = Random.Range(0, gameMode);
                    allBtm[pos].transform.GetChild(0).GetComponent<Image>().sprite = up;
                    allBtm[pos].transform.GetChild(0).GetComponent<Image>().enabled = true;
                    allBtm[pos].GetComponent<Button>().interactable = true;

                    if (round > 3)
                    {
                        for (int s = 0; s < 3; s++)
                        {
                            pos = Random.Range(0, gameMode);
                            allQus[pos].transform.GetChild(0).GetComponent<Image>().enabled = true;
                            allQus[pos].GetComponent<Button>().interactable = true;
                        }
                        round = 0;
                    }
                    /*
                    if (round == 1)
                    {
                        int bonus = Random.Range(0, gameMode);
                        allQus[bonus].transform.GetChild(0).GetComponent<Image>().enabled = true;
                        allQus[bonus].GetComponent<Button>().interactable = true;
                    }*/

                }

            }
            yield return new WaitForSeconds(yield);            

            /*
            if (round == 2)
            {
                foreach (var item in allQus)
                {
                    item.transform.GetChild(0).GetComponent<Image>().enabled = false;  
                }
                round = 0;
            }*/

        }
    }

    void HideAll()
    {
        for (int i = 0; i < 36; i++)
        {
            allBtm[i].transform.gameObject.SetActive(false);
            allQus[i].transform.gameObject.SetActive(false);
            allBtm[i].transform.GetChild(0).GetComponent<Image>().enabled = false;
            allBtm[i].GetComponent<Button>().interactable = false;
            allQus[i].GetComponent<Button>().interactable = false;
        }
    }

}
