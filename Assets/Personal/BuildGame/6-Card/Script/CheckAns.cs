﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckAns : MonoBehaviour {

    public int qus;
    public int ans;

    public void CheckAnswer()
    {
        if (this.GetComponent<Image>().sprite.name == "Lsign")
        {
            if (qus < ans)
            {
                NumberCardQus.trueCount += 1;
            }
        }
        else if (this.GetComponent<Image>().sprite.name == "Gsign")
        {
            if (qus > ans)
            {
                NumberCardQus.trueCount += 1;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Cover"))
        {
            for (int i = 0; i < 10; i++)
            {
                if (other.GetComponent<Image>().sprite.name == "Card_" + i)
                {
                    qus = i;
                }
            }
        }
        if (other.CompareTag("Drag"))
        {
            for (int i = 0; i < 10; i++)
            {
                if (other.GetComponent<Image>().sprite.name == "Card_"+ i)
                {
                    ans = i;
                }
            }
        }
    }
}
