﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragNdrop : MonoBehaviour {

    private bool isSlot = false;
    public Transform parentPos;
    private Transform tempPos;
    
    public void DragHere()
    {
        this.gameObject.transform.position = Input.mousePosition;
        this.transform.SetAsLastSibling();
    }

    public void DropHere()
    {
        if (isSlot == true)
        {
            this.transform.position = tempPos.position;
        }
        else
        {
            this.transform.position = parentPos.position;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Slot"))
        {
            isSlot = true;
            tempPos = other.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isSlot = false;
        tempPos = parentPos;
    }


}
