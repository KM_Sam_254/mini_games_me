﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NumberCardQus : MonoBehaviour {

    public RuntimeAnimatorController[] anim;
    public Animator animator;
    public Image[] cards;
    public Sprite[] sprite;
    public Transform[] ansSlot;
    public Transform[] cardSlot;
    public GameObject[] cover;
    public Dropdown dropdown;
    public OldDemantia.QuestionHandler qus;
    public CheckAns[] checkAns;
    public string[] easy;
    public string[] normal;
    public string[] hard;
    public int index;
	public Button nextbtn;
	private int touch = 0;
    public static int trueCount;

    private void Start()
    {
        GameStart();
    }

    public void GameStart()
    {
        if (index < 15)
        {
            Difficulty();
        }
        else
        {
            GameEnd();
        }
    }

    void Difficulty()
    {
        if (dropdown.value == 0)
        {
            qus.Split(easy, index);
        }
        else if (dropdown.value == 1)
        {
            qus.Split(normal, index);
        }
        else if (dropdown.value == 2)
        {
            qus.Split(hard, index);
        }
        AssignImage();
        index++;
    }    

    void AssignImage()
    {
        nextbtn.interactable = false;
        for (int i = 0; i < cards.Length; i++)
        {
            cards[i].sprite = sprite[qus.q[i]];
			if (i >= 3 && i <= 5) {
                iTween.ScaleFrom(cards[i].gameObject, 
					iTween.Hash("x", 1.3f, "y", 1.3f, "time", 2f));
			}
        }
        StartCoroutine(ShuffleCard());
    }

    public void CheckAns()
    {
		if (touch == 0) {
			for (int i = 0; i < checkAns.Length; i++)
			{
				checkAns[i].CheckAnswer();
			}
			if (trueCount == 3)
			{
				AnswerSound.answer = "right";
			}
			else
			{
				AnswerSound.answer = "wrong";
			}
			nextbtn.interactable = false;
			touch++;
			StartCoroutine(Next());
		} else {
			touch = 0;
			ResetAll();
			GameStart();
		}
       
    }

    void ResetAll()
    {
        trueCount = 0;
        for (int i = 0; i < cardSlot.Length; i++)
        {
            cards[i + 3].transform.position = cardSlot[i].position;
        }
    }   

	void ShowCard()
	{
		for (int i = 0; i < cardSlot.Length; i++)
		{
			cover[i].SetActive(false);
		}
	}

    IEnumerator Next()
    {
        yield return new WaitForSeconds(3f);
		ShowCard();
		nextbtn.interactable = true;        
    }

	void GameEnd()
	{
        Result.image = 5;
        Result.title = 5;
		SceneManager.LoadScene ("Complete");
		// payment.SetActive(true);
	}

    IEnumerator ShuffleCard()
    {
        yield return new WaitForSeconds(4f);
        for (int i = 0; i < cover.Length; i++)
        {
            cover[i].SetActive(true);
        }
        int r = Random.Range(0, 1);
        animator.enabled = true;
        animator.runtimeAnimatorController = anim[r];
        animator.Play("");
        yield return new WaitForSeconds(4f);
        animator.enabled = false;
        nextbtn.interactable = true;
    }
}
