﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {
    
    public static float TotalQuestion{ get; set;}
    public static float TotalTimeSec{ get; set;}
    public static float TotalTimeMin{ get; set;}
    public static float RightAnswer{ get; set;}
    public static float RateRightAnswer{ get; set;}
    public static int Achievement{ get; set;}
    public static bool TimerStart = false;

    void Update()
    {
        if (SceneManager.GetActiveScene().name == "1Welcome" ||
            SceneManager.GetActiveScene().name == "ChapterMenu")
        {
            TotalQuestion = 0;
            TotalTimeSec = 0;
            TotalTimeMin = 0;
            RightAnswer = 0;
            RateRightAnswer = 0;
            Achievement = 0;
        }        

        if (TimerStart == true)
        {
            Score.TotalTimeSec += Time.deltaTime;
            if (Score.TotalTimeSec > 59f)
            {
                Score.TotalTimeSec = 0f;
                Score.TotalTimeMin += 1f;
            }
        }
    }
}
