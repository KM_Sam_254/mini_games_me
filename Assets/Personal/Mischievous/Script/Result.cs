﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Result : MonoBehaviour {

    public static float time;
    public static int total;
    public static int right;
    public static int grade;
    public static int image;
    public static int title;
    public static bool isBegin;
    public Image img;
    public Text txt;
    private string[] mark;
    private string[] caption;
    public Text[] text;
    public Sprite[] sp;

    void Start()
    {
        mark = new string[10] { "D","D","D","D","C","C+","B","B+","A","A+",};
        caption = new string[8]
            {"도미노", "수의 크기 비교", "순서 맞추기",  "테트로미노",
                "규칙 찾기", "카드", "두더지" , "라인 연결"};
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().name == "MenuOld")
        {
            time = 0f; 
            total = 0;
            right = 0;
            grade = 0;
        } 

        if (isBegin)
        {
            time += 1f * Time.deltaTime;
        }

        if (SceneManager.GetActiveScene().name == "Complete")
        {
            text[0].text = (time / 60).ToString("N0")+" : "+(time % 60).ToString("N0");
            isBegin = false;
            text[1].text =  total.ToString();  
            text[2].text = right.ToString(); 
            text[3].text =mark[(right/total)*10].ToString();
            img.sprite = sp[image];
            txt.text = caption[title];
        }
    }
}
