﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OldDemantia
{
	public class MainMenu : MonoBehaviour {

		private Button[] sceneBtn;
		public static int completeGame = -1;
		private int i = 0;

		void Start()
		{
			GameObject panel = this.gameObject;
			sceneBtn = new Button[panel.transform.childCount];
			for (int i = 0; i < sceneBtn.Length; i++) {
				sceneBtn [i] = panel.transform.GetChild (i).GetComponent<Button> ();
			}
			if (completeGame < 6) {
				completeGame++;
			} else {
				completeGame = 0;
			}
			LockScene ();
		}

		void LockScene()
		{
			iTween.ScaleTo (sceneBtn [i].gameObject, 
				iTween.Hash ("x", 1.1f, "y", 1.1f, 
					"time", 3f,
					"oncomplete", "LoopMenu", 
					"oncompleteTarget", this.gameObject));			
		}

		void LoopMenu()
		{		
			iTween.ScaleTo (sceneBtn [i].gameObject, 
				iTween.Hash ("x", 1f, "y", 1f, 
					"time", 1f, "easeType", "linear",
					"oncomplete", "LockScene", 
					"oncompleteTarget", this.gameObject));
			if (i < sceneBtn.Length - 1) {
				i++;
			} else {
				i = 0;
			}
		}
	}
}

