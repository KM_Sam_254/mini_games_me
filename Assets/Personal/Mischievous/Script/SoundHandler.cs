﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace OldDemantia
{
	public class SoundHandler : MonoBehaviour {

		public AudioClip[] soundTrack;
		public AudioClip[] effect;
		// public Dictionary<string, AudioClip> soundTrack;
		public AudioSource audioSource;
		public AudioSource touch;

		void Update()
		{
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		void OnSceneLoaded(Scene scene, LoadSceneMode mode)
		{      			
			if (scene.name == "MenuOld" && scene.buildIndex > 21)
			{
				audioSource.clip = soundTrack[Random.Range(0,2)];
				audioSource.PlayDelayed(0.5f);
			}
			else if (scene.buildIndex > 21) {
				audioSource.clip = soundTrack[2];
				audioSource.PlayDelayed(0.5f);
			}
			else
			{      
				audioSource.Stop ();
			}    
			if (scene.name != "Complete" && scene.buildIndex > 21)
			{
				Result.image = scene.buildIndex - 1;
				Result.title = scene.buildIndex - 1;
			}
		}
	}
}

