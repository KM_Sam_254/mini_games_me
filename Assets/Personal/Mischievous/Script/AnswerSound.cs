﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnswerSound : MonoBehaviour
{
	public GameObject right_answer;
	public GameObject wrong_answer;
	public static string answer { get; set; }
	private GameObject temp;
	private bool isRunning = false;
	public ParticleSystem particle;

	private void Start()
	{
		Result.total = 15;
		Result.isBegin = true;
		particle.Stop();
		right_answer.SetActive(false);
		wrong_answer.SetActive(false);
	}

	private void Update()
	{
		if (isRunning == false)
		{
			if (answer == "right")
			{
				right_answer.SetActive(true);
				Show(right_answer);
				particle.Play();
				Result.right += 1;
			}
			else if (answer == "wrong")
			{
				wrong_answer.SetActive(true);
				Show(wrong_answer);
			}
			answer = "null";
		}
	}

	void Show(GameObject tempObj)
	{
		isRunning = true;
		tempObj.GetComponent<AudioSource>().Play();
		iTween.ScaleTo(tempObj, iTween.Hash("scale", new Vector3(1.3f, 1.3f, 0f),
			"easeType", "easeOutElastic", "time", 2f));
		temp = tempObj;
		StartCoroutine(ShowWait());
	}

	IEnumerator ShowWait()
	{
		yield return new WaitForSeconds(2f);
		iTween.ScaleTo(temp, iTween.Hash("scale", new Vector3(0.5f, 0.5f, 0f),
			"easeType", "linear", "time", 0.5f ));
		yield return new WaitForSeconds(0.5f);
		particle.Stop();
		temp.SetActive(false);
		isRunning = false;
	}
}
	
