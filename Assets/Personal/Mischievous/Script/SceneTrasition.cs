﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace OldDemantia
{
	public class SceneTrasition : MonoBehaviour {

		public void SceneTransition(string scene)
		{
			SceneManager.LoadScene(scene);
		}
	}

}
