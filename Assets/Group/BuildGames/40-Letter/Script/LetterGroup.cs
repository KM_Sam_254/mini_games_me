﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterGroup : MonoBehaviour {

	public Image[] img;
	public Sprite[] sp;

	public void Question()
	{
		for (int i = 0; i < img.Length; i++) {
			int s = Random.Range (0, sp.Length - 1);
			img [i].sprite = sp [s];
		}
	}
}
