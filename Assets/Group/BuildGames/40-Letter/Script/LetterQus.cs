﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterQus : MonoBehaviour {

    private GameObject[] qus;
    private Dropdown dropdown;

    public void ShowQuestion()
    {
        dropdown = GameObject.Find("Dropdown").GetComponent<Dropdown>();
        qus = new GameObject[3];
        for (int i = 0; i < qus.Length; i++)
        {
            qus[i] = this.gameObject.transform.GetChild(0).gameObject.transform
                .GetChild(0).gameObject.transform.GetChild(i).gameObject;
            qus[i].SetActive(false);
            if (dropdown.value == i)
            {
                qus[i].SetActive(true);
            }
        }
    }
}
