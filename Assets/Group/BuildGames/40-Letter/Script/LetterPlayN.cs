﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterPlayN : MonoBehaviour {

	public Image question;
	public GameObject next;
	public Sprite[] spt;
	public List<int> no;

	void Start()
	{
		AddList ();
	}

	void AddList()
	{
		no.Clear ();
		for (int i = 0; i < spt.Length; i++) {
			no.Add (i);
		}
	}

	public void Next()
	{
		int rand = Random.Range (0, no.Count);
		question.sprite = spt [no [rand]];
		iTween.ScaleFrom (question.gameObject, new Vector3 (3f, 3f, 0f), 1f);
		no.RemoveAt (rand);
		if (no.Count < 1) {
			AddList ();
		}
		StartCoroutine (ShowNext ());
	}

	IEnumerator ShowNext()
	{
		next.SetActive (false);
		yield return new WaitForSeconds (1.5f);
		next.SetActive (true);
		iTween.PunchScale (next.gameObject, new Vector3 (1f, 1f, 0f), 0.5f);
	}
}
