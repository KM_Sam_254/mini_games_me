﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Keyboard : MonoBehaviour {

	public bool isVowel = false;
	public GameObject nxtKbrd;
	public TextInput tempInput;
	public int tempNo;
	private Text tempTxt;
	private string[] consonant;
	private string[] vowel;
	private Text[] conTxt;
	private Text[] vowTxt;
	private string[] vowOne;
	private string[] vowTwo;

	void Start()
	{
		if (isVowel) {
			vowel = new string[10] { "ㅏ", "ㅑ", "ㅓ", "ㅕ", "ㅗ",
				"ㅛ", "ㅜ", "ㅠ", "ㅡ", "ㅣ"
			};
			vowTxt = new Text[this.gameObject.transform.childCount];
			for (int i = 0; i < vowTxt.Length; i++) {
				vowTxt [i] = this.gameObject.transform.GetChild (i)
					.gameObject.GetComponentInChildren<Text> ();
				vowTxt [i].text = vowel [i];
			}
		} 
		else 
		{
			consonant = new string[14] {"ㄱ", "ㄴ", "ㄷ", "ㄹ", "ㅁ", 
				"ㅂ", "ㅅ","ㅇ", "ㅈ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"
			};
			conTxt = new Text[this.gameObject.transform.childCount];
			for (int i = 0; i < conTxt.Length; i++) {
				conTxt[i] = this.gameObject.transform.GetChild (i)
					.gameObject.GetComponentInChildren<Text> ();
				conTxt [i].text = consonant [i];
			}
		}
		vowOne = new string[5] { "ㅗ", "ㅛ", "ㅜ", "ㅠ", "ㅡ" };
		vowTwo = new string[5] { "ㅏ", "ㅑ", "ㅓ", "ㅕ", "ㅣ"};
	}

	public void AddText()
	{
		for (int i = 0; i < 3; i++) {
			if (tempNo == i) {
				TextInput.index = i;
			}
		}
		tempTxt = EventSystem.current.currentSelectedGameObject.gameObject
			.GetComponentInChildren<Text> ();

		if (TextInput.index % 2 == 0) {
			for (int i = 0; i < consonant.Length; i++) {
				if (tempTxt.text == consonant[i]) {
					tempInput.txtSlot [TextInput.index].text = tempTxt.text;
				}
			}
		} else {
			for (int i = 0; i < vowOne.Length; i++) {
				if (tempTxt.text == vowOne[i]) {
					tempInput.txtSlot [3].text = tempTxt.text;

				} 
				else if(tempTxt.text == vowTwo[i]){
					tempInput.txtSlot [TextInput.index].text = tempTxt.text;
				}
			}
		}
		this.gameObject.SetActive (false);
		if (tempNo < 2) {
			nxtKbrd.SetActive (true);
		}
	}

}
