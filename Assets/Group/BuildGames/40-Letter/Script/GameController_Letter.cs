﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_Letter : GameController {

	public LetterPlayN gameScript;

	void Start()
	{
		SetGame();
	}

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	public override void StartGame()
	{
		hasStageStarted = true;
		this.difficulty = 1;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register task that have to be excuted before the game start
	}

	protected override void Next()
	{
		gameScript.Next ();
	}

	protected override void Verify()
	{
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
	}
}
