﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LetterNameTag : MonoBehaviour {

    public GameObject[] letterQus;
    public InputField[] nameTag;

    public void MoveQuestionByName()
    {
        for (int i = 0; i < nameTag.Length; i++)
        {
            letterQus[i].SetActive(false);
            if (EventSystem.current.currentSelectedGameObject.name == nameTag[i].name)
            {
                letterQus[i].SetActive(true);
                letterQus[i].GetComponent<LetterQus>().ShowQuestion();
                iTween.ScaleFrom(letterQus[i].gameObject, new Vector3(1.5f, 1.5f, 0f), 1f);
            }
        }
       
    }
}
