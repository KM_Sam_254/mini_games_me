﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextInput : MonoBehaviour {

	public Text[] txtSlot;
	private Keyboard[] keyboard;
	public static int index;
	public static int click;

	void Start()
	{
		txtSlot = new Text[this.gameObject.transform.childCount];
		keyboard = new Keyboard[4];
		GameObject kbrd = GameObject.Find ("Keyboard");
		for (int i = 0; i < txtSlot.Length; i++) {
			txtSlot [i] = this.gameObject.transform.GetChild (i)
				.GetComponent<Text> ();
			keyboard[i] = kbrd.transform.GetChild (i).GetComponent<Keyboard> ();
		}	
		Debug.Log (txtSlot.Length);
	}



	public void ClearSlot()
	{		
		TextInput.click++;
		if (TextInput.click == 1) {			
			for (int i = 0; i < txtSlot.Length; i++) {	
				txtSlot [i].text = "";
				keyboard [0].gameObject.SetActive (true);
				keyboard [i].tempInput = this.gameObject.
					GetComponent<TextInput> ();
			}
			iTween.ScaleTo (this.gameObject, new Vector3 (1.2f, 1.2f, 0f), 1f);

		} 
		else if (TextInput.click == 2) {
			for (int i = 0; i < keyboard.Length; i++) {
				keyboard [i].gameObject.SetActive (false);
				TextInput.index = 0;
			}
			TextInput.click = 0;
			iTween.ScaleTo (this.gameObject, new Vector3 (1f, 1f, 0f), 1f);
		}
	}
}
