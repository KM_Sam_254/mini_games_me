﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagicShafflePlay : GameController
{
	private Image[] cupHide;
	private Image[] cupShow;
	private GameObject[] coin;
	private GameObject no;
	public Animator[] cupMode;
	public Animator handAnim;
    private Toggle toggle;
	private int[] coinNo;
	private string[] cupAnimName;
    private List<int> temp;

    public override void SetGame()
    {
        base.SetGame();

        GameManager.Instance.OnClientGameReady();
    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    public override void StartGame()
    {
        hasGameStarted = true;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register task that have to be excuted before the game start
    }

    protected override void Next()
    {
        AddList();
        ChangeCups();
        SetNextInteractable(false);
        SetDiffcultyBtnInteractable(false);
        
        cupMode[difficulty].enabled = true;
        cupMode[difficulty].Play(cupAnimName[difficulty + 3]);
        CoinShow();
        StartCoroutine(GamePlay());
    }

    protected override void Verify()
    {
        SetVerifyInteractable(false);
        CoinShow();
        handAnim.Play("Hand_show");
        SetNextInteractable(true);
        SetDiffcultyBtnInteractable(true);
    }

    protected override void SetDifficulty(int difficulty)
    {
        difficulty -= 1;
        Debug.Log("Set difficulty : " + difficulty);
        this.difficulty = difficulty;
    }

    void Start()
	{
		cupAnimName = new string[6] {"CupEasy", "CupNormal", "CupHard",
			"CupEasyReset", "CupNormalReset", "CupHardReset"
		};
        toggle = GameObject.Find("Toggle").GetComponent<Toggle>();
        SetGame();
    }

    void AddList()
    {
        temp = new List<int>();
        for (int i = 0; i < difficulty + 3; i++)
        {
            temp.Add(i);
        }
        if (toggle.isOn)
        {
            coinNo = new int[2];
            int r1 = Random.Range(0, temp.Count);
            coinNo[0] = temp[r1];
            temp.RemoveAt(r1);
            int r2 = Random.Range(0, temp.Count);
            coinNo[1] = temp[r2];
            temp.RemoveAt(r2);
        }
        else
        {
            coinNo = new int[1];
            coinNo[0] = Random.Range(0, temp.Count);
        }
    }
    

	void ChangeCups()
	{
		for (int i = 0; i < cupMode.Length; i++) {
			cupMode [i].gameObject.SetActive (false);
		}
		cupMode [difficulty].gameObject.SetActive (true);

        int childCount = cupMode[difficulty].transform.childCount -1;
        Debug.Log("chiledCount : " + childCount);
        cupHide = new Image[childCount];
		cupShow = new Image[childCount];
		coin = new GameObject[childCount];
		for (int i = 0; i < cupHide.Length; i++)
        {
            Debug.Log("i : " + i);
			cupHide [i] = cupMode [difficulty].transform.GetChild (i)
				.GetComponent<Image> ();
			cupShow [i] = cupMode [difficulty].transform.GetChild (i)
				.transform.GetChild (0).GetComponent<Image> ();
			coin [i] = cupMode [difficulty].transform.GetChild (i)
                .transform.GetChild (0).transform.GetChild (0).gameObject;
        }

		no = cupMode [difficulty].transform.GetChild (difficulty + 3).gameObject;
		no.SetActive (false);
	}

	void CoinShow()
	{
        Debug.Log("Cointshow count : " + cupHide.Length);

        for (int i = 0; i < cupHide.Length; i++) {
            Debug.Log("Cointshow i : " + i + "  name : " + cupHide[i].name);

            cupHide [i].enabled = false;
			cupShow [i].enabled = true;
			iTween.PunchScale (cupHide [i].gameObject, new Vector3 (1f, 1f, 0f), 1f);
			coin [i].SetActive (false);
		}

        for (int i = 0; i < coinNo.Length; i++)
        {
            coin[coinNo[i]].SetActive(true);
        }
	}

	void CoinHide()
	{
		for (int i = 0; i < cupHide.Length; i++) {
			cupHide [i].enabled = true;
			cupShow [i].enabled = false;
			coin [i].SetActive (false);
		}
	}

	IEnumerator GamePlay()
	{
		yield return new WaitForSeconds (2f);
		CoinHide ();
		handAnim.enabled = true;
		handAnim.Play ("Hand_stick");
		yield return new WaitForSeconds (4f);
		cupMode[difficulty].Play (cupAnimName[difficulty]);
		yield return new WaitForSeconds (4f);
		no.SetActive (true);
        SetVerifyInteractable(true);
	}
}
