﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DotPlay : GameController
{

	public GameObject[] panel;
	public DotPanel[] dot;
	public Color[] c;
    public Image qus;
	private int index;
	private List<int> no;
    private int[] clr;
    private int colorDot;

    private void Start()
    {
        SetGame();
    }

    public override void SetGame()
    {
        base.SetGame();

    }

    void ChangePanel()
	{
		index = difficulty;
        clr = new int[3];
        for (int i = 0; i < 3; i++) {			
			panel [i].SetActive (false);
            clr[i] = Random.Range(0, c.Length);
			if (index == i) {
				panel [i].SetActive (true);
				dot [i].Reset ();
				iTween.MoveFrom (panel [i], new Vector3 (
					panel [i].transform.position.x,
					1500f, 0f), 1f);
				iTween.ScaleFrom (panel [i], new Vector3 (
					2f, 2f, 0f), 1f);	
			}
		}
		no = new List<int> ();
		no.Clear ();
		int max = Random.Range (2, 4);
        int childCount = panel[index].transform.GetChild(0).transform.childCount - 1;
        Debug.Log("childCount : " + childCount);
		for (int i = 0; i < max; i++) {
			int r = Random.Range (0, childCount);
			no.Add (r);
		}
        colorDot = Random.Range(0, no.Count);
		if (no[0] == no[1]) {
			no [0] = childCount;
		}
	}

	protected override void Next()
	{	
        SetNextInteractable(false);
		ChangePanel ();	
		StartCoroutine (Play ());
	}

	protected override void Verify()
	{
		ShowDot ();
        SetNextInteractable(true);
        SetVerifyInteractable(false);
	}

	IEnumerator Play()
	{
		yield return new WaitForSeconds (1f);
		ShowDot ();
		yield return new WaitForSeconds (1f);
		HideDot ();
		yield return new WaitForSeconds (3f);
		ShowDot ();
		yield return new WaitForSeconds (1f);
		HideDot ();
        SetVerifyInteractable(true);
	}

	void ShowDot()
	{
		for (int i = 0; i < no.Count; i++) {
			dot [index].color [no[i]].enabled = true;
            dot [index].color [no[i]].color = c [clr[i]];
		}
        qus.color = dot[index].color[no[colorDot]].color;
	}

	void HideDot()
	{
		for (int i = 0; i < no.Count; i++) {
			dot [index].color [no[i]].enabled = false;
		}
        qus.color = new Color(1f,1f,1f);
	}

    protected override void RegistGameStartProcedure()
    {
        
    }

    public override void StartGame()
    {

    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    protected override void SetDifficulty(int difficulty)
    {
        this.difficulty = difficulty -1;
    }
}
