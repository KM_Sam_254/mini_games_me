﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DotPanel : MonoBehaviour {

	public Image[] color;
	private NumberTxt[] no;
	private int max;

	void Start()
	{
		GameObject panel = this.gameObject;
		max = panel.transform.childCount;
		color = new Image[max];
		no = new NumberTxt[max];
		for (int i = 0; i < max; i++) {
			color [i] = panel.transform.GetChild (i).gameObject.
				transform.GetChild (0).gameObject.GetComponent<Image> ();
			no [i] = panel.transform.GetChild (i).gameObject.
				transform.GetChild (1).gameObject.GetComponent<NumberTxt> ();			
		}
		Reset ();
	}

	public void Reset()
	{
		for (int i = 0; i < max; i++) {
			color [i].enabled = false;
            no[i].SetNumber(i + 1);
		}
	}
}
