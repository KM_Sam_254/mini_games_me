﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_ColorBall : GameController {

	public ColorBallPlay play;

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
		play.mode = 1;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		play.mode = difficulty;
	}

	protected override void Next()
	{
		play.Next ();
	}

	protected override void Verify()
	{
		play.Verify ();
	}


}
