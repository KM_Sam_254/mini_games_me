﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorBallPlay : MonoBehaviour {

    public Image[] balls;
    public Image[] ballsAns;
	public Transform[] startPos;
	public Transform[] endPos;
    public Sprite[] sp;    
	public GameObject next;
	public GameObject verify;
	public GameObject dropdown;
	public int mode;

	private List<int> no;
	private List<int> pos;

    public void Next()
    {
		next.SetActive (false);
		verify.SetActive (false);
		dropdown.SetActive (false);
        HideBall();
        HideAns();
		StartCoroutine(ShowBall());
    }

    public void Verify()
    {
        ShowAns();
		next.SetActive (true);
		dropdown.SetActive (true);
    }

	IEnumerator ShowBall()
    {
		for (int i = 0; i < mode * 2; i++)
        {
            balls[i].gameObject.SetActive(true);
			int r = Random.Range (0, pos.Count);
			balls [i].transform.position = startPos [pos [r]].position;
            iTween.RotateAdd(balls[i].gameObject, iTween.Hash(
                    "z", -360f, "time", 2f, "loopType", "loop",
                    "easeType", "linear"));
            iTween.MoveTo(balls[i].gameObject, iTween.Hash(
				"position", endPos[pos[r]].position, "time", 4f,
                "easeType", "linear"));
			pos.RemoveAt (r);

            int s = Random.Range(0, 25);
            balls[i].sprite = sp[s];
            no.Add(s);

			yield return new WaitForSeconds (1f);
        }
		yield return new WaitForSeconds (3f);
		verify.SetActive (true);
    }

    void ShowAns()
    {
		for (int i = 0; i < mode * 2; i++)
        {
            ballsAns[i].gameObject.SetActive(true);
            ballsAns[i].sprite = sp[no[i]];   
            iTween.PunchScale(ballsAns[i].gameObject, new Vector3(1f, 1f, 1f), 1f);
        }
    }

    void HideAns()
    {
		for (int i = 0; i < ballsAns.Length; i++)
        {
            ballsAns[i].gameObject.SetActive(false);           
        }
    }

    void HideBall()
    {        
        for (int i = 0; i < balls.Length; i++)
        {
            balls[i].gameObject.SetActive(false);
        }
		pos = new List<int> ();
		for (int i = 0; i < startPos.Length; i++) {
			pos.Add (i);
		}
		no = new List<int> ();
    }

}
