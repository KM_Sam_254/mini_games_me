﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fruit : MonoBehaviour {

	public GameObject[] table;
	public Transform[] pos;
    private int no;

    int difficulty = 0;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    public GameController_Fruits gameController;

	void ChangeTable()
	{
		for (int i = 0; i < 4; i++) {
			iTween.MoveTo (table [i], pos [i + 1].position, 0f);
			if (Difficulty == i) {
				iTween.MoveTo (table [i], pos [0].position, 2f);
                no = i;
			}
		}
        gameController.SetNextInteractable(false);
        gameController.SetVerifyInteractable(true);
    }

    public void Next()
    {
        Debug.Log("Next");
		ChangeTable ();
        table[no].transform.GetChild(0).GetComponent<FruitTable>().AssignFruit();
        table[no].transform.GetChild(0).GetComponent<FruitTable>().cover.SetActive(true);
        gameController.SetNextInteractable(false);
        gameController.SetVerifyInteractable(true);
    }

    public void Verify()
    {
        Debug.Log("Verify");
        table[no].transform.GetChild(0).GetComponent<FruitTable>().cover.SetActive(false);
        table[no].transform.GetChild(0).GetComponent<FruitTable>().StopAnim();
        gameController.SetNextInteractable(true);
        gameController.SetVerifyInteractable(false);
    }
}
