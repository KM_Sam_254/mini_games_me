﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FruitTable : MonoBehaviour {

	private Image[] fruit;
	public Sprite[] fruitSprite;
    public GameObject cover;
    private Image question;
    public int fruit_count;
    public Animator anim;

	void Start()
	{
		fruit = new Image[12];
		//AssignFruit ();
	}

	public void AssignFruit()
	{        
        question = GameObject.Find("Question").GetComponent<Image>();
        fruit_count = Random.Range(1, this.gameObject.transform.childCount);
		for (int i = 0; i <  this.transform.childCount; i++) {
			fruit [i] = this.transform.GetChild (i).
				gameObject.transform.GetChild (0).GetComponent<Image> ();
			fruit[i].sprite = fruitSprite[Random.Range(0, 5)];
		}
        for (int i = 0; i < this.transform.childCount - 1; i++)
        {
            fruit[i].enabled = false;
        }
        for (int i = 0; i < fruit_count; i++)
        {
            fruit[i].enabled = true;
            question.sprite = fruit[i].sprite;
        }
        StartCoroutine(PlayAnim());
	}

    IEnumerator PlayAnim()
    {
        yield return new WaitForSeconds(2f);
        anim.enabled = true;
        anim.Play("");
    }

    public void StopAnim()
    {
        anim.enabled = false;
    }

}
