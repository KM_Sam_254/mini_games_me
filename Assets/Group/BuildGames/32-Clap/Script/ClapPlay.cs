﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClapPlay : MonoBehaviour {

	public GameObject face;
	public Text txt;
	private int clapCount;

    int difficulty = 0;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    public GameController_Clap gameController;
    Clap clap;

    float animLength;

    private void Awake()
    {
        SoundMgr.Instance.Mute_BGM();
        SoundMgr.Instance.ClickSoundMute = true;
    }

    private void Start()
    {
        clap = face.GetComponent<Clap>();
        gameController.SetVerifyInteractable(false);
    }

    public void Next()
	{
        animLength = clap.GetClapAnimLength();
        if (animLength < 1f) animLength = 1f;
        
		txt.text = "";
		face.SetActive (true);
		iTween.ScaleFrom(face, iTween.Hash("x", 0f, "y", 0f, 
			"easeType", "easeOutElastic", "time", 2f));
		StartCoroutine(ClapStart());

        gameController.SetNextInteractable(false);
        gameController.SetVerifyInteractable(false);
    }

	public void Verify()
	{
        gameController.SetVerifyInteractable(false);
        StartCoroutine (CheckCount ());
	}

	IEnumerator ClapStart()
	{
		yield return new WaitForSeconds (2f);

		clapCount = Random.Range (5, 7 + Difficulty * 3) + Difficulty;
        Debug.Log("Clap Count : " + clapCount);

		for (int i = 0; i < clapCount; i++)
        {
			clap.PlayClapSound ();
            float interval = Random.Range(0.6f, 1.5f);
            yield return new WaitForSeconds (interval);
            if(i == clapCount -1) gameController.SetVerifyInteractable(true);
        }
    }

	IEnumerator CheckCount()
	{
        yield return new WaitForSeconds(.7f);
		for (int i = 0; i < clapCount; i++)
        {
            clap.PlayClap();
            txt.text = (i + 1).ToString();
            iTween.ScaleFrom(txt.gameObject, new Vector3(3f, 3f, 0f), 0.3f);

            yield return new WaitForSeconds(animLength);
            if(i == clapCount-1)
            {
                Debug.Log("Check finished");
                txt.text = "";
                gameController.SetNextInteractable(true);
            }
		}
	}
}
