﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clap : MonoBehaviour {

	private AudioSource audiosource;
	private Animator anim;

    private void Start()
    {
        audiosource = this.GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }

    public void PlayClapSound()
	{
		audiosource.Play ();
	}

    public void PlayClap()
    {
        anim.SetTrigger("clap");
        PlayClapSound();
    }

    public float GetClapAnimLength()
    {
        return anim.GetCurrentAnimatorStateInfo(0).length;
    }
}
