﻿using UnityEngine.UI;
using UnityEngine;

public class GameController_Clap : GameController
{

    public ClapPlay gameScript;

    void Start()
    {
        SetGame();
    }

    public override void SetGame()
    {
        base.SetGame();

        GameManager.Instance.OnClientGameReady();
    }

    public override void HomeBtnClicked()
    {
        Debug.Log("Calp HomeBtn Clicked");
        SoundMgr.Instance.Resume_BGM();
        SoundMgr.Instance.ClickSoundMute = false;
        SceneMgr.Instance.GoGroupMenu();
    }

    public override void StartGame()
    {
        hasGameStarted = true;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register task that have to be excuted before the game start
    }

    protected override void Next()
    {
        gameScript.Next();
    }

    protected override void Verify()
    {
        gameScript.Verify();
    }

    protected override void SetDifficulty(int difficulty)
    {
        Debug.Log("Set difficulty : " + difficulty);
        this.difficulty = difficulty;
        gameScript.Difficulty = difficulty;
    }

    protected override void NextBtnClicked()
    {
        if (hasStageStarted) return;

        Next();

        isCheckingResult = false;
        hasStageStarted = true;
    }

    protected override void CheckResultClicked()
    {
        if (isCheckingResult || !hasStageStarted) return;

        Verify();

        isCheckingResult = true;
        hasStageStarted = false;
    }
}
