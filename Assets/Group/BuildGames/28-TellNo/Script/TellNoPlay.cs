﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TellNoPlay : MonoBehaviour {

	public GameObject qusCircle;
	public GameObject gamePlay;
	public Text text;
    public Text previousTxt;
	public Transform[] path;
	public Transform origin;
	public Transform last;
	public Sprite[] sp;
	public int min, max;
    public List<int> previous;
    public GameController_TellNo gameController;
    public Sprite whiteCircle;

    int difficulty = 0;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    public void GoPath()
	{
        qusCircle.GetComponent<Image>().sprite = whiteCircle;
        previousTxt.gameObject.SetActive(false);
        gameController.SetNextInteractable(false);
		if (Difficulty == 0) {
			min = 1; max = 20;
		}
		else if (Difficulty == 1) {
			min = 21; max = 50;
		}
		else if (Difficulty == 2) {
			min = 51; max = 99;
		}
		iTween.MoveTo (qusCircle, iTween.Hash ("path", path, 
			"easeType", "easeOutBounce", "time", 2f,  
			"oncomplete", "ShowQus", "oncompletetarget", gamePlay));
		iTween.ScaleTo (qusCircle, iTween.Hash ("scale", new Vector3 (3f, 3f, 0f),
			"easeType", "linear", "time",2f));
	}

	void ShowQus()
	{
        gameController.SetVerifyInteractable(true);
		text.gameObject.SetActive (true);

        int randomFruite = Random.Range(0, sp.Length);
		qusCircle.GetComponent<Image>().sprite = sp[randomFruite];
        int rand = Random.Range(min, max);
        text.text = rand.ToString ();
        previous.Add(rand);
		iTween.PunchScale (qusCircle, new Vector3(3f, 3f, 3f), 1f);
	}

	public void Verify()
	{
        gameController.SetVerifyInteractable(false);
        if (previous.Count > 1) {
            previousTxt.gameObject.SetActive(true);
            previousTxt.text = previous[0].ToString(); 
            previous.Remove(previous[0]);
        }
		iTween.ScaleTo (qusCircle, new Vector3 (4f, 4f, 4f), 1f);
		StartCoroutine (Reset ());
	}

	IEnumerator Reset()
	{
		yield return new WaitForSeconds (1.5f);
		iTween.ScaleTo (qusCircle, new Vector3 (0f, 0f, 0f), 1.5f);
		iTween.MoveTo (qusCircle, last.position, 0.5f);
		yield return new WaitForSeconds (0.5f);
        gameController.SetNextInteractable(true);
		text.gameObject.SetActive (false);
		iTween.MoveTo (qusCircle, origin.position, 0f);
	}
}
