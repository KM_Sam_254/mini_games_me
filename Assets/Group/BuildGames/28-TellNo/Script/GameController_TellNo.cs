﻿

public class GameController_TellNo : GameController
{
    public TellNoPlay gameScript;

    public override void SetGame()
    {
        base.SetGame();
        
        GameManager.Instance.OnClientGameReady();
    }

    public override void StartGame()
    {
        hasGameStarted = true;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register tasks to be done before the game started
    }

    void Start ()
    {
        Debug.Log("TellNo Start");
        SetGame();
    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    protected override void SetDifficulty(int difficulty)
    {
        Debug.Log("Set difficulty : " + difficulty);
        this.difficulty = difficulty;
        gameScript.Difficulty = difficulty;
    }

    protected override void Next()
    {
        gameScript.GoPath();
    }

    protected override void Verify()
    {
        gameScript.Verify();
    }
}
