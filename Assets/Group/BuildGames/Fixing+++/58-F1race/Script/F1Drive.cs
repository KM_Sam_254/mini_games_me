﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1Drive : MonoBehaviour {

	public Animator[] anim;

	public void Drive(int no)
	{
		for (int i = 0; i < anim.Length; i++) {			
			anim[i].enabled = true;
		}
	}
}
