﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class F1FinishLine : MonoBehaviour {

	public Text[] rank;
	public F1Car[] car;
	private List<F1Car> temp_list;
	private int index;

	void Start()
	{
		temp_list = new List<F1Car> ();	
	}

	void OnTriggerEnter2D(Collider2D other)
	{			
		other.GetComponent<F1Car> ().lapCount++;
		temp_list.Add (other.GetComponent<F1Car>());
		if (temp_list.Count > rank.Length) {
			temp_list.RemoveAt(0);	
		}
		for (int i = 5; i < temp_list.Count; i--) {
			rank [i].text = temp_list [i].ToString ();
		}
	}
}
