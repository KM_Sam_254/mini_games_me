﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1Car : MonoBehaviour {

	public int currentStanding;
	public int lapCount;
	public bool isLast;
	private float t;

	void Update()
	{
		t += Time.deltaTime;
		if (t > 3) {
			this.GetComponent<Animator> ().speed = Random.Range (0.5f, 0.55f);
			t = 0f;
		}
	}
}
