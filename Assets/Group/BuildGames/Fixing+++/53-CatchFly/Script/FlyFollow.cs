﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyFollow : MonoBehaviour {

	public Transform target;
	private Rigidbody2D rb;
	public bool isFollow;
	public float speed = 300f;
	private FlyPlay play;

	void Start()
	{
		rb = this.GetComponent<Rigidbody2D> ();
		play = GameObject.Find ("Gameplay").GetComponent<FlyPlay> ();
	}

	void FixedUpdate()
	{
		if (isFollow) {
			Vector2 dir = ((Vector2)target.position - (Vector2)transform.position).normalized;
			Vector2 deltaPosition = speed * dir * Time.deltaTime;
			rb.MovePosition ((Vector2)transform.position + deltaPosition);
		}
	}

	public void GetHit()
	{
		this.GetComponent<Image> ().raycastTarget = false;
		play.hit_fly++;
		play.ChangeCurrentText ();
		this.transform.GetChild (0).gameObject.SetActive (true);
		iTween.PunchScale (this.gameObject, new Vector3 (1f, 1f, 0f), 0.2f);
		StartCoroutine (Hide ());
	}

	IEnumerator Hide()
	{
		yield return new WaitForSeconds (0.3f);
		{
			this.transform.GetChild (0).gameObject.SetActive (false);
			this.gameObject.SetActive (false);
		}
	}
}
