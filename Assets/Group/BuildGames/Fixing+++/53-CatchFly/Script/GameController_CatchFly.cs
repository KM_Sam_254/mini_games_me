﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_CatchFly : GameController {

	public FlyTarget[] target;
	public FlyFollow[] follow;
	public GameObject[] candle;
	public FlyPlay play;
	public int mode;

	public override void SetGame()
	{
		base.SetGame();

		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		mode = difficulty;
		GameMode (300f + (difficulty * 100f));
	}

	protected override void Next()
	{
	}

	protected override void Verify()
	{
	}

	public void GameMode(float s)
	{
		HideAll ();
		for (int i = 0; i < mode; i++) {
			target [i].gameObject.SetActive (true);
			target [i].speed = s + 500f;
			target[i].FlyMove ();
			follow [i].gameObject.SetActive (true);
			follow [i].target = target [i].transform;
			follow [i].speed = Random.Range (300f, s);
			follow [i].isFollow = true;
		}
		play.isFilling = true;
		play.total_fly = 0;
		play.hit_fly = 0;
		play.Fade ();
	}

	void HideAll()
	{
		for (int i = 0; i < follow.Length; i++) {
			follow [i].gameObject.SetActive (false);
			target [i].gameObject.SetActive (false);
		}
	}
}
