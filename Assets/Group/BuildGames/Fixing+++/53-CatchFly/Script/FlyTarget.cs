﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyTarget : MonoBehaviour {

	public Transform[] flyPos;
	public Transform[] tempPath;
	public float speed;

	private List<int> pos;

	public void FlyMove()
	{
		pos = new List<int> ();
		for (int i = 0; i < flyPos.Length; i++) {
			pos.Add (i);
		}
		int c = Random.Range (3, 5);
		tempPath = new Transform[c];
		for (int i = 0; i < c; i++) {
			int r = Random.Range (0, pos.Count);
			tempPath [i] = flyPos [pos [r]];
			pos.RemoveAt (r);
		}
		iTween.MoveTo (this.gameObject, iTween.Hash (
			"path", tempPath, "easeType", "linear", "speed", speed,
			"oncomplete", "NextPosition", "oncompletetarget", this.gameObject));
	}

	void NextPosition()
	{
		FlyMove ();
	}
}
