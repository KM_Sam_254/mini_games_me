﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyPlay : MonoBehaviour {

	public float fillSpeed = 5f;
	public bool isFilling = false;
	public GameController_CatchFly control;
	public Image fill;
	public Image background;
	public int hit_fly;
	public int total_fly;
	public Text currentText;
	public GameObject touchBlocker;

	void Update()
	{
		if (isFilling) {
			fill.fillAmount += Time.deltaTime / fillSpeed;
			if (fill.fillAmount == 1f) {	
				StartCoroutine (LightCandle ());
			}
		}
	}

	IEnumerator LightCandle()
	{		
		Light ();
		isFilling = false;
		yield return new WaitForSeconds (0.5f);
		float wait = control.mode * 0.1f;
		for (int i = 0; i < control.candle.Length; i++) {
			control.candle [i].SetActive (false);
			if (i == control.candle.Length - 1) {
				Fade ();
			}
			yield return new WaitForSeconds (wait);
		}
		yield return new WaitForSeconds (1f);
		this.fillSpeed = Random.Range (3f, 5f);
		ShowFly ();
	}

	void ShowFly()
	{
		for (int i = 0; i < control.mode; i++) {
			control.follow [i].gameObject.SetActive (true);
		}
		isFilling = true;
		fill.fillAmount = 0f;
	}

	public void Fade()
	{
		for (int i = 0; i < control.candle.Length; i++) {
			control.candle [i].SetActive (false);		
		}
		for (int i = 0; i < control.mode; i++) {
			control.follow [i].GetComponent<Image> ().color = 
				new Color (1f, 1f, 1f, 0.5f);
			control.follow [i].GetComponent<Image> ().raycastTarget = true;
			total_fly++;
		}
		touchBlocker.SetActive (true);
		background.color = new Color (1f, 1f, 1f, 0.5f);
		ChangeCurrentText ();
	}

	void Light()
	{
		for (int i = 0; i < control.candle.Length; i++) {
			control.candle [i].SetActive (true);		
		}
		for (int i = 0; i < control.mode; i++) {
			control.follow [i].GetComponent<Image> ().color = 
				new Color (1f, 1f, 1f, 1f);
		}
		background.color = new Color (1f, 1f, 1f, 1f);
		touchBlocker.SetActive (false);
	}

	public void ChangeCurrentText()
	{
		currentText.text = 
			string.Format ("Hit Flies : <color=orange>{0}</color> / " +
				"Total Flies : <color=orange>{1}</color>"
				, hit_fly, total_fly);
	}
}
