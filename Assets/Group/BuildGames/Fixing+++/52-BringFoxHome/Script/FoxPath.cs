﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoxPath : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Tile")) 
		{
			other.GetComponent<Image> ().raycastTarget = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		other.GetComponent<Image> ().raycastTarget = false;
	}
}
