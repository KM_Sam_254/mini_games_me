﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileHunter : MonoBehaviour {

	public bool isHunterHere;
	public bool isHome;

	private GameObject fox;

	void Start()
	{
		fox = GameObject.Find ("Fox");
	}

	public void MoveFox()
	{		
		iTween.ScaleFrom (this.gameObject, new Vector3 (0.8f, 0.8f, 0f), 0.3f);

		if (isHunterHere) {
			fox.GetComponent<Animator> ().SetInteger ("moveState", 2);
			this.transform.GetChild (0).gameObject.SetActive (true);
			BringFoxHomePlay.isFoxDead = true;
		} 
		else 
		{
			iTween.MoveTo (fox, iTween.Hash (
				"position", this.transform.position, "easeType", "linear",
				"time", 1f, "oncomplete", "Idle", 
				"oncompletetarget", this.gameObject)); 
			fox.GetComponent<Animator> ().SetInteger ("moveState", 1);
			fox.GetComponent<Rigidbody2D> ().simulated = false;
		}	
	}
	void Idle()
	{		
		if (isHome) {
			fox.GetComponent<Animator> ().SetInteger ("moveState", 3);
			BringFoxHomePlay.isFoxHomeArrived = true;
		} else {
			fox.GetComponent<Animator> ().SetInteger ("moveState", 0);
			fox.GetComponent<Rigidbody2D> ().simulated = true;
			BringFoxHomePlay.isIdle = true;
		}
	}

}
