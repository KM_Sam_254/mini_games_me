﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BringFoxHomePlay : MonoBehaviour {

	public int hunter = 2;
	public GameObject fox;
	public GameObject cave;
	public GameObject arrivePanel;
	public GameObject deadPanel;
	public Image touchBlocker;
	public Button next;

	public static bool isFoxHomeArrived;
	public static bool isFoxDead;
	public static bool isIdle;

	public TileHunter[] tile;
	public Transform[] foxPos;
	public Transform[] cavePos;

	public List<TileHunter> randomHunter;
	private List<int> no;
	private List<int> index;

	void Update()
	{
		if (isFoxDead) {			
			isFoxDead = false;
			isFoxHomeArrived = false;
			StartCoroutine (Check (false));
		}
		else if (isFoxHomeArrived) {
			isFoxDead = false;
			isFoxHomeArrived = false;
			StartCoroutine (Check (true));
		}
		else if (isIdle) {
			AssignHunter ();
			isIdle = false;
		}
	}

	public void AssignHunter()
	{
		no = new List<int> ();
		index = new List<int> () { 0, 1, 2, 3 };
		for (int i = 0; i < randomHunter.Count; i++) {
			randomHunter [i].transform.GetChild (0).gameObject.SetActive (false);
			randomHunter [i].isHunterHere = false;
			no.Add (i);
		}

		for (int i = 0; i < hunter; i++) {	
			int r = Random.Range (0, no.Count);
			randomHunter [no[r]].isHunterHere = true;
			randomHunter [no[r]].transform
				.GetChild (0).gameObject.SetActive (true);
			no.RemoveAt (r);
		}
		StartCoroutine (HideHunter ());
	}

	void ResetFox()
	{
		int f = Random.Range (0, foxPos.Length);
		fox.transform.position = foxPos [f].position;
		fox.GetComponent<Animator> ().SetInteger ("moveState", 0);
		fox.GetComponent<Rigidbody2D> ().simulated = true;

		int c = Random.Range (0, cavePos.Length);
		cave.transform.position = cavePos [c].position;

		fox.GetComponent<Rigidbody2D> ().simulated = true;

		for (int i = 0; i < randomHunter.Count; i++) {
			randomHunter [i].transform.GetChild (0).gameObject.SetActive (false);
		}
		next.interactable = true;
	}

	IEnumerator HideHunter()
	{
		yield return new WaitForSeconds (0.5f);
		for (int i = 0; i < tile.Length; i++) {
			tile [i].transform
				.GetChild (0).gameObject.SetActive (false);
		}
	}

	IEnumerator Check(bool isCorrect)
	{
		next.interactable = false;
		yield return new WaitForSeconds (1f);
		touchBlocker.raycastTarget = true;
		if (isCorrect) {
			arrivePanel.SetActive (true);
		} else {
			deadPanel.SetActive (true);
		}
		yield return new WaitForSeconds (2f);
		arrivePanel.SetActive (false);
		deadPanel.SetActive (false);
		ResetFox ();
	}

}
