﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControllerFoxPlay : GameController {

	public BringFoxHomePlay[] play;

	public override void SetGame()
	{
		base.SetGame();

		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
		difficulty = 1;
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
	}

	protected override void Next()
	{
		ChangeDifficulty (difficulty);
	}

	protected override void Verify()
	{
	}

	public void ChangeDifficulty(int mode)
	{
		mode = difficulty;
		for (int i = 0; i < play.Length; i++) {
			play [i].gameObject.SetActive (false);
		}
		play [mode - 1].gameObject.SetActive (true);
		play [mode - 1].next.interactable = false;
		play [mode - 1].AssignHunter ();
		play [mode - 1].touchBlocker.raycastTarget = false;
	}

}
