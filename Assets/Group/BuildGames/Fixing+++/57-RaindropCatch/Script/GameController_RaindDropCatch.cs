﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController_RaindDropCatch : GameController {

	public Raindrop[] rainDrop;
	public RaindropBearMove bear;
	public Image fillImg;
	public GameObject savePanel;
	public GameObject wastePanel;
	private float drip;
	private int maxDrop;

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		savePanel.SetActive (false);
		wastePanel.SetActive (false);
		SetDiffcultyBtnInteractable (false);
		fillImg.fillAmount = 0.5f;
		ChangeDifficulty ();
	}

	protected override void Next()
	{
	}

	protected override void Verify()
	{
	}

	void ChangeDifficulty ()
	{
		if (difficulty == 1) {
			maxDrop = 3;
			drip = 1.5f;
		} else if (difficulty == 2) {
			maxDrop = 4;
			drip = 1f;
		} else if (difficulty == 3) {
			maxDrop = 5;
			drip = 0.5f;
		}
		StartCoroutine (CreateDrop ());
	}

//-------------Gameplay--------------

	public void FillDrop()
	{
		if (fillImg.fillAmount == 1f) {
			savePanel.SetActive (true);
			SetDiffcultyBtnInteractable (true);
			StopAllCoroutines ();
		} else {
			fillImg.fillAmount += 0.01f;
		}
	}

	public void UnFillDrop()
	{
		if (fillImg.fillAmount == 0f) {
			wastePanel.SetActive (true);
			SetDiffcultyBtnInteractable (true);
			StopAllCoroutines ();
		} else {
			fillImg.fillAmount -= 0.02f;
		}
	}
	 
	IEnumerator CreateDrop()
	{
		for (int i = 0; i < maxDrop; i++) {
			rainDrop [i].drip = drip;
			rainDrop [i].RandomRaindrop ();
			yield return new WaitForSeconds (drip + 1f);
		}
		yield return new WaitForSeconds (1f);
		ChangeDifficulty ();
	}
}
