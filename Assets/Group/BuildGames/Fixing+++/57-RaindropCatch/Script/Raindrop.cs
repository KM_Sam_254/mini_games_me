﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Raindrop : MonoBehaviour {

	private float x;
	public float drip;
	public Sprite[] sprt;
	public GameController_RaindDropCatch play;
	public bool isDropOnGround = true;

	public void RandomRaindrop()
	{
		isDropOnGround = true;
		x = Random.Range (-5f, 5f);
		this.transform.position = new Vector2 (x, 2.5f);
		this.GetComponent<SpriteRenderer> ().enabled = true;
		this.GetComponent<SpriteRenderer> ().sprite = sprt [0];
		if (x < 0) {
			iTween.MoveTo (this.gameObject, iTween.Hash("x", x - 0.3f, "y", 1.1f, 
				"easeType", "linear", "time", drip - 0.1f));	
		} else if (x > 0) {
			iTween.MoveTo (this.gameObject, iTween.Hash("x", x + 0.3f, "y", 1.1f, 
				"easeType", "linear", "time", drip - 0.1f));
		}
		StartCoroutine (WaitDrop ());
	}

	IEnumerator WaitDrop()
	{
		yield return new WaitForSeconds (drip);
		this.GetComponent<SpriteRenderer> ().sprite = sprt [1];
		iTween.MoveTo (this.gameObject, iTween.Hash("x", x, "y", -4f, 
			"easeType", "linear", "time", 0.9f));
		yield return new WaitForSeconds (1f);
		if (isDropOnGround) {
			this.gameObject.GetComponent<SpriteRenderer> ().enabled = false;
			this.GetComponentInChildren<Animator>().SetTrigger ("Splash");
			play.UnFillDrop ();
		}
	}
}
