﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaindropBearMove : MonoBehaviour {

	public Slider slider;
	public GameObject bucket;
	public GameController_RaindDropCatch play;
	private Animator anim;
	private SpriteRenderer sprite;
	private bool isLeft;
	private bool isRight;
	private int p;

	void Start()
	{
		anim = this.GetComponentInChildren<Animator> ();
		sprite = this.GetComponent<SpriteRenderer> ();
	}

	void Update()
	{
		if (isLeft && slider.value > -5.5f) {
			slider.value -= 10f * Time.deltaTime;
			iTween.MoveUpdate(this.gameObject, 
				new Vector3(slider.value, 
					this.transform.position.y, 0f), 0f);
		} else if (isRight && slider.value < 5.5f) {
			slider.value += 10f * Time.deltaTime;
			iTween.MoveUpdate(this.gameObject, 
				new Vector3(slider.value, 
					this.transform.position.y, 0f), 0f);
		}
	}

	public void MoveLeft()
	{	
		isLeft = true;
		this.transform.localScale = new Vector2 (-1f, 1f);
		anim.SetBool ("isWalk", true);
	}

	public void MoveRight()
	{	
		isRight = true;
		this.transform.localScale = new Vector2 (1f, 1f);
		anim.SetBool ("isWalk", true);
	}

	public void UnTouch()
	{	
		isLeft = false;
		isRight = false;
		anim.SetBool ("isWalk", false);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Tile")) {
			p++;
			other.GetComponent<SpriteRenderer> ().enabled = false;
			other.GetComponent<Raindrop> ().isDropOnGround = false;
			other.transform.position = new Vector2 (0f, 10f);
			iTween.PunchScale (bucket, new Vector3 (0.2f, 0.2f, 0f), 0.5f);
			play.FillDrop ();
		}
	}
}
