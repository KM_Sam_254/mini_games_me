﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceShipMovement : MonoBehaviour {

	public GameObject[] ship;
	public GameObject verifyPanel;
	public Text dodgeTxt;
	public int dodgeCount;
	public Image fill;
	public RocketMovement[] rocket;
	public Transform[] rocketPos;
	public Sprite[] sprt;
	public Animator[] boom;
	public Button difficulty;
	public int boomNo;
	public int totalRocket;
	public int totalShip;
	public float min;
	public float max;

	private int index = 0;
	private List<GameObject> tempShip;

	public void Next()
	{
		verifyPanel.SetActive (false);
		difficulty.gameObject.SetActive (false);
		dodgeCount = 0;
		fill.fillAmount = 1f;
		tempShip = new List<GameObject> ();
		dodgeTxt.text = "Total Dodged Rocket : <color=orange> ?? </color>";

		for (int i = 0; i < ship.Length; i++) {
			ship [i].SetActive (false);
		}

		for (int i = 0; i < totalShip; i++) {
			ship [i].SetActive (true);
		}

		for (int i = 0; i < totalRocket; i++) {
			rocket [i].min_speed = min;
			rocket [i].max_speed = max;
			rocket [i].LaunchRocket ();
		}
	}

	public void ApplyDamage(GameObject other)
	{		
		if (fill.fillAmount == 0f) {
			verifyPanel.SetActive (true);
			ResetRocket ();
			difficulty.gameObject.SetActive (true);
		} else {
			iTween.PunchScale (other, iTween.Hash (
				"amount", new Vector3 (1f, 1f, 0f), "time", 0.5f,
				"oncomplete", "ChangeToNormal", "oncompletetarget",
				this.gameObject));
			other.GetComponent<Image> ().sprite = sprt [1];
			fill.fillAmount -= 0.05f; 
			tempShip.Add (other);
		}
	}

	void ResetRocket()
	{
		for (int i = 0; i < rocket.Length; i++) {
			rocket [i].Reset ();
			rocket [i].StopAllCoroutines ();
		}
	}

	void ChangeToNormal()
	{
		for (int i = tempShip.Count - 1; i >= 0 ; i--) {
			tempShip [i].GetComponent<Image> ().sprite = sprt [0];
			tempShip.RemoveAt (i);
		}
	}

	public void MoveShip()
	{		
		iTween.MoveTo(ship[index], iTween.Hash(
			"position", Input.mousePosition, "speed", 200f));
	}

	public void SelectShip(int idx)
	{
		for (int i = 0; i < ship.Length; i++) {
			ship [i].transform.localScale = new Vector2 (1f, 1f);
		}
		ship [idx].transform.localScale = new Vector2 (1.2f, 1.2f);
		index = idx;
	}
}
