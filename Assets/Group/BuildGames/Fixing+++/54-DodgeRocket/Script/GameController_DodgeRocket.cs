﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_DodgeRocket : GameController {

	public SpaceShipMovement ship;

	public override void SetGame()
	{
		base.SetGame();

		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
	}

	protected override void Next()
	{
	}

	protected override void Verify()
	{
	}

	public void LaunchRockets (int mode)
	{
		switch (mode) {
		case 1:
			ship.totalShip = 1;
			ship.min = 200f;
			ship.max = 300f;
			ship.totalRocket = 4;
			break;
		case 2:
			ship.totalShip = 1;
			ship.min = 300f;
			ship.max = 400f;
			ship.totalRocket = 4;
			break;
		case 3:
			ship.totalShip = 1;
			ship.min = 400f;
			ship.max = 500f;
			ship.totalRocket = 4;
			break;


		case 4:
			ship.totalShip = 2;
			ship.min = 200f;
			ship.max = 300f;
			ship.totalRocket = 5;
			break;
		case 5:
			ship.totalShip = 2;
			ship.min = 300f;
			ship.max = 400f;
			ship.totalRocket = 5;
			break;
		case 6:
			ship.totalShip = 2;
			ship.min = 400f;
			ship.max = 500f;
			ship.totalRocket = 5;
			break;


		case 7:
			ship.totalShip = 3;
			ship.min = 300f;
			ship.max = 400f;
			ship.totalRocket = 6;
			break;
		case 8:
			ship.totalShip = 3;
			ship.min = 400f;
			ship.max = 500f;
			ship.totalRocket = 7;
			break;
		}
		ship.Next ();
	}
}
