﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RocketMovement : MonoBehaviour {

	public float min_speed;
	public float max_speed;
	private float speed;
	public float rotatingSpeed;
	public GameObject[] target;
	private Rigidbody2D rb;
	private bool isFollowing;
	private SpaceShipMovement play;
	private int t;

	void Start () {	
		rb = GetComponent<Rigidbody2D> ();
		play = GameObject.Find ("Gameplay")
			.GetComponent<SpaceShipMovement> ();
		Reset ();
	}

	public void LaunchRocket()
	{
		speed = Random.Range (min_speed, max_speed);
		rotatingSpeed = Random.Range (50f, 150f);
		t = Random.Range (0, play.totalShip);
		rb.simulated = true;
		isFollowing = true;
	}

	void FixedUpdate () {
		if (isFollowing) {			
			Vector2 point2Target = 
				(Vector2)transform.position - (Vector2)target[t].transform.position;
			point2Target.Normalize ();
			float value = Vector3.Cross (point2Target, transform.right).z;
			/*
				if (value > 0) {
				
						rb.angularVelocity = rotatingSpeed;
				} else if (value < 0)
						rb.angularVelocity = -rotatingSpeed;
				else
						rotatingSpeed = 0; 
		*/

			rb.angularVelocity = rotatingSpeed * value;
			rb.velocity = transform.right * speed;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		play.boomNo++;
		play.boom[play.boomNo % play.boom.Length].transform.position = this.transform.position;
		play.boom[play.boomNo % play.boom.Length].SetTrigger ("Boom");

		if (other.tag == "Player") {
			Reset ();
			StartCoroutine (WaitNext ());
			play.ApplyDamage (other.gameObject);
		}
		else if (other.tag == "Balls") {
			Reset ();
			StartCoroutine (WaitNext ());
			play.dodgeCount += 1;
			play.dodgeTxt.text = "Total Dodged Rocket : <color=orange> " 
				+ play.dodgeCount.ToString () + "</color>";
		}
	}

	public void Reset()
	{		
		isFollowing = false;
		rb.simulated = false;
		int p = Random.Range (0, play.rocketPos.Length);
		this.transform.position = play.rocketPos [p].position;
	}

	IEnumerator WaitNext()
	{
		float wait = Random.Range (2f, 5f);
		yield return new WaitForSeconds (wait);
		LaunchRocket ();
	}
}
