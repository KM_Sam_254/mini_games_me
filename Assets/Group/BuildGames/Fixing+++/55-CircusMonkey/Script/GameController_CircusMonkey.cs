﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_CircusMonkey : GameController {

	public CircusMonkeyPlay play;

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
		play.Next ();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
	}

	protected override void Next()
	{
		
	}

	protected override void Verify()
	{
	}
}
