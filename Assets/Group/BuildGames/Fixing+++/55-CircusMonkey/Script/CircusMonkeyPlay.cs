﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircusMonkeyPlay : MonoBehaviour {

	public bool isNext;
	public Text currentTxt;
	public CircusMonkey monkey;
	public CircusReset boundary;
	public GameObject[] platform;
	public Transform[] path;
	public Transform[] limit;

	private int jump_count;
	public int reach_count;

	void Start()
	{
		Next ();
	}

	public void Next()
	{		
		boundary.isCanHit = true;
		platform [1].GetComponentInChildren<CircusReset> ().isCanHit = true;
		ResetPlatform ();
		currentTxt.text = 
			string.Format ("Jump : <color=orange>{0}</color> / " +
				"Reach : <color=orange>{1}</color>", jump_count, reach_count);
		jump_count++;
	}

	void ResetPlatform()
	{		
		float x = Random.Range (limit [2].transform.position.x,
			limit [1].transform.position.x);
		float y = Random.Range (limit [2].transform.position.y, 
			limit [0].transform.position.y);		
		iTween.MoveTo (platform [0].gameObject, iTween.Hash (
			"y", y, "time", 0f));
		monkey.transform.position = new Vector2 (platform [0].transform.position.x,
			platform [0].transform.position.y + 1000f);
		iTween.MoveTo (platform [1].gameObject, iTween.Hash (
			"x", x, "y", y, "time", 0f, "oncomplete", "GoPath",
			"oncompletetarget", this.gameObject));		
	}

	void GoPath()
	{
		path [0].transform.parent.position = platform [1].transform.position;
		iTween.MoveTo (platform [1].gameObject, iTween.Hash
			("path",  path, 
				"loopType", "pingPong", "easeType", "linear",
				"speed", 300f));
	}
}
