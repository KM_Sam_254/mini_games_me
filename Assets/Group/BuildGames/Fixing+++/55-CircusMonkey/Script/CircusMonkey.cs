﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircusMonkey : MonoBehaviour {

	public Transform parent;
	private bool isJump = false;
	private float timer;
	private float start;
	private float end;
	public Image fill;
	private float force;
	private bool isFilling;
	public bool isLeft = false;

	void Update()
	{
		if (isFilling) {
			fill.fillAmount += Time.deltaTime / 3f;
		}
	}

	public void StartDraw()
	{
		start = Time.time;
		isFilling = true;
	}

	public void Jump()
	{		
		isJump = true;
		isFilling = false;
		fill.fillAmount = 0f;
		end = Time.time;
		float f = end - start;
		if (isLeft && f > 1f && f <= 3f) {
			this.transform.localScale = new Vector2 (-1f, 1f);
			force = (f / 3f) * 100f;
			this.GetComponent<Rigidbody2D> ().AddForce
			(new Vector2 (-force * 10f, force * 20f), ForceMode2D.Impulse);
			this.GetComponent<Animator> ().SetBool ("isJump", isJump);
		}
		else if (isLeft == false && f > 1f && f <= 3f) {
			this.transform.localScale = new Vector2 (1f, 1f);
			force = (f / 3f) * 100f;
			this.GetComponent<Rigidbody2D> ().AddForce
			(new Vector2 (force * 10f, force * 20f), ForceMode2D.Impulse);
			this.GetComponent<Animator> ().SetBool ("isJump", isJump);
		}

	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Tile") {
			isJump = false;
			this.GetComponent<Animator> ().SetBool ("isJump", isJump);
			this.transform.SetParent (col.transform);
		} 
		else if (isJump) {
			isJump = false;
			this.GetComponent<Animator> ().SetBool ("isJump", isJump);
			this.transform.SetParent (parent.transform);
		}
	}

	void OnCollisionExit2D(Collision2D col)
	{
		this.transform.SetParent (parent.transform);
	}
}
