﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircusReset : MonoBehaviour {

	public bool isCanHit = true;
	public bool isBoundary;
	public CircusMonkeyPlay play;

	void Update()
	{
		if (play.isNext) {
			play.isNext = false;
			StopAllCoroutines ();
			play.Next ();
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (isCanHit && isBoundary == false) {
			isCanHit = false;
			play.isNext = false;
			StartCoroutine (Check ());
		}
		else if (isCanHit && isBoundary) {
			isCanHit = false;
			play.isNext = true;
		}
	}

	IEnumerator Check()
	{
		yield return new WaitForSeconds (2f);
		play.isNext = true;
		play.reach_count++;
	}		
}
