﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimalSoundPlay : MonoBehaviour {

	public AudioSource sound;
	public GameObject next;
	public GameObject verify;
	public int mode = 1;
	public AudioClip[] animalSfx;
	public GameObject[] ansRing;
	private List<int> no;
	public List<int> ans;
	
	public void Next () 
	{
		StartCoroutine (AnimalMakeSound ());
		next.SetActive (false);
	}
	
	public void Verify () 
	{
		StartCoroutine (AnimalSoundVerfiy ());
		verify.SetActive (false);
	}

	void AddList()
	{
		no = new List<int> ()
		{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		for (int i = 0; i < ansRing.Length; i++) {
			ansRing [i].SetActive (false);
		}
		ans.Clear ();
	}

	IEnumerator AnimalMakeSound()
	{
		AddList ();
		yield return new WaitForSeconds (2f);
		for (int i = 0; i < mode * 2; i++) {
			int rnd = Random.Range (0, no.Count);
			sound.clip = animalSfx [no [rnd]];
			sound.Play ();
			ans.Add (no [rnd]);
			no.RemoveAt (rnd);
			yield return new WaitForSeconds (sound.clip.length);
		}
		verify.SetActive (true);
	}

	IEnumerator AnimalSoundVerfiy()
	{
		for (int i = 0; i < ans.Count; i++) {
			ansRing [ans[i]].SetActive (true);
			iTween.ScaleFrom (ansRing [ans [i]],
				new Vector3 (3f, 3f, 0f), 0.3f);
			yield return new WaitForSeconds (0.5f);
		}
		next.SetActive (true);
	}
}
