﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiamondTile : MonoBehaviour {
	
	public DigDiamondPlay play;
	public bool isNewTile;
	private Animator miner;

	void Start()
	{
		miner = GameObject.Find ("Miner").GetComponent<Animator> ();
	}

	public void DigDiamond()
	{
		if (play.allowNo >= 0 && isNewTile) {
			iTween.MoveTo (miner.gameObject, this.transform.position, 0f);
			miner.SetBool ("isIdle", true);
			isNewTile = false;
			StartCoroutine (Show ());
			DisableAll ();
		} 
	}
	IEnumerator Show()
	{
		yield return new WaitForSeconds (0.8f);
		miner.SetBool ("isIdle", false);
		this.transform.GetChild(0).gameObject.SetActive (true);
		play.ansSprt.Add (this.transform.GetChild (0)
			.GetComponent<Image> ().sprite);

		if (play.allowNo == 1) {
			yield return new WaitForSeconds (1f);
			play.isFull = true;
		} else {
			EnableAll ();
			play.allowNo--;
		}			
	}

	void DisableAll()
	{
		for (int i = 0; i < play.tiles.Length; i++) {
			play.tiles [i].raycastTarget = false;
		}
	}

	void EnableAll()
	{
		for (int i = 0; i < play.tiles.Length; i++) {
			play.tiles [i].raycastTarget = true;
		}
	}
}
