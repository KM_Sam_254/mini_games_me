﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController_DigDiamond : GameController {

	public DigDiamondPlay[] playMode;
	private int mode;

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
		difficulty = 1;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		mode = difficulty - 1;
		ChangeDifficulty ();
	}

	protected override void Next()
	{
	}

	protected override void Verify()
	{
	}

	void ChangeDifficulty()
	{
		for (int i = 0; i < playMode.Length; i++) {
			playMode [i].gameObject.SetActive (false);
		}
		playMode [mode].gameObject.SetActive (true);
		playMode [mode].AssignDiamond ();
	}
}
