﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DigDiamondPlay : MonoBehaviour {
	
	public int diamondCount;
	public int allowNo;
	public Image[] tiles;

	[HideInInspector]
	public bool isFull;
	[HideInInspector]
	public Transform[] pos;
	[HideInInspector]
	public Sprite[] sprt;
	[HideInInspector]
	public GameObject board;
	[HideInInspector]
	public GameObject panel;
	[HideInInspector]
	public GameObject miner;
	[HideInInspector]
	public Transform minerPos;
	[HideInInspector]
	public List<Sprite> ansSprt;

	private Image[] ansTile1;
	private Image[] ansTile2;
	private Image[] showQus;
	private Image touchBlocker;
	private List<int> temp_qus;
	private List<Sprite> qusSprt;
	private List<int> rand;

	void Awake()
	{
		GameObject a1 = GameObject.Find ("Question");
		GameObject a2 = GameObject.Find ("Answer");
		GameObject s = GameObject.Find ("ShowPanel");
		ansTile1 = new Image[a1.transform.childCount];
		ansTile2 = new Image[a2.transform.childCount];
		showQus = new Image[s.transform.childCount];
		for (int i = 0; i < a1.transform.childCount; i++) {
			ansTile1 [i] = a1.transform.GetChild (i)
				.GetComponent<Image> ();
			ansTile2 [i] = a2.transform.GetChild (i)
				.GetComponent<Image> ();
			showQus [i] = s.transform.GetChild (i)
				.GetComponent<Image> ();
			showQus [i].gameObject.SetActive (false);
		}
		touchBlocker = GameObject.Find ("TouchBlocker")
			.GetComponent<Image>();
		touchBlocker.raycastTarget = true;
	}

	void Update()
	{
		if (isFull) {
			StartCoroutine (CheckDiamond ());
			isFull = false;
		}
	}

	public void AssignDiamond()
	{
		allowNo = diamondCount;
		tiles = new Image[board.transform.childCount];
		for (int i = 0; i < board.transform.childCount; i++) {
			tiles [i] = board.transform.GetChild (i).GetComponent<Image> ();
		}
		panel.transform.position = pos [0].position;

		rand = new List<int> ();
		for (int i = 0; i < tiles.Length; i++) {
			tiles [i].raycastTarget = true;
			tiles [i].transform.GetChild (0)
				.GetComponent<Image> ().sprite = sprt [0];
			tiles [i].transform.GetChild (0)
				.gameObject.SetActive (false);
			tiles [i].GetComponent<DiamondTile> ().isNewTile = true;
			rand.Add (i);
		}

		temp_qus = new List<int> ();
		qusSprt = new List<Sprite> ();
		ansSprt = new List<Sprite> ();
		for (int i = 0; i < diamondCount; i++) {
			int r = Random.Range (0, rand.Count);
			tiles [rand [r]].transform.GetChild (0)
				.GetComponent<Image> ().sprite = sprt [Random.Range (1, 4)];
			qusSprt.Add (tiles [rand [r]].transform.GetChild (0)
				.GetComponent<Image> ().sprite);			
			temp_qus.Add (rand [r]);
			rand.RemoveAt (r);
		}

		for (int i = 0; i < showQus.Length; i++) {
			showQus [i].gameObject.SetActive (false);
			ansTile1 [i].gameObject.SetActive (false);
			ansTile2 [i].gameObject.SetActive (false);
		}

		miner.transform.position = minerPos.position;
		StartCoroutine (ShowDiamond ());
	}

	IEnumerator ShowDiamond()
	{
		for (int i = 0; i < temp_qus.Count; i++) {
			tiles [temp_qus [i]].transform.GetChild (0)
				.gameObject.SetActive (true);
			showQus [i].gameObject.SetActive (true);
			showQus [i].sprite = qusSprt [i];
			yield return new WaitForSeconds (0.5f);
		}
		yield return new WaitForSeconds (0.5f);
		for (int i = 0; i < temp_qus.Count; i++) {
			tiles [temp_qus [i]].transform.GetChild (0)
				.gameObject.SetActive (false);
		}
		touchBlocker.raycastTarget = false;
	}

	IEnumerator CheckDiamond()
	{
		yield return new WaitForSeconds (0.8f);
		touchBlocker.raycastTarget = true;
		panel.transform.position = pos [1].position;
		panel.GetComponent<Image> ().color = Color.green;
		for (int i = 0; i < qusSprt.Count; i++) {
			ansTile1 [i].gameObject.SetActive (true);
			ansTile2 [i].gameObject.SetActive (true);
			ansTile1 [i].sprite = qusSprt [i];
			ansTile2 [i].sprite = ansSprt [i];
			if (ansSprt[i] != qusSprt[i]) {
				panel.GetComponent<Image> ().color = Color.red;
			}
		}
		yield return new WaitForSeconds (2f);
		panel.GetComponent<Image> ().color = Color.black;
		AssignDiamond ();

	}

}
