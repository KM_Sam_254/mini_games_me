﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PumpkinMove : MonoBehaviour {

	public string path;
	public float min;
	public float max;
	public float rot;
	public float direction;
	private float speed;
	private GameController_Pumpkin play;

	void Start()
	{
		play = GameObject.Find ("GameController").GetComponent<GameController_Pumpkin>();
	}

	public void RollPath()
	{
		speed = Random.Range (min, max);
		this.GetComponent<SpriteRenderer> ().enabled = true;
		iTween.MoveTo(this.gameObject, iTween.Hash(
			"path", iTweenPath.GetPath(path), "easeType", "linear", 
			"time", speed,
			"oncomplete", "LoopNext", "oncompletetarget", this.gameObject));
		iTween.RotateAdd(this.gameObject, iTween.Hash(
			"z", direction,"easeType", "linear", 
			"time", rot, "loopType", "loop"));
		iTween.ScaleTo(this.gameObject, iTween.Hash(
			"x", 0.15f, "y", 0.15f,"easeType", "linear", 
			"time", speed - 0.2f));
	}

	void LoopNext()
	{
		this.transform.position = this.GetComponent<iTweenPath> ().nodes [0];
		this.transform.localScale = new Vector2 (0.1f, 0.1f);
		play.fill.fillAmount -= 0.01f;
		play.ShowPanel ();
		RollPath ();
	}

}
