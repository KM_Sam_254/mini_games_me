﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController_Pumpkin : GameController {

	public PumpkinMove[] pumpkin;
	public Image fill;
	public GameObject slicePanel;
	public GameObject pumpkinPanel;
	private float roll_min;
	private float roll_max;
	private float roll_rot;

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		ChangeDifficulty ();
	}

	protected override void Next()
	{
	}

	protected override void Verify()
	{
	}

	void ChangeDifficulty ()
	{
		if (difficulty == 1) {
			roll_min = 7;
			roll_max = 10;
			roll_rot = 2f;
		} else if (difficulty == 2) {
			roll_min = 5;
			roll_max = 8;
			roll_rot = 1.5f;
		} else if (difficulty == 3) {
			roll_min = 3;
			roll_max = 6;
			roll_rot = 1f;
		}
		slicePanel.SetActive (false);
		slicePanel.SetActive (false);
		for (int i = 0; i < pumpkin.Length; i++) {
			pumpkin [i].min = roll_min;
			pumpkin [i].max = roll_max;
			pumpkin [i].rot = roll_rot;
			pumpkin [i].RollPath ();
		}
		fill.fillAmount = 0.5f;
	}

	public void ShowPanel()
	{
		if (fill.fillAmount == 1) {
			slicePanel.SetActive (true);
		}else if (fill.fillAmount == 0) {
			pumpkinPanel.SetActive (true);
		}
	}
}
