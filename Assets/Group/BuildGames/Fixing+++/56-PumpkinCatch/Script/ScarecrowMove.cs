﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.UI;

public class ScarecrowMove : MonoBehaviour {

	private Animator anim;
	private SpriteRenderer sprite;
	private bool isLeft;
	private bool isRight;
	public Slider slider;
	public ParticleSystem[] particle;
	public ParticleSystem trail;
	private int p;
	private GameController_Pumpkin play;

	void Start()
	{
		anim = this.GetComponent<Animator> ();
		sprite = this.GetComponent<SpriteRenderer> ();
		play = GameObject.Find ("GameController").GetComponent<GameController_Pumpkin>();
	}

	void Update()
	{
		if (isLeft && slider.value > -5.5f) {
			slider.value -= 20f * Time.deltaTime;
			iTween.MoveUpdate(this.gameObject, 
				new Vector3(slider.value, 
					this.transform.position.y, 0f), 0f);
		} else if (isRight && slider.value < 5.5f) {
			slider.value += 20f * Time.deltaTime;
			iTween.MoveUpdate(this.gameObject, 
				new Vector3(slider.value, 
					this.transform.position.y, 0f), 0f);
		}
	
	}

	public void MoveLeft()
	{	
		isLeft = true;
		this.transform.localScale = new Vector2 (-0.5f, 0.5f);
		trail.Play ();
	}

	public void MoveRight()
	{	
		isRight = true;
		this.transform.localScale = new Vector2 (0.5f, 0.5f);
		trail.Play ();
	}

	public void UnTouch()
	{	
		isLeft = false;
		isRight = false;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Tile")) {
			p++;
			other.GetComponent<SpriteRenderer> ().enabled = false;
			particle[p % particle.Length].Play ();
			play.fill.fillAmount += 0.02f;
			play.ShowPanel ();
		}
	}
}
