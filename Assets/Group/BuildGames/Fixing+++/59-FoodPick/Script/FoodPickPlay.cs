﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodPickPlay : MonoBehaviour {

	public Sprite[] sprt;
	public SpriteRenderer[] mochi;
	public SpriteRenderer qusMochi;
	public int maxMochi;
	public FoodPickForkMove fork;
	public GameObject cam;

	void Start()
	{
		RandomMochi ();
	}

	public void RandomMochi()
	{
		maxMochi = Random.Range (5, mochi.Length);
		for (int i = 0; i < mochi.Length; i++) {
			mochi [i].gameObject.SetActive (false);
		}
		for (int i = 0; i < maxMochi; i++) {
			mochi [i].gameObject.SetActive (true);
			mochi [i].sprite = sprt[Random.Range (0, sprt.Length)];
		}
		int rand = Random.Range (0, maxMochi);
		qusMochi.sprite = mochi [rand].sprite;
	}

	public void CheckMochi()
	{
		if (qusMochi.sprite == 
			fork.food.GetComponent<SpriteRenderer>().sprite) {
			iTween.MoveFrom (qusMochi.gameObject, 
				fork.food.transform.position, 0.5f);			
		} else {
			iTween.PunchRotation (cam, 
				new Vector3 (0f, 0f, 20f), 0.5f);
		}
		StartCoroutine (NextChoice ());
	}

	IEnumerator NextChoice()
	{
		yield return new WaitForSeconds (1f);
		fork.food.SetActive (false);
		fork.anim.GetComponent<CircleCollider2D> ().enabled = true;
		fork.isAllowed = true;
		RandomMochi ();
	}
}
