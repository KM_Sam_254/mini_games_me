﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameController_FoodPick : GameController {

	public Sprite[] sprt;
	public SpriteRenderer[] mochi;
	public SpriteRenderer qusMochi;
	public int maxMochi;
	public FoodPickForkMove fork;
	public GameObject cam;
	public TextMeshPro correctTxt;
	private int correctNo = 0;

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		ChangeDifficulty ();
	}

	protected override void Next()
	{
	}

	protected override void Verify()
	{
	}

	void ChangeDifficulty()
	{
		if (difficulty == 1) {
			maxMochi = Random.Range (3, 5);
		}
		else if (difficulty == 2) {
			maxMochi = Random.Range (5, 7);
		}
		else if (difficulty == 3) {
			maxMochi = Random.Range (7, 10);
		}
		RandomMochi ();
	}

	void RandomMochi()
	{
		for (int i = 0; i < mochi.Length; i++) {
			mochi [i].gameObject.SetActive (false);
		}
		for (int i = 0; i < maxMochi; i++) {
			mochi [i].gameObject.SetActive (true);
			mochi [i].sprite = sprt[Random.Range (0, sprt.Length)];
		}
		int rand = Random.Range (0, maxMochi);
		qusMochi.sprite = mochi [rand].sprite;
	}

	public void CheckMochi()
	{
		if (qusMochi.sprite == 
			fork.food.GetComponent<SpriteRenderer>().sprite) {
			iTween.MoveFrom (qusMochi.gameObject, 
				fork.food.transform.position, 0.5f);
			correctNo++;
			correctTxt.text = "x " + correctNo.ToString();
		} else {
			iTween.PunchRotation (cam, 
				new Vector3 (0f, 0f, 20f), 0.5f);
			if (correctNo > 0) {
				correctNo--;
				correctTxt.text = "x " + correctNo.ToString();
			}
		}
		StartCoroutine (NextChoice ());
	}

	IEnumerator NextChoice()
	{
		yield return new WaitForSeconds (2f);
		fork.food.SetActive (false);
		fork.anim.GetComponent<CircleCollider2D> ().enabled = true;
		fork.isAllowed = true;
		ChangeDifficulty ();
	}
}
