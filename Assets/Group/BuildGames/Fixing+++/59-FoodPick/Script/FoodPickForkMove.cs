﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodPickForkMove : MonoBehaviour {

	private bool isLeft;
	private bool isRight;
	public Slider slider;
	public Animator anim;
	public GameObject food;
	public GameController_FoodPick play;
	public bool isAllowed = true;

	void Update()
	{
		if (isLeft && slider.value > -5.5f) {
			slider.value -= 10f * Time.deltaTime;
			iTween.MoveUpdate(this.gameObject, 
				new Vector3(slider.value, 
					this.transform.position.y, 0f), 0f);
		} else if (isRight && slider.value < 5.5f) {
			slider.value += 10f * Time.deltaTime;
			iTween.MoveUpdate(this.gameObject, 
				new Vector3(slider.value, 
					this.transform.position.y, 0f), 0f);
		}
	}

	public void MoveLeft()
	{	
		isLeft = true;
	}

	public void MoveRight()
	{	
		isRight = true;
	}

	public void MoveDown()
	{
		if (isAllowed) {
			anim.SetTrigger ("Down");
		}
	}

	public void UnTouch()
	{	
		isLeft = false;
		isRight = false;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Balls")) {
			food.SetActive (true);
			food.GetComponent<SpriteRenderer> ().sprite = 
				other.GetComponent<SpriteRenderer> ().sprite;
			other.gameObject.SetActive (false);
			anim.GetComponent<CircleCollider2D> ().enabled = false;
			play.CheckMochi ();
			isAllowed = false;
		}
	}
}
