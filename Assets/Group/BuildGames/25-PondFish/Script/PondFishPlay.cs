﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PondFishPlay : MonoBehaviour {

	public GameObject[] koi;
	public Shutter[] shutter;
	public Text shutterTxt;
	public GameObject panelVerify;
	public GameObject input;
	public Text koiCount;
	private List<int> no;
	private int touch_count = 0;
	private int show_count;
	public int shutter_count = 1;
	private int n;

	private int tempKoi;
	private List<int> activeKoi;

	public GameController_PondFish gameController;
	int difficulty = 1;
	public int Difficulty { get { return difficulty; } set { difficulty = value; } }

	public void Next()
	{
		gameController.SetNextInteractable (false);
		gameController.SetDiffcultyBtnInteractable (false);
		HideAll ();
		AddList ();
		for (int i = 0; i < show_count; i++) {
			activeKoi = new List<int> ();
			int rand = Random.Range (0, no.Count);
			koi [no[rand]].SetActive (true);
			float size = Random.Range (0.5f, 1f);
			koi[no[rand]].transform.localScale = 
				new Vector2(size, size);
			koi [no [rand]].GetComponent<KoiMovement> ().MoveKoi ();
			activeKoi.Add (no [rand]);
			no.RemoveAt (rand);
		}

		StartCoroutine (ShowShutter ());
	}

	IEnumerator ShowShutter()
	{
		yield return new WaitForSeconds (Random.Range (3f, 5f));
		shutterTxt.text = "?";
		tempKoi = 0;
		touch_count = 0;
		for (int i = 0; i < shutter_count; i++) {
			shutter[i].gameObject.SetActive (true);
			shutter [i].currentKoiNo = 0;
			int pos = Random.Range (0, activeKoi.Count);
			shutter[i].transform.position = koi[activeKoi[pos]].transform.position;
//			for (int ii = 0; i < koi.Length; ii++) {
//				koi [ii].GetComponent<Image> ().enabled = false;
//				iTween.Pause (koi [ii]);
//			}
//			yield return new WaitForSeconds (1f);
//			for (int ii = 0; i < koi.Length; ii++) {
//				koi [ii].GetComponent<Image> ().enabled = true;
//				iTween.Resume (koi [ii]);
//			}
			yield return new WaitForSeconds (0.5f);
			tempKoi += shutter[i].currentKoiNo;
			Debug.Log (shutter[i].currentKoiNo);
			shutter[i].gameObject.SetActive (false);
			yield return new WaitForSeconds (1.5f);
		}
		input.SetActive (true);
	}

	public void Verify()
	{
		gameController.SetNextInteractable (true);
		gameController.SetDiffcultyBtnInteractable (true);
		panelVerify.SetActive (true);
		koiCount.text = tempKoi.ToString();
		input.SetActive (false);
		iTween.ScaleFrom(panelVerify, new Vector3(0f, 0f, 0f), 0.3f);
        touch_count = 1;
        NumberCircle();
	}

	public void InputShutterNo(Button btn)
	{		
		iTween.ScaleFrom (btn.gameObject, new Vector3 (0.8f, 0.8f, 0f), 0.3f);
		shutterTxt.text = btn.GetComponentInChildren<Text> ().text;
		Verify ();
		//shutter.GetComponent<Animator> ().enabled = false;
		StartCoroutine (Pause());
	}

	void HideAll()
	{
		panelVerify.SetActive (false);
		shutter[0].gameObject.SetActive (false);
		shutter[1].gameObject.SetActive (false);
		for (int i = 0; i < koi.Length; i++) {
			koi [i].GetComponent<Image> ().enabled = true;
			koi [i].SetActive (false);
		}
	}
	void AddList()
	{
		show_count = Random.Range (5 + difficulty, 8 + difficulty);
		no = new List<int> ();
		for (int i = 0; i < koi.Length; i++) {
			no.Add (i);
		}
	}

	public void NumberCircle()
	{
		touch_count++;
		if (touch_count == 1) {	
			InvokeRepeating ("ChangeNumber", 0f, 1f);
		}
		else if (touch_count == 2) {
			CancelInvoke ();
			touch_count = 0;
		}
	}

	IEnumerator Pause()
	{
		yield return new WaitForSeconds (0.5f);
	}
}
