﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shutter : MonoBehaviour {

	public int currentKoiNo;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Koi")) {
			other.GetComponent<Image> ().enabled = true;
			currentKoiNo++;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
	//	currentKoiNo--;
	//	other.GetComponent<Image> ().enabled = false;
	}
}
