﻿
public class GameController_PondFish : GameController
{
	public PondFishPlay gameScript;

	public override void SetGame()
	{
		base.SetGame();

		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		Debug.Log("Pond Fish Start");
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		gameScript.Difficulty = difficulty;
		if (difficulty <= 2) {
			gameScript.shutter_count = 1;
		} else if (difficulty <= 5) {
			gameScript.shutter_count = 2;
		} else {
			gameScript.shutter_count = difficulty - 3;
		}
	}

	protected override void Next()
	{
		gameScript.Next();
	}

	protected override void Verify()
	{
	}

	void GameMode(int mode)
	{
		
	}
}

