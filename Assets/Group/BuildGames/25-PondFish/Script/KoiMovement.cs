﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KoiMovement : MonoBehaviour {

	[HideInInspector]
	public List<Transform> koiPos;
	private Transform tempPos;
	private int current;
	private Vector3 dir;
	private float angle;

	public void MoveKoi()
	{		
		current = Random.Range (0, 17);
		tempPos = koiPos [current];

		dir = koiPos [current].transform.position - 
			this.transform.position;
		angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

		iTween.MoveTo (this.gameObject, iTween.Hash (
			"position", tempPos.position, "easeType", "linear",
			"speed", 150f, "oncomplete", "MoveNext", "oncompletetarget",
			this.gameObject));
	}

	void MoveNext()
	{			
		MoveKoi ();
	}

}
