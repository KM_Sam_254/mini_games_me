﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alphabet : MonoBehaviour {

	public bool isAnim = false;
	private float delay;
	private int count;
	private float x;
	private float y;

	void Start()
	{
		delay = 0;
		count = 0;
	}

	public void RandomMovement()
	{	
		if (isAnim == true) {
			delay = Random.Range (0f, 0.5f);
			iTween.MoveAdd (this.gameObject, iTween.Hash("x", x, "y", y,
				"easeType", "linear", "time", 2f, "delay", delay,
				"oncomplete","RandomFloat",
				"oncompletetarget", this.gameObject));
		} else {
			delay = 0; 
			count = 0;
		}
	}

	void RandomFloat()
	{
		if (count % 2 == 1) {
			x = Random.Range (0f, 50f);
			y = Random.Range (0f, 50f);
			iTween.ScaleTo (this.gameObject, new Vector3 (1.5f, 1.5f, 0f), 2f);
//			iTween.ScaleTo (this.gameObject, iTween.Hash("x", 1.5f, "y", 1.5f,
//				"easeType", "linear", "time", 2f));
		}
		else {
			iTween.ScaleTo (this.gameObject, new Vector3 (1f, 1f, 0f), 2f);
			x = Random.Range (0f, -50f);
			y = Random.Range (0f, -50f);
		}
		count++;
		RandomMovement ();
	}

}
