﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordPlay : MonoBehaviour {

	private GameObject conPanel;
	private GameObject vowPanel;
//	private GameObject veriPanel;
	private GameObject conPosPanel;
	private GameObject vowPosPanel;
	public GameObject[] consonant;
	public GameObject[] vowel;
	private Transform[] conPos;
	private Transform[] vowPos;
	public GameObject next;
	public Transform[] show;
	public int ShowCon = 7;
	public int ShowVow = 5;
	private bool isShow = false;
	private List<int> noVow;
	private List<int> noCon;

	void Start()
	{
		Add ();
	}

	public void Next()
	{	
		isShow = true;	
		ResetPosition ();
		ShowAlphabet ();
		next.SetActive (false);
//		next.interactable = false;
//		verify.interactable = true;
	}

	public void Verify()
	{	 
		isShow = false;	
		HideAlphabet ();
//		verify.interactable = false;
	}

	void Add()
	{
		conPanel = GameObject.Find ("Consonant");
		conPosPanel = GameObject.Find ("ConsonantPos");
		consonant = new GameObject[14];
		conPos = new Transform[14];
		noCon = new List<int> ();
		for (int i = 0; i < consonant.Length; i++) {
			consonant[i] = conPanel.transform.GetChild (i).gameObject;
			conPos [i] = conPosPanel.transform.GetChild (i).
				GetComponent<Transform> ();
			noCon.Add (i);
		}

		vowPanel = GameObject.Find ("Vowel");
		vowPosPanel = GameObject.Find ("VowelPos");
		vowel = new GameObject[10];
		vowPos = new Transform[10];
		noVow = new List<int> ();
		for (int i = 0; i < vowel.Length; i++) {
			vowel [i] = vowPanel.transform.GetChild (i).gameObject;
			vowPos [i] = vowPosPanel.transform.GetChild (i).
				GetComponent<Transform> ();
			noVow.Add (i);
		}
	}

	void ResetPosition()
	{
		for (int i = 0; i < consonant.Length; i++) {
			consonant [i].transform.position = conPos [i].position;
			consonant [i].gameObject.SetActive (false);
		}
		for (int i = 0; i < vowel.Length; i++) {
			vowel [i].transform.position = vowPos [i].position;
			vowel [i].gameObject.SetActive (false);
		}
	}

	void MoveAlphabet()
	{
		next.SetActive (true);
		ShowCon = Random.Range (2, 4);
		for (int i = 0; i < ShowCon; i++) {
			int r = Random.Range (0, noCon.Count);
			consonant [noCon [r]].SetActive (true);
			iTween.MoveTo (consonant [noCon[r]], conPos [noCon[r]].position, 1f);
			if (isShow == true) {
				consonant [noCon[r]].GetComponent<Alphabet> ().isAnim = true;
			} else {
				consonant [noCon[r]].GetComponent<Alphabet> ().isAnim = false;
			}
			consonant [noCon[r]].GetComponent<Alphabet> ().RandomMovement ();

		}

		ShowVow= Random.Range (2, 4);
		for (int i = 0; i < ShowVow; i++) {
			int r = Random.Range (0, noVow.Count);
			vowel [noCon [r]].SetActive (true);
			iTween.MoveTo (vowel [noCon[r]], vowPos [noCon[r]].position, 1f);	
			if (isShow == true) {
				vowel [noCon[r]].GetComponent<Alphabet> ().isAnim = true;
			} else {
				vowel [noCon[r]].GetComponent<Alphabet> ().isAnim = false;
			}
			vowel [noCon[r]].GetComponent<Alphabet> ().RandomMovement ();
		}
	}

	void ShowAlphabet ()
	{
		iTween.MoveTo (conPosPanel, iTween.Hash("position", show[0].position,
			"time", 0f,
			"oncomplete","MoveAlphabet",
			"oncompletetarget", this.gameObject));
		iTween.MoveTo (vowPosPanel, iTween.Hash("position", show[0].position,
			"time", 0f,
			"oncomplete","MoveAlphabet",
			"oncompletetarget", this.gameObject));
	//	iTween.MoveTo (veriPanel, show [3].position, 1f);
	}

	void HideAlphabet()
	{
		iTween.MoveTo (conPosPanel, iTween.Hash("position", show[1].position,
			"time", 0f,
			"oncomplete","MoveAlphabet",
			"oncompletetarget", this.gameObject));
		iTween.MoveTo (vowPosPanel, iTween.Hash("position", show[2].position,
			"time", 0f,
			"oncomplete","MoveAlphabet",
			"oncompletetarget", this.gameObject));
	//	iTween.MoveTo (veriPanel, show [0].position, 1f);
	//	iTween.PunchScale (veriPanel, new Vector3 (0.5f, 0.5f, 0f), 1f);
	}
}
