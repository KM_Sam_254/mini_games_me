﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EngPhrase : MonoBehaviour {

	public string[] phrase;
	public AudioClip[] sound;
	public AudioClip[] sound_slow;

	public void AssignSound(int clip)
	{
		this.GetComponent<Image> ().enabled = false;
		this.GetComponentInChildren<Text> ().text = "";
		this.GetComponent<AudioSource> ().clip = sound [clip];
		this.GetComponent<AudioSource> ().Play ();
	}

	public void AssignPhraseNsound(int phraseNclip)
	{
		this.GetComponent<Image> ().enabled = true;
		this.GetComponentInChildren<Text> ().text = phrase [phraseNclip];
		this.GetComponent<AudioSource> ().clip = sound_slow [phraseNclip];
		this.GetComponent<AudioSource> ().Play ();
	}
}
