﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EngPhrasePlay : MonoBehaviour {

	public EngPhrase[] phrase;
	public GameObject next;
	public GameObject verify;
	private int random_clip;
	private List<int> index;

	void Start()
	{
		AddList ();
	}

	void AddList()
	{
		index = new List<int> ();
		for (int i = 0; i < phrase[0].phrase.Length; i++) {
			index.Add (i);
		}
	}

	public void Next()
	{
		next.SetActive (false);
		verify.SetActive (true);
		random_clip = Random.Range (0, index.Count);
		phrase[0].AssignSound (index[random_clip]);
	}

	public void Verify()
	{
		next.SetActive (true);
		verify.SetActive (false);
		phrase[0].AssignPhraseNsound (index[random_clip]);
		phrase[0].gameObject.SetActive (true);
		iTween.ScaleFrom(phrase[0].gameObject, new Vector3(3f, 3f, 0f), 1f);
		index.RemoveAt (random_clip);
		if (index.Count < 1) {
			AddList ();
		}
	}

}
