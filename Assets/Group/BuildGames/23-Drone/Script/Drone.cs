﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Drone : MonoBehaviour {

	public GameObject verifyPanel;
	public GameObject verifyDog;
	public GameObject verifyWoman;
	public int goPos = 4;
	public Transform origin;
	//public Transform[] pos;
	public Transform[] dropPos;
	public GameObject box;
	public GameObject dogOne;
	public GameObject dogTwo;
	public Transform dogOnePos;
	public Transform dogTwoPos;
	public Sprite dogspt;
    public GameObject[] women;

    int difficulty = 0;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    public GameController_Drone gameController;

    public void MoveDrone()
	{
		goPos = Random.Range (1, 8);
		StartCoroutine (Move ());
        gameController.SetNextInteractable(false);
        gameController.SetDiffcultyBtnInteractable(false);
		verifyPanel.SetActive (false);
		verifyDog.SetActive (false);
		verifyWoman.SetActive (false);
	}

	public void Verify()
	{
        gameController.SetVerifyInteractable(false);
        gameController.SetNextInteractable(true);
        gameController.SetDiffcultyBtnInteractable(true);
        verifyPanel.SetActive (true);
		Reset ();
	}

	IEnumerator Move()
	{			
		for (int i = 0; i <= goPos; i++) {
			iTween.MoveTo (this.gameObject, iTween.Hash (
                //"position", pos [i].position, "time", 2f,
                "position", dropPos[i].position, "time", 2f,
                "easeType", "linear"
			));
			yield return new WaitForSeconds (2f);

			if (i == goPos) {
				iTween.MoveTo (this.gameObject, iTween.Hash (
					"position", dropPos [i].position, "time", 2f,
					"easeType", "linear"
				));
				yield return new WaitForSeconds (1.5f);

				box.SetActive (false);
				dropPos [i].gameObject.SetActive (true);
				StartCoroutine (DogOneMove ());
				StartCoroutine (DogTwoMove ());
				yield return new WaitForSeconds (1f);

				iTween.MoveTo (this.gameObject, iTween.Hash (
                    //"position", pos [i].position, "time", 1f,
                    "position", dropPos[i].position, "time", 1f,
                    "easeType", "linear"
				));
				yield return new WaitForSeconds (1f);

				iTween.MoveTo (this.gameObject, iTween.Hash (
					"position", origin.position, "time", 1f,
					"easeType", "linear"
				));
                float wait = Random.Range(1f, 2f);
              	yield return new WaitForSeconds(wait);
                women[goPos].SetActive(true);
                dropPos[goPos].gameObject.SetActive(false);

			}
		}
	}

	void Reset()
	{
		iTween.MoveTo (dogOne, iTween.Hash (
			"position", dogOnePos.position, "time", 2f,
			"easeType", "linear"
		));
		iTween.MoveTo (dogTwo, iTween.Hash (
			"position", dogTwoPos.position, "time", 2f,
			"easeType", "linear"
		));
		iTween.ScaleTo(dogOne, new Vector3(1f, 1f, 0f), 0f);
		iTween.ScaleTo(dogTwo, new Vector3(-1f, 1f, 0f), 0f);
		dogOne.GetComponentInChildren<Animator> ().enabled = true;
		dogOne.GetComponentInChildren<Animator> ().Play ("");
		dogOne.transform.GetChild (1).gameObject.SetActive (false);

		dogTwo.GetComponentInChildren<Animator> ().enabled = true;
		dogTwo.GetComponentInChildren<Animator> ().Play ("");
		dogTwo.transform.GetChild (1).gameObject.SetActive (false);
		box.SetActive (true);
		for (int i = 0; i < dropPos.Length; i++) {
			dropPos [i].gameObject.SetActive (false);
            women[i].SetActive(false);
		}
	}

	IEnumerator DogOneMove()
	{
		dogOne.GetComponentInChildren<Animator> ().enabled = true;
		dogOne.GetComponentInChildren<Animator> ().Play ("");
		verifyWoman.SetActive (true);
		iTween.ScaleTo(dogOne, new Vector3(-1f, 1f, 0f), 0f);
		for (int i = 0; i <= goPos; i++) {
			iTween.MoveTo (dogOne, iTween.Hash (
				"position", dropPos [i].position, "time", 1f,
				"easeType", "linear"
			));
			yield return new WaitForSeconds (1f);
			if (i == goPos) {
				if (women[goPos].activeInHierarchy == false) {
					dropPos[goPos].gameObject.SetActive(false);
					dogOne.transform.GetChild (1).gameObject.SetActive (true);
					iTween.MoveTo (dogOne, iTween.Hash (
						"position", dogOnePos.position, "time", 2f,
						"easeType", "linear"
					));
					iTween.ScaleTo(dogOne, new Vector3(1f, 1f, 0f), 0f);
					women [goPos].transform.
					GetChild (0).gameObject.SetActive (false);
					verifyDog.SetActive (true);
					verifyWoman.SetActive (false);
				} else {
					dogOne.GetComponentInChildren<Animator> ().enabled = false;
					dogOne.transform.GetChild (0)
						.GetComponent<Image> ().sprite = dogspt;
                    gameController.SetVerifyInteractable(true);
				}
			}
		}
	}
	IEnumerator DogTwoMove()
	{
		dogTwo.GetComponentInChildren<Animator> ().enabled = true;
		dogTwo.GetComponentInChildren<Animator> ().Play ("");
		verifyWoman.SetActive (true);
		iTween.ScaleTo(dogTwo, new Vector3(1f, 1f, 0f), 0f);
		for (int i = 7; i >= goPos; i--) {
			iTween.MoveTo (dogTwo, iTween.Hash (
				"position", dropPos [i].position, "time", 1f,
				"easeType", "linear"
			));
			yield return new WaitForSeconds (1f);
			if (i == goPos) {				
				if (women[goPos].activeInHierarchy == false) {
					dropPos[goPos].gameObject.SetActive(false);
					dogTwo.transform.GetChild (1).gameObject.SetActive (true);
					iTween.MoveTo (dogTwo, iTween.Hash (
						"position", dogTwoPos.position, "time", 2f,
						"easeType", "linear"
					));
					iTween.ScaleTo(dogTwo, new Vector3(-1f, 1f, 0f), 0f);
					women [goPos].transform.
					GetChild (0).gameObject.SetActive (false);
					verifyDog.SetActive (true);
					verifyWoman.SetActive (false);
				} else {
					dogTwo.GetComponentInChildren<Animator> ().enabled = false;
					dogTwo.transform.GetChild (0)
						.GetComponent<Image> ().sprite = dogspt;
                    gameController.SetVerifyInteractable(true);
				}
			}
		}
	}
}
