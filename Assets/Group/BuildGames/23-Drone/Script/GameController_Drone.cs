﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_Drone : GameController
{
    public Drone gameScript;

    void Start()
    {
        SetGame();
    }

    public override void SetGame()
    {
        base.SetGame();

        GameManager.Instance.OnClientGameReady();
    }

    public override void HomeBtnClicked()
    {
        SoundMgr.Instance.Resume_BGM();
        SoundMgr.Instance.ClickSoundMute = false;
        SceneMgr.Instance.GoGroupMenu();
    }

    public override void StartGame()
    {
        hasGameStarted = true;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register task that have to be excuted before the game start
    }

    protected override void Next()
    {
        gameScript.MoveDrone();
    }

    public void TestRandom()
    {
        int[] randomArray = SimpleRandom.GetRandomArray(0, 6, 5);
        List<int> randomList = SimpleRandom.GetRandomList(0, 6, 5);

        Debug.Log(" ==== Random Array ====");
        for (int i = 0; i < randomArray.Length; i++)
        {
            Debug.Log("i : " + i + "  array : " + randomArray[i] + "  list : " + randomList[i]);
        }
    }

    protected override void Verify()
    {
        gameScript.Verify();
    }

    protected override void SetDifficulty(int difficulty)
    {
        difficulty -= 1;
        Debug.Log("Set difficulty : " + difficulty);
        this.difficulty = difficulty;
        gameScript.Difficulty = difficulty;
    }

    protected override void NextBtnClicked()
    {
        if (hasStageStarted) return;

        Next();

        isCheckingResult = false;
        hasStageStarted = true;
    }

    protected override void CheckResultClicked()
    {
        if (isCheckingResult || !hasStageStarted) return;

        Verify();

        isCheckingResult = true;
        hasStageStarted = false;
    }
}
