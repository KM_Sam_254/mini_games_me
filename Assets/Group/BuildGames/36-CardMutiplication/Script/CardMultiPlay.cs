﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardMultiPlay : MonoBehaviour {

	public GameObject next;
	public GameObject verify;
	public GameObject diff;
	public int mode;
	private GameObject qus;
	private Image[] card;
	private GameObject[] sign;
	private GameObject[] cover;
	private int[] r;
	public Sprite[] sp;
	private int c = 0;

    public GameController_CardMulti gameController;

    void Awake()
	{
		mode = 1;
		qus = GameObject.Find ("Qus");
		card = new Image[6];
		sign = new GameObject[2];
		cover = new GameObject[6];
		r = new int[6];
		for (int i = 0; i < card.Length; i++) {
			card[i] = qus.transform.GetChild (i).GetComponent<Image> ();
			cover [i] = card [i].gameObject.transform.GetChild (0).gameObject;
		}
		sign [0] = qus.transform.GetChild (4).gameObject;
		sign [1] = qus.transform.GetChild (5).gameObject;
		Next ();
	}

	public void Next()
	{
        HideCard ();
		RandomCard ();		
		StartCoroutine (MoveCard ());
		next.SetActive (false);
		verify.SetActive (false);
		diff.SetActive (false);
	}

	public void Verify()
	{
		for (int i = 0; i < card.Length; i++) {
			cover [i].SetActive (false);
			iTween.PunchScale(card[i].gameObject, 
				new Vector3 (0.5f, 0.5f, 0f), 1f);
		}
		next.SetActive (true);
		verify.SetActive (false);
		diff.SetActive (true);
        //iTween.PunchScale(next.gameObject, 
        //	new Vector3 (0.5f, 0.5f, 0f), 1f);
	}

	IEnumerator MoveCard()
	{
		for (int i = 0; i < card.Length - 1; i++) {
			card [i].gameObject.SetActive (true);
			iTween.MoveFrom (card [i].gameObject, iTween.Hash ("y", 1500f, "time", 1f));
			yield return new WaitForSeconds (1.2f);
		}
        sign [0].gameObject.SetActive (true);
        sign [1].gameObject.SetActive (true);
        iTween.MoveFrom (sign [0].gameObject, iTween.Hash ("y", 1500f, "time", 2f));
		verify.SetActive (true);
		gameController.SetVerifyInteractable (true);
        //iTween.PunchScale(verify.gameObject, 
        //    new Vector3 (0.5f, 0.5f, 0f), 1f);
	}

	void HideCard()
	{
		for (int i = 0; i < card.Length; i++) {
			card[i].gameObject.SetActive(false);
		}
		sign [0].gameObject.SetActive (false);
		sign [1].gameObject.SetActive (false);
	}


	void RandomCard()
	{		
        if (mode == 1)
        {    
            card[4].enabled = false;
            card[5].enabled = false;
            r[0] = Random.Range(2, 9);
            r[1] = Random.Range(2, 9);
            r[2] = (r[0] * r[1]) / 10;
            r[3] = (r[0] * r[1]) % 10;
            Debug.Log(r[2]);
        }
		else if (mode == 2) {	
            card[4].enabled = true;
            card[5].enabled = false;
            r[4] = Random.Range(1, 3);
            r[0] = Random.Range(1, 5);         
			r[1] = Random.Range(2, 5);			
            r[2] = (((10 * r[4]) + r[0]) * r[1]) / 10;
            r[3] = (((10 * r[4]) + r[0]) * r[1]) % 10;
        } 
        else if (mode == 3){
            card[4].enabled = true;
            card[5].enabled = true;
            r[4] = Random.Range(5, 9);
            r[0] = Random.Range(1, 9);         
            r[1] = Random.Range(2, 9);  
            if ((((10 * r[4]) + r[0]) * r[1]) > 99)
            {
                r[2] = (((10 * r[4]) + r[0]) * r[1]) / 100;
                int temp = (((10 * r[4]) + r[0]) * r[1]) / 10;
                r[3] = temp % 10;
                r[5] = (((10 * r[4]) + r[0]) * r[1]) % 10;
                Debug.Log(r[4]); Debug.Log(r[0]);
                Debug.Log(r[1]); 
              //  Debug.Log(r[2]);
              //  Debug.Log(r[3]);

            }
            else
            {
//                r[2] = 0;
//                r[3] = (((10 * r[4]) + r[0]) * r[1]) / 10;
//                r[5] = (((10 * r[4]) + r[0]) * r[1]) % 10;
            }
		}
		for (int i = 0; i < r.Length; i++) {
			card [i].sprite = sp [r[i]];
			cover [i].SetActive(false);
		}
		cover [Random.Range(0, 5)].SetActive(true);
		c++;
	}

}
