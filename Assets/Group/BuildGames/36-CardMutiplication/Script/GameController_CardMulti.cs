﻿using UnityEngine.UI;
using UnityEngine;

public class GameController_CardMulti : GameController
{
    public CardMultiPlay play;

    void Start()
    {
        SetGame();
    }

    public override void SetGame()
    {
        base.SetGame();
        GameManager.Instance.OnClientGameReady();
    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    public override void StartGame()
    {
        hasStageStarted = true;
		this.difficulty = 1;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register task that have to be excuted before the game start
    }

    protected override void Next()
	{
		play.Next ();
	}

    protected override void Verify()
    {
		play.Verify();
    }

    protected override void SetDifficulty(int difficulty)
    {
        Debug.Log("Set difficulty : " + difficulty);
        this.difficulty = difficulty;
    }
}
