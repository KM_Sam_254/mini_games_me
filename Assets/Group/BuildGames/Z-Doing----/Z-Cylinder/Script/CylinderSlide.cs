﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylinderSlide : MonoBehaviour {

    public RuntimeAnimatorController touch;
    public RuntimeAnimatorController drag;
    public RuntimeAnimatorController roll;

    public void TouchCylin()
    {
        this.GetComponent<Animator>().enabled = true;
        this.GetComponent<Animator>().runtimeAnimatorController = touch;
        this.GetComponent<Animator>().Play("");
    }

    public void DragCylin()
    {
        this.GetComponent<Animator>().runtimeAnimatorController = drag;
    }

    public void RollCylin()
    {
        this.GetComponent<Animator>().runtimeAnimatorController = roll;
    }
}
