﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_BonFire : GameController {

	public BonfirePlay play;

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		play.mode = difficulty;
	}

	protected override void Next()
	{
		play.Next ();
	}

	protected override void Verify()
	{
		play.Verify ();
	}
}
