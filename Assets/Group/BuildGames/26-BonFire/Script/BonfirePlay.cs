﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonfirePlay : MonoBehaviour {

	public int mode;
	public GameObject next;
	public GameObject verify;
	public GameObject difficulty;
	public List<GameObject> bubble;
    public List<int> no;
    public List<int> noCount;
    public Sprite spb_bubble;
    public GameObject panel;
    public Text number;
    public Text totalNo;
	public int total;
	private int rnd;
    private int pop;
	private int totalBubble;
    private int temp;
	private GameObject bub;

    public void Next()
    {      
        AddList ();
      	StartCoroutine(BubbleMovement());
        panel.SetActive(false);
		next.SetActive (false);
		verify.SetActive (true);
		difficulty.SetActive (false);
		totalBubble = 0;
		pop = 0;
    }

	void AddList()
	{
		bubble = new List<GameObject> ();
        noCount = new List<int>();
		bub = GameObject.Find ("Bubble");
		for (int i = 0; i < bub.transform.childCount; i++) {
			bubble.Add(bub.transform.GetChild(i).gameObject);
            noCount.Add(i);
		}
        temp = 6;
	}

    public void Verify()
    {
        StopAllCoroutines();
        panel.SetActive(true);
        number.text = pop.ToString();
        totalNo.text = totalBubble.ToString();
        iTween.ScaleFrom(panel, new Vector3(0f, 0f, 0f), 1f);
		next.SetActive (true);
		difficulty.SetActive (true);
    }

    IEnumerator BubbleMovement()
    {	
		for (int i = 0; i < total; i++) {
            yield return new WaitForSeconds (1f);
            BubbleShow();
			yield return new WaitForSeconds (0.5f);
            BubbleUp();
            yield return new WaitForSeconds (1f);
            BubblePop();
            yield return new WaitForSeconds (1f);
            BubbleReset(); 
            if (i == total - 1)
            {
                i = 0;
            }
		}
    }   

    void BubbleShow()
    {        
        no.Clear();
        AddList();
		for (int i = 0; i < mode; i++)
        {              
            rnd = Random.Range (0, noCount.Count);
            no.Add(noCount[rnd]);
            noCount.RemoveAt(rnd);
            float size = Random.Range (1f, 1.5f);
            bubble [no[i]].transform.localScale = new Vector3 (size, size, 0f);
            bubble[no[i]].SetActive(true);
            totalBubble++;
        }
    }

    void BubbleUp()
    {
		for (int i = 0; i < mode; i++)
        {
            iTween.MoveAdd(bubble[no[i]], iTween.Hash(
                "y", 100f, "easeType", "linear", "time", 1f));
            bubble [no[i]].GetComponent<Image> ().raycastTarget = true;
        }
    }
    void BubblePop()
    {
		for (int i = 0; i < mode; i++)
        {
            if (i != temp)
            {
                bubble [no[i]].GetComponent<Animator> ().enabled = true;
                bubble [no[i]].GetComponent<Animator> ().Play ("bubble");
            }
        }
    }

    void BubbleReset()
    {
		for (int i = 0; i < mode; i++)
        {
            bubble [no[i]].GetComponent<Animator> ().enabled = false;
            bubble [no[i]].GetComponent<Image> ().sprite = spb_bubble;
            bubble [no[i]].transform.localScale = new Vector3 (1f, 1f, 0f);
			iTween.MoveTo(bubble[no[i]], iTween.Hash(
				"y", bub.transform.position.y, "easeType", "linear", "time", 0f));                
            bubble [no[i]].gameObject.SetActive (false);
        }
    }

    public void PopBubble(int b)
    {
        bubble[b].GetComponent<Animator>().enabled = true;
        bubble[b].GetComponent<Animator>().Play("bubble");
        iTween.PunchScale(bubble[b], new Vector3(2f, 2f, 0f), 0.5f);
		bubble [b].GetComponent<Image> ().raycastTarget = false;
        b = temp;
        pop++;
    }
}
