﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PuzzlePlay : MonoBehaviour {

	public GameController_Puzzle controller;
	public Sprite[] qusImage;
	public GameObject[] jigsaw;
	public GameObject[] frame;
	private GameObject[] jigsawPiece;
	public List<int> piece;
	private int h;
	private int rand;

    int difficulty = 1;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    bool isPuzzleSet = false;

    void Start()
	{
		piece = new List<int> ();
	}

	void ChangePicture ()
	{
		piece.Clear ();
		for (int i = 0; i < jigsaw.Length; i++) {
			jigsaw [i].SetActive (false);
			frame [i].SetActive (false);
		}
        jigsaw [Difficulty - 1].SetActive (true);
		frame [Difficulty - 1].SetActive (true);
        jigsawPiece = new GameObject[jigsaw [Difficulty - 1].transform.childCount];
		for (int i = 0; i < jigsawPiece.Length; i++) {
            jigsawPiece [i] = jigsaw[Difficulty - 1].transform.GetChild (i).gameObject.
				transform.GetChild (0).gameObject;
			jigsawPiece [i].gameObject.SetActive (true);
			piece.Add (i);
		}
        jigsaw[Difficulty - 1].GetComponent<Image> ().sprite = 
            qusImage [Random.Range(0, qusImage.Length)];

        isPuzzleSet = true;
	}

	public void Next()
	{	
        if (!isPuzzleSet) ChangePicture();
        else HidePuzzle();
		Debug.Log (Difficulty);
    }

	public void Verify()
	{
        StartCoroutine(HideAll());
        isPuzzleSet = false;
    }

	void Hide()
	{
		jigsawPiece [piece[h]].SetActive (false);
		piece.RemoveAt (h);
	}

	void HidePuzzle()
	{
		if (piece.Count > 2) {			
			h = Random.Range (piece[0], piece.Count);
			iTween.PunchScale (jigsawPiece[piece[h]], new Vector3 (1.5f, 1.5f, 0f), 1f);
			iTween.PunchRotation (jigsawPiece[piece[h]], iTween.Hash("z", Random.Range (-180f, 180f), "time", 1f,
				"oncomplete", "Hide", "oncompletetarget", this.gameObject));			
		} else {
			Verify ();
		}
	}

	IEnumerator HideAll()
	{
		while (piece.Count > 0) {
			for (int i = 0; i < piece.Count; i++) {
				iTween.PunchScale (jigsawPiece [piece[i]], new Vector3 (1.5f, 1.5f, 0f), 0.3f);
				iTween.PunchRotation (jigsawPiece [piece[i]], iTween.Hash("z", 
					Random.Range (-180f, 180f), "time", 0.3f));
				yield return new WaitForSeconds (0.3f);
				jigsawPiece [piece[i]].SetActive (false);
				piece.RemoveAt (i);
			}
		}
	}
}
