﻿using UnityEngine.UI;
using UnityEngine;

public class GameController_Puzzle : GameController
{

    public PuzzlePlay gameScript;

    void Start()
    {
        SetGame();
    }

    public override void SetGame()
    {
        base.SetGame();

        GameManager.Instance.OnClientGameReady();
    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    public override void StartGame()
    {
        hasGameStarted = true;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register task that have to be excuted before the game start
    }

    protected override void Next()
    {		
        gameScript.Next();
    }

    protected override void Verify()
    {
        gameScript.Verify();
    }

    protected override void SetDifficulty(int difficulty)
    {
        Debug.Log("Set difficulty : " + difficulty);
        this.difficulty = difficulty;
        gameScript.Difficulty = difficulty;
    }
}
