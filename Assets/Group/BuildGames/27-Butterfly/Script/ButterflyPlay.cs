﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButterflyPlay : GameController
{
    public GameObject flightOrderPanel;
    public Button btnFlight1, btnFlight2, btnFlight3, btnFlight4;
    public GameObject answerPanel;

    public Animator allFly;
    public Animator[] butterfly;
    public Animator[] ansFly;
    public RuntimeAnimatorController[] flyAnim;
    public GameObject ans;
    public Transform ansPos;
    public Transform hidePos, moveEndPos;
    public List<int[]> sequence;
    public Text txt;
    public string[] round;
    Vector3 butterStartPos;
    Vector3[] flightPath;

    int butterSequenceCount = 2;
    public int ButterSequenceCount { get { return butterSequenceCount; } set { butterSequenceCount = value; } }

    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    int flightOrder = 0;
    public int FlightOrder { get { return flightOrder; } set { flightOrder = value; } }

    public override void SetGame()
    {
        base.SetGame();

        difficulty = 1;
        flightPath = iTweenPath.GetPath("flightPath");
        butterStartPos = butterfly[0].transform.position;
        sequence = new List<int[]>();

        // 결과확인할때 비행 순서 선택용 버튼
        btnFlight1.GetComponent<ButtonEventDelegator>().AddListener(ShowFlightResult);
        btnFlight2.GetComponent<ButtonEventDelegator>().AddListener(ShowFlightResult);
        btnFlight3.GetComponent<ButtonEventDelegator>().AddListener(ShowFlightResult);
        btnFlight4.GetComponent<ButtonEventDelegator>().AddListener(ShowFlightResult);

        GameManager.Instance.OnClientGameReady();
    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    public override void StartGame()
    {
        hasGameStarted = true;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register task that have to be excuted before the game start
    }

    protected override void Verify()
    {

    }

    protected override void SetDifficulty(int difficulty)
    {
        if (difficulty > -1)
        {
            Debug.Log("Set difficulty : " + difficulty);
            this.difficulty = difficulty;
            Difficulty = difficulty;
        }
        else HideFlightResult();
    }

    void ShowFlightResult(string order)
    {
        Debug.Log("ShowFlightResult : " + order);
        FlightOrder = ParseUtil.ParseStringToInt(order, 0);

        flightOrderPanel.GetComponent<FlighSequencePanel>().HideBtns(); ;
        
        flightOrderPanel.SetActive(false);
        answerPanel.SetActive(true);

        Verify(ParseUtil.ParseStringToInt(order, 1));
    }

    void HideFlightResult()
    {
        flightOrderPanel.GetComponent<FlighSequencePanel>().HideBtns(); ;
        flightOrderPanel.SetActive(false);
        answerPanel.SetActive(false);
        ResetAnsButterPosition();
    }

    protected override void CheckResultClicked()
    {
        btnDifficulty.GetComponent<DifficultyButton>().HideDifficultyBtns();
        ResetAnsButterPosition();

        flightOrderPanel.SetActive(true);
        answerPanel.SetActive(false);

        flightOrderPanel.GetComponent<FlighSequencePanel>()
            .ShowBtns(GetSequenceCount());

        isCheckingResult = true;
        hasStageStarted = false;
    }

    protected override void NextBtnClicked()
    {
        if (hasStageStarted) return;

        HideFlightResult();

        Next();

        //difficultyBtn.SetActive(false);
        isCheckingResult = false;
        hasStageStarted = true;

        btnNext.GetComponent<ButtonNext>().SetInteractable(false);
        //btnCheckResult.GetComponent<Button>().interactable = true;

        flightOrderPanel.GetComponent<FlighSequencePanel>().HideBtns(); ;
        btnDifficulty.GetComponent<DifficultyButton>().HideDifficultyBtns();
    }

    private void Start()
    {
        SetGame();
    }

    protected override void Next()
    {
        isCheckingResult = false;
        hasStageStarted = true;
        answerPanel.SetActive(false);

        txt.text = "";
		sequence.Clear();
        array[0] = array[1] = array[2] = array[3] = -1;
        btnDifficulty.GetComponent<Button>().interactable = false;
        btnCheckResult.GetComponent<Button>().interactable = false;
        StartCoroutine(GoButterfly(0));
    }

    public void Verify(int sequenceOrder)
    {
        sequenceOrder -= 1;
        Debug.Log("Verify : " + sequenceOrder);
        txt.text = round[sequenceOrder];
        iTween.MoveTo(ans, ansPos.position, 1f);
        for (int i = 0; i < ansFly.Length; i++)
        {
            int s = sequence[sequenceOrder][i];
            Debug.Log("s : " + s );
            ansFly[i].runtimeAnimatorController = flyAnim[s];
            iTween.PunchScale(ansFly[i].gameObject, new Vector3(1f, 1f, 0f), 1f);
        }
    }

    IEnumerator GoButterfly(float time)
    {
        yield return new WaitForSeconds(time);
        
        StartCoroutine(ChangeButterflies());
    }

    int[] array = new int[4];
    IEnumerator ChangeButterflies()
    {
        for (int i = 0; i < butterfly.Length; i++)
        {
            int rand = GetRandomButterAnim();
            Debug.Log("Set Butter : " + rand);
            butterfly[i].runtimeAnimatorController = flyAnim[rand];
            array[i] = rand;

            butterfly[i].GetComponent<ButterflyTween>().Fly(flightPath);

            yield return new WaitForSeconds(1f);
        }
        //allFly.enabled = true;
        //allFly.Play("");
    }

    public void OnButterMoveEnd(GameObject butterfly)
    {
        Debug.Log("Flight end : " + butterfly.name);
        butterfly.transform.position = butterStartPos;

        // Butterfly move complete
        if (butterfly.name.Equals("Butterfly (3)"))
        {
            sequence.Add(new int[4] { array[0], array[1], array[2], array[3] });
            array[0] = array[1] = array[2] = array[3] = -1;
            if(sequence.Count < Difficulty + 1) StartCoroutine(GoButterfly(2f));
            // All butterfly done
            else
            {
                btnNext.GetComponent<ButtonNext>().SetInteractable(true);
                btnDifficulty.GetComponent<Button>().interactable = true;
                btnCheckResult.GetComponent<Button>().interactable = true;
                hasStageStarted = false;    
            }
        }
    }

    int lastValue = 0;
    int GetRandomButterAnim()
    {
        int r = 0;
        while(true)
        {
            r = Random.Range(0, 3);
            if(r != lastValue)
            {
                lastValue = r;
                return lastValue;
            }
        }
    }

    public void SetDiffculty(int diff)
    {
        Difficulty = diff;
        sequence.Clear();
        for (int i = 0; i < butterfly.Length; i++)
        {
            butterfly[i].transform.position = butterStartPos;
        }
    }

    public int GetSequenceCount()
    {
        return sequence.Count;
    }

    public void ResetAnsButterPosition()
    {
        iTween.MoveTo(ans, hidePos.position, 1f);
    }
}
