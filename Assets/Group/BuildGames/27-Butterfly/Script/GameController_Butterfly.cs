﻿using UnityEngine.UI;
using UnityEngine;

public class GameController_Butterfly : GameController
{
    public ButterflyPlay gameScript;
    public GameObject flightOrderPanel;
    public Button btnFlight1, btnFlight2, btnFlight3, btnFlight4;
    public GameObject answerPanel;

    void Start()
    {
        SetGame();
    }

    public override void SetGame()
    {
        base.SetGame();

        // 결과확인할때 비행 순서 선택용 버튼
        btnFlight1.GetComponent<ButtonEventDelegator>().AddListener(ShowFlightResult);
        btnFlight2.GetComponent<ButtonEventDelegator>().AddListener(ShowFlightResult);
        btnFlight3.GetComponent<ButtonEventDelegator>().AddListener(ShowFlightResult);
        btnFlight4.GetComponent<ButtonEventDelegator>().AddListener(ShowFlightResult);

        GameManager.Instance.OnClientGameReady();
    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    public override void StartGame()
    {
        hasGameStarted = true;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register task that have to be excuted before the game start
    }

    protected override void Next()
    {
        isCheckingResult = false;
        hasStageStarted = true;
        answerPanel.SetActive(false);
    }

    protected override void Verify()
    {
        
    }

    protected override void SetDifficulty(int difficulty)
    {
        Debug.Log("Set difficulty : " + difficulty);
        this.difficulty = difficulty;
        gameScript.Difficulty = difficulty;
    }

    public void ShowFlightResult(string order)
    {
        Debug.Log("ShowFlightResult : " + order);
        gameScript.FlightOrder = ParseUtil.ParseStringToInt(order, 0);

        flightOrderPanel.GetComponent<FlighSequencePanel>().HideBtns(); ;
        flightOrderPanel.SetActive(false);
        answerPanel.SetActive(true);

        gameScript.Verify(ParseUtil.ParseStringToInt(order, 1));
    }

    protected override void CheckResultClicked()
    {
        gameScript.ResetAnsButterPosition();

        flightOrderPanel.SetActive(true);
        answerPanel.SetActive(false);

        flightOrderPanel.GetComponent<FlighSequencePanel>()
            .ShowBtns(gameScript.GetSequenceCount());

        isCheckingResult = true;
        hasStageStarted = false;

        //btnNext.GetComponent<ButtonNext>().SetInteractable(true);
        //btnCheckResult.GetComponent<Button>().interactable = false;
    }

    protected override void NextBtnClicked()
    {
        if (hasStageStarted) return;

        Next();

        //difficultyBtn.SetActive(false);
        isCheckingResult = false;
        hasStageStarted = true;

        btnNext.GetComponent<ButtonNext>().SetInteractable(false);
        //btnCheckResult.GetComponent<Button>().interactable = true;
    }
}
