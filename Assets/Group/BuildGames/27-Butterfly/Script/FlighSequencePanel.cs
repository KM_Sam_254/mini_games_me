﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlighSequencePanel : MonoBehaviour {

    public GameObject[] btns;

	public void ShowBtns(int btnCount)
    {
        Debug.Log("ShowCheckBtns : " + btnCount);
        for (int i = 0; i < btns.Length; i++)
        {
            btns[i].SetActive(false);
        }

        for (int i = 0; i < btnCount; i++)
        {
            btns[i].SetActive(true);
        }
    }	

    public void HideBtns()
    {
        for (int i = 0; i < btns.Length; i++)
        {
            btns[i].SetActive(false);
        }
    }
}
