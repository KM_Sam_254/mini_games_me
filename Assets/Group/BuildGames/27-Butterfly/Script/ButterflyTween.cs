﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButterflyTween : MonoBehaviour
{
    public ButterflyPlay gameScript;
	
    public void Fly(Vector3[] path)
    {
        iTween.MoveTo(gameObject, iTween.Hash(
                "path", path,
                "time", 3.5f,
                "easetype", iTween.EaseType.linear,
                "oncomplete", "OnFlightEnd",
                "oncompletetarget", gameObject));
    }

    void OnFlightEnd()
    {
        gameScript.OnButterMoveEnd(gameObject);
    }
}
