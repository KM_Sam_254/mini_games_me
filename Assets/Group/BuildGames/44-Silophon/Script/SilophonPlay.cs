﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SilophonPlay : MonoBehaviour {

	public int startNote;
	public int endNote;
	public int qusNoteCount;
	public List<int> qusMelo;
	public List<int> ansMelo;

	public SiloKey[] siloKey;
	public GameObject[] stick;
	public AudioClip[] sound;
	public Image[] ansImg;
	public bool isVerify;
	public AudioSource audiosource;
	public GameObject next;
	public GameObject verify;
	public GameObject dropdown;

	private string[] note_str = 
		new string[8]{"Do", "Re", "Mi", "Fa", "So", "La", "Si", "Do"};
	private int right;

	public GameController controller;
	int difficulty = 1;
	public int Difficulty { get { return difficulty; } set { difficulty = value; } }

	public void Next () 
	{
		next.SetActive (false);
		dropdown.SetActive (false);
		verify.SetActive (false);
		DisableKey ();
		qusMelo.Clear ();
		ansMelo.Clear ();
		isVerify = false;
		HideAnsNote ();
		StartCoroutine (QuestionMelody ());
	}

	public void Verify () 
	{
		verify.SetActive (false);
		StartCoroutine (AnswerMelody ());
	}

	void DisableKey()
	{
		for (int i = 0; i < siloKey.Length; i++) {
			siloKey [i].GetComponent<Button> ().interactable = false;
		}
	}

	IEnumerator QuestionMelody()
	{		
		yield return new WaitForSeconds (2f);
		for (int i = startNote; i < startNote + qusNoteCount; i++) {
			int rnd = Random.Range (startNote, endNote + 1);
			audiosource.clip = sound [rnd];
			audiosource.Play ();
			qusMelo.Add (rnd);
			float wait = Random.Range (0.5f, 1f);
			yield return new WaitForSeconds (wait);
		}
		for (int i = startNote; i <= endNote; i++) {
			siloKey [i].GetComponent<Button> ().interactable = true;
		}
		verify.SetActive (true);
	}

	IEnumerator AnswerMelody()
	{
		isVerify = true;
		right = 0;
		for (int i = 0; i < qusMelo.Count; i++) {
			siloKey [qusMelo [i]].TouchKey ();
			ansImg [i].gameObject.SetActive (true);
			iTween.MoveFrom (ansImg [i].gameObject, iTween.Hash (
				"x", 3000f, "time", 0.5f));			
			ansImg [i].color = Color.red;
			ansImg[i].GetComponentInChildren<Text>().text = 
				note_str [qusMelo [i]];
			if (i < ansMelo.Count && qusMelo[i] == ansMelo[i]) {				
				ansImg [i].color = Color.green;
				right++;
			}
			yield return new WaitForSeconds (0.5f);
		}
		next.SetActive (true);
		dropdown.SetActive (true);
	}

	void HideAnsNote()
	{
		for (int i = 0; i < ansImg.Length; i++) {
			ansImg [i].GetComponentInChildren<Text> ().text = "??";
			ansImg [i].color = Color.white;
			ansImg [i].gameObject.SetActive (false);
		}
	}


}
