﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SiloKey : MonoBehaviour {

	private GameObject stickOne;
	private GameObject stickTwo;
	private GameObject stickObj1;
	private GameObject stickObj2;
	private GameObject tempObj;
	private GameObject tempRot;
	private ParticleSystem[] tempParticle;
	private ParticleSystem particle;
	private SilophonPlay siloPlay;
	private float zstick_one;
	private float zstick_two;
	public bool isLeft;

	void Start()
	{
		tempParticle = new ParticleSystem[2];
		this.GetComponent<Button> ().onClick.AddListener(TouchKey);
		tempParticle[0] = GameObject.Find ("Particle (1)")
			.GetComponent<ParticleSystem>();
		tempParticle[1] = GameObject.Find ("Particle (2)")
			.GetComponent<ParticleSystem>();
		stickOne = GameObject.Find ("stick (1)");
		stickTwo = GameObject.Find ("stick (2)");
		stickObj1 = GameObject.Find ("StickPos (1)");
		stickObj2 = GameObject.Find ("StickPos (2)");
		siloPlay = GameObject.Find ("Gameplay").GetComponent<SilophonPlay> ();
	}

	public void TouchKey()
	{
		if (isLeft) {
			zstick_one = 20f;
			zstick_two = -20f; 
			tempObj = stickObj1;
			tempRot = stickOne;
			particle = tempParticle [0];
		} else {
			zstick_one = -20f;
			zstick_two = 20f; 
			tempObj = stickObj2;
			tempRot = stickTwo;
			particle = tempParticle [1];
		}
		MoveAndHitNote ();
		int no;
		int.TryParse (this.gameObject.name, out no);
		siloPlay.audiosource.clip = siloPlay.sound [no];
		siloPlay.audiosource.Play ();
		if (siloPlay.ansMelo.Count < siloPlay.qusMelo.Count 
			&& siloPlay.isVerify == false) {
			siloPlay.ansMelo.Add (no);
		}
	}

	void MoveAndHitNote()
	{
		iTween.MoveTo (tempObj, iTween.Hash (
			"position", this.GetComponent<Transform>().position, "easeType", "linear", 
			"time", 0.05f, "oncomplete", "ResetKey",
			"oncompletetarget", this.gameObject
		));

		iTween.RotateTo (tempRot, iTween.Hash (
			"z", zstick_one, "easeType", "linear", 
			"time", 0.05f, "oncomplete", "ResetStick",
			"oncompletetarget", this.gameObject
		));
	}

	void ResetStick()
	{		
		iTween.RotateTo (tempRot, iTween.Hash (
			"z", zstick_two, "easeType", "linear", 
			"time", 0.025f
		));

		iTween.ScaleTo (this.gameObject, iTween.Hash (
			"x", 0.9f, "y", 0.9f, "easeType", "linear", 
			"time", 0.05f, "oncomplete", "ResetKey",
			"oncompletetarget", this.gameObject
		));
	}

	void ResetKey()
	{
		particle.Emit (5);
		iTween.ScaleTo (this.gameObject, iTween.Hash (
			"x", 1f, "y", 1f, "easeType", "linear", 
			"time", 0.025f
		)); 
	}
}
