﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_Silophon : GameController {

	public SilophonPlay play;

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
		difficulty = 1;
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		play.Difficulty = difficulty;
	}

	protected override void Next()
	{
		GameMode (difficulty);
		play.Next();
	}

	protected override void Verify()
	{
		play.Verify();
	}

	void GameMode(int mode)
	{
		switch (mode) {
		case 1:
			play.startNote = 0;
			play.endNote = 2;
			play.qusNoteCount = 2;
			break;
		case 2:
			play.startNote = 0;
			play.endNote = 2;
			play.qusNoteCount = 3;
			break;
		case 3:
			play.startNote = 0;
			play.endNote = 3;
			play.qusNoteCount = 2;
			break;
		case 4:
			play.startNote = 0;
			play.endNote = 3;
			play.qusNoteCount = 3;
			break;
		case 5:
			play.startNote = 0;
			play.endNote = 4;
			play.qusNoteCount = 3;
			break;
		case 6:
			play.startNote = 0;
			play.endNote = 5;
			play.qusNoteCount = 3;
			break;
		case 7:
			play.startNote = 0;
			play.endNote = 6;
			play.qusNoteCount = 4;
			break;
		case 8:
			play.startNote = 0;
			play.endNote = 7;
			play.qusNoteCount = 5;
			break;
		case 9:
			play.startNote = 0;
			play.endNote = 7;
			play.qusNoteCount = 6;
			break;
		case 10:
			play.startNote = 0;
			play.endNote = 7;
			play.qusNoteCount = 7;
			break;
		}
	}
}
