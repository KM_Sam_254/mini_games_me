﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Truck : MonoBehaviour {

	public List<GameObject> trucks;
	public Transform[] truckPos;
	public int maxTruck;

	void AddTruck()
	{
		GameObject t = this.gameObject;
		for (int i = 0; i < t.transform.childCount; i++) {
			trucks.Add (t.transform.GetChild(i).gameObject);
		}
	}

	public void DriveTruck()
	{
		trucks.Clear ();
		AddTruck ();
		StartCoroutine (Drive ());
	}

	IEnumerator Drive()
	{
		for (int i = 0; i < maxTruck; i++) {
			iTween.MoveFrom(trucks[i], iTween.Hash(
				"position", truckPos[i].position,
                "easeType", "linear", "time", 3f
			));
			yield return new WaitForSeconds (Random.Range (0.5f, 1.5f));
		}
	}
}
