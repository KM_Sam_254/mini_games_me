﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TruckPlay : MonoBehaviour {
	 
	public Truck truckNo;
	public GameObject ansTruck;
    public Sprite[] sp;
    public List<int> sequence;
	private int no;
	private bool isCheck = false;

    int difficulty = 0;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    public GameController_Truck gameController;
    
    public void Next()
    {
        gameController.SetNextInteractable(false);
        gameController.SetVerifyInteractable(true);
		//ansTruck.SetActive (false);
		no = Difficulty + 5;
		AnsBoxTruck();
		TruckBoxColor ();	 
    }

    public void Verify()
    {       
        gameController.SetNextInteractable(true);
        gameController.SetVerifyInteractable(false);
        isCheck = true;
		AnsBoxTruck ();
    }

	void TruckBoxColor()
	{
		for (int i = 0; i < no; i++) {	
			int rnd = Random.Range (0, sp.Length);
			truckNo.transform.GetChild (i)
				.GetComponent<Image> ().sprite = sp [rnd];
			sequence.Add (rnd);
		}
		truckNo.maxTruck = no;
		truckNo.DriveTruck ();
	}

	void AnsBoxTruck()
	{
		//ansTruck.SetActive (true);
		if (isCheck) {
			for (int i = 0; i < no; i++) {
				ansTruck.transform.GetChild (i)
					.GetComponent<Image> ().enabled = true;	
				ansTruck.transform.GetChild (i)
					.GetComponent<Image> ().sprite = sp[sequence [i]];
			}
			isCheck = false;
		} else {
			for (int i = 0; i < ansTruck.transform.childCount; i++) {
				ansTruck.transform.GetChild (i)
					.GetComponent<Image> ().enabled = false;			
			}
			sequence.Clear ();
		}
	}
    
}
