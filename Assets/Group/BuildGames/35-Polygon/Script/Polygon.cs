﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Polygon : MonoBehaviour {

	public int polyCount;
	public int hideCount;
	public Text[] polyText;

	void Start()
	{
		polyText = new Text[polyCount];
		for (int i = 0; i < polyCount; i++) {
			polyText [i] = this.transform.GetChild (i)
				.gameObject.GetComponentInChildren<Text> ();
		}
	}

	public void HideRandomNumber()
	{	
		for (int i = 0; i < hideCount; i++) {
			int r = Random.Range (0, polyCount);
			polyText [(i + r) % polyCount].text = "?";
		}
	}
}
