﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PolyPlay : MonoBehaviour {

	public GameObject[] poly;
    Vector3[] polyOriginPos;
	public Transform[] polyPos;
	//public QuestionHandler qus;
	public string[] penta;
	public string[] hexa;
	public string[] hepta;
	private int index = 0;
	private int count = 0;
	private int hide = 0;
	private bool isVerify = false;
	private List<string[]> question;

    int difficulty = 0;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    public GameController_Polygon gameController;
    
    void Start()
	{
		question = new List<string[]> ();
		question.Add (penta);
		question.Add (hexa);
		question.Add (hepta);

        polyOriginPos = new Vector3[poly.Length];
        for (int i = 0; i < poly.Length; i++)
        {
            polyOriginPos[i] = poly[i].transform.position;
        }
	}

	void AssignNo()
	{        
		//qus.Split (question[Difficulty], index);
        int[] q = StringUtil.SplitStringToIntArray(question[Difficulty][index], '.');
		poly [Difficulty].GetComponent<Polygon> ().polyCount = count;
		poly [Difficulty].GetComponent<Polygon> ().hideCount = hide;
       
		for (int i = 0; i < count; i++) {			
			poly [Difficulty].GetComponent<Polygon> ().polyText [i].text =
                //qus.q[i].ToString ();	
                q[i].ToString();
        }

		if (isVerify == false) {
			poly [Difficulty].GetComponent<Polygon> ()
			.HideRandomNumber ();
            StartCoroutine(RotatePoly());
		}
	}

    IEnumerator RotatePoly()
    {
        yield return new WaitForSeconds(2f);
        float rand = Random.Range(20f, 50f);
        iTween.RotateBy(poly[Difficulty].gameObject, 
            new Vector3(0f, 0f, -rand), 5f);
        for (int i = 0; i < count; i++) {    
            iTween.RotateBy(poly [Difficulty].GetComponent<Polygon> ()
                .polyText [i].gameObject, 
                new Vector3(0f, 0f, rand), 5f);
        }
        gameController.SetVerifyInteractable(true);
    }

	public void ChangeMode()
	{
        gameController.SetNextInteractable(false);
		for (int i = 0; i < poly.Length; i++) {
			iTween.MoveTo (poly [i], polyPos [i].position, 0f);
			if (Difficulty == i) {
				count = Difficulty + 5;
				hide = Random.Range (2, Difficulty + 4);
				iTween.MoveTo (poly [i], polyPos [3].position, 1f);
			}
		}
		isVerify = false;
        index = Random.Range(0, 5);
		AssignNo ();
	}

	public void Next()
	{
        gameController.SetNextInteractable(false);
		isVerify = false;
        ResetPolyPosition();
		ChangeMode ();		
	}

	public void Verify()
	{
        gameController.SetVerifyInteractable(false);
        gameController.SetNextInteractable(true);
		isVerify = true;
		AssignNo ();
	}

    void ResetPolyPosition()
    {
        for (int i = 0; i < poly.Length; i++)
        {
            poly[i].transform.position = polyOriginPos[i];
        }
    }
}
