﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RabbitPlay : MonoBehaviour {

	public Image[] rabbit;
	public Image[] noImg;
	public Image qus;
	public Sprite[] sp;
	private int[] no;
	public GameObject next;
	public GameObject verify;
	public GameObject diffPanel;
	public Color[] clr;
	private List<int> random;
	private List<int> rabbit_sprite;

	public GameController_Rabbit control;
	int difficulty = 1;
	public int Difficulty { get { return difficulty; } set { difficulty = value; } }

	void AddList()
	{		
		no = new int[difficulty + 2];
		rabbit_sprite = new List<int>();
		random = new List<int> ();
		for (int i = 0; i < rabbit.Length; i++) {
			random.Add (i);
		}
	}

	public void Next()
	{
		AddList ();
		HideRabbit ();
		for (int i = 0; i < no.Length; i++) {
			int r= Random.Range (0, random.Count);
			int c = Random.Range (0, sp.Length);
			rabbit_sprite.Add (c);
			no [i] = random [r];
			random.RemoveAt(r);
		}
		StartCoroutine(ShowAgain ());

		next.SetActive (false);
		diffPanel.SetActive (false);
		verify.SetActive (false);
	}

	public void Verify()
	{
		ShowRabbit ();
		CheckRabbit ();

		next.SetActive (true);
		diffPanel.SetActive (true);
		verify.SetActive (false);
	}

	void CheckRabbit()
	{
		for (int i = 0; i < no.Length; i++) {
			if (rabbit[no[i]].sprite  == qus.sprite) {
				noImg [no [i]].color = clr [0];
			}
		}
	}

	IEnumerator ShowAgain()
	{
		ShowRabbit ();
		yield return new WaitForSeconds (2f);
		HideRabbit ();
		yield return new WaitForSeconds (3f);
		ShowRabbit ();
		yield return new WaitForSeconds (2f);
		HideRabbit ();
		verify.SetActive (true);
	}

	void ShowRabbit()
	{
		for (int i = 0; i < no.Length; i++) {
			rabbit [no[i]].gameObject.SetActive(true);
			rabbit[no[i]].sprite = sp[rabbit_sprite[i]];
			iTween.MoveFrom (rabbit [no[i]].gameObject, iTween.Hash(
				"y", -300f, "easeType", "easeOutElastic", "time", 1f));
			qus.sprite = sp [rabbit_sprite [i]];
		}
	}

	void HideRabbit()
	{
		for (int i = 0; i < rabbit.Length; i++) {
			rabbit [i].gameObject.SetActive (false);
		}

		for (int i = 0; i < noImg.Length; i++) {
			noImg [i].color = clr [1];
		}
	}
}
