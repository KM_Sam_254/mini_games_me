﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pig : MonoBehaviour
{
    Animator animator;
    public Text numTxt;
    public GameObject numTxtObj, shadowObj;
    Vector3[] racePoints;
    int raceIndex = 0;
    int midPoint, endPoint = 1;
    
    Vector3 raceEndPosition;
    public PigPlay pigScript;

    Vector3 originPosition;

    int labelNumber = 0;
    public int LabelNumber { get { return labelNumber; } set { labelNumber = value; } }

    void Start()
    {
        animator = GetComponent<Animator>();
        if (animator == null) Debug.LogError("Pig animator is NULL");
        originPosition = gameObject.transform.position;
    }

    public void Anim_Run()
    {
        animator.SetBool("run", true);
    }

    public void Anim_Stop()
    {
        animator.SetBool("run", false);
    }

    public void SetNumberText(int num)
    {
        if (num < 0)
        {
            numTxt.text = "";
            LabelNumber = 0;
        }
        else
        {
            numTxt.text = "" + num;
            LabelNumber = num;
        }
    }

    public void Race(float time)
    {
        if (raceIndex > 1) raceIndex = 1;
        iTween.MoveTo(gameObject, iTween.Hash("position", racePoints[raceIndex], "time", time
            , "easetype", iTween.EaseType.linear
            , "oncomplete", "OnRaceComplete", "oncompletetarget", gameObject));
        Anim_Run();
        raceIndex++;
    }

    void OnRaceComplete()
    {
        Anim_Stop();
    }

    public void ReadyToStartLine(Vector3 pos)
    {
        iTween.MoveTo(gameObject, iTween.Hash("position", pos, "time", 2f,
            "easetype", iTween.EaseType.linear, "oncomplete", "OnPigReadyStartLine", "oncompletetarget", gameObject));
        Anim_Run();
    }

    void OnPigReadyStartLine()
    {
        //Debug.Log("On Pig Ready : " + gameObject.name);
        Anim_Stop();
        
        pigScript.OnPigRaceReady(gameObject);
    }

    public void SetRacePoint(int midPoint, int endPoint, Vector3 raceEndPosition)
    {
        this.midPoint = midPoint;
        this.endPoint = endPoint;
        this.raceEndPosition = raceEndPosition;

        racePoints = new Vector3[2];   // Race only 2 times
        float moveUnit = Mathf.Abs(gameObject.transform.position.x - raceEndPosition.x) / 10;

        //Debug.Log("SetRacePoint : " + gameObject.transform.position);

        // 중간 이동 위치
        float distX = gameObject.transform.position.x + (moveUnit * midPoint);
        racePoints[0] = new Vector3(distX, gameObject.transform.position.y, 0);

        // 최종 이동 위치
        distX = gameObject.transform.position.x + (moveUnit * endPoint);
        racePoints[1] = new Vector3(distX, gameObject.transform.position.y, 0);

        //Debug.Log("RacePoint1 " + gameObject.name + "  point : " + racePoints[0]);
        //Debug.Log("RacePoint2 " + gameObject.name + "  point : " + racePoints[1]);
    }

    public int GetEndPoint()
    {
        return endPoint;
    }

    public void ResetPig()
    {
        gameObject.transform.position = originPosition;
        raceIndex = 0;
    }

    public void SetVisibility(bool b)
    {
        GetComponent<Image>().enabled = b;
        numTxtObj.SetActive(b);
        shadowObj.SetActive(b);
    }
}
