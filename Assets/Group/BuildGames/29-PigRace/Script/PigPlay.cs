﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PigPlay : GameController
{
    public GameObject[] pigObjects;
    Pig[] pigs;
    public Transform[] pigStartPos;
    Vector3[] startLinePositions;
    Vector3[] pigOriginPositions;
    int[] pigNumbers;   // 돼지에 붙은 번호표
    float[] pigMovedDistance;  // 돼지가 움직인 거리
    int pigCount=3;
    float raceTime = 4f;   // 경주할때 뛰어가는 시간
    public GameObject answerPanel;
    public PigAnswerPanel answerScript;

    public Transform raceEndPosition;

    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    public override void SetGame()
    {
        base.SetGame();

        Debug.Log("SetGame");
        // Get pigs' origin positions
        pigOriginPositions = new Vector3[pigObjects.Length];
        for (int i = 0; i < pigObjects.Length; i++)
        {
            pigOriginPositions[i] = pigObjects[i].transform.position;
        }
        
        pigCount = Difficulty + 3;

        pigMovedDistance = new float[pigCount];

        GameManager.Instance.OnClientGameReady();

        ResetPigs();

        // First pig show
        Invoke("ReadyPigs", .5f);
    }

    void ResetPigs()
    {
        Debug.Log("ResetPigs");
        SetNextInteractable(false);
        pigReadyCount = 0;

        // Reset moved distance
        for (int i = 0; i < pigMovedDistance.Length; i++)
        {
            pigMovedDistance[i] = 0;
        }

        // Get pigs back where it was
        for (int i = 0; i < pigObjects.Length; i++)
        {
            pigObjects[i].transform.position = pigOriginPositions[i];
            pigObjects[i].GetComponent<Pig>().SetVisibility(true);
        }

        // Pigs
        pigs = new Pig[pigCount];
        for (int i = 0; i < pigCount; i++)
        {
            pigs[i] = pigObjects[i].GetComponent<Pig>();
        }

        // Start positions
        startLinePositions = new Vector3[pigStartPos.Length];
        for (int i = 0; i < pigStartPos.Length; i++)
        {
            startLinePositions[i] = pigStartPos[i].position;
        }

        // Pig numbers for text label
        pigNumbers = SimpleRandom.GetRandomArray(1, 10, pigCount);
        for (int i = 0; i < pigNumbers.Length; i++)
        {
            pigs[i].SetNumberText(pigNumbers[i]);
        }

        raceTime = GetRaceTime();
    }

    public override void StartGame()
    {
        hasGameStarted = true;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register task that have to be excuted before the game start
    }

    protected override void SetDifficulty(int difficulty)
    {
        difficulty -= 1;
        Debug.Log("Set difficulty : " + difficulty);
        this.difficulty = difficulty;
        pigCount = difficulty + 3;
        ResetPigs();
        Invoke("ReadyPigs", .5f);
    }

    void Start()
    {
        SetGame();
    }
    
	void ReadyPigs()
    {
        // Get pig ready to start line
        for (int i = 0; i < pigs.Length; i++)
        {
            pigs[i].ReadyToStartLine(startLinePositions[i]);
        }
    }

    int[] midPoints, endPoints;
    void SetPigsRace()
    {
        Debug.Log("=== SetPigsRacePoints");
        endPoints = SimpleRandom.GetRandomArray(5, 11, pigCount);
        midPoints = new int[pigCount];

        for (int i = 0; i < endPoints.Length; i++)
        {
            midPoints[i] = endPoints[i] / 2 + Random.Range(0, 3);

        }

        for (int i = 0; i < midPoints.Length; i++)
        {
            for (int k = 0; k < midPoints.Length; k++)
            {
                if (i == k) continue;
                if (midPoints[i] == midPoints[k]) midPoints[i] += 1;
            }
        }

        for (int i = 0; i < pigs.Length; i++)
        {
            Debug.Log("Set midPoint : " + midPoints[i] + "  endPoint : " + endPoints[i]);
            pigs[i].SetRacePoint(midPoints[i], endPoints[i], raceEndPosition.position);
        }
    }

    protected override void Verify()
    {
        Debug.Log("Verify");
        answerPanel.SetActive(true);
        SetVerifyInteractable(false);

        answerScript.ShowAnswer(endPoints, pigNumbers);

        for (int i = 0; i < pigs.Length; i++)
        {
            pigs[i].SetVisibility(true);
            pigs[i].ResetPig();
        }

        ResetPigs();
        Invoke("ReadyPigs", 1f);
    }

    void RunPigs()
    {
        for (int i = 0; i < pigCount; i++)
        {
            pigs[i].Race(raceTime);
        }
    }

    void ShowPigs()
    {
        for (int i = 0; i < pigs.Length; i++)
        {
            pigs[i].SetVisibility(true);
        }
    }

    void HidePigs()
    {
        for (int i = 0; i < pigs.Length; i++)
        {
            pigs[i].SetVisibility(false);
        }
    }

    protected override void Next()
    {
        SetNextInteractable(false);
        SetVerifyInteractable(false);
        SetDiffcultyBtnInteractable(false);
        answerPanel.SetActive(false);
        StartCoroutine(Race());
    }

    IEnumerator Race()
    {
        RunPigs();
        yield return new WaitForSeconds(raceTime);
        HidePigs();
        yield return new WaitForSeconds(2f);
        ShowPigs();
        raceTime = GetRaceTime();
        RunPigs();
        yield return new WaitForSeconds(raceTime);
        HidePigs();
        SetVerifyInteractable(true);
        SetDiffcultyBtnInteractable(true);
    }

    float GetRaceTime()
    {
        return Random.Range(2, 5);
    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    int pigReadyCount = 0;
    public void OnPigRaceReady(GameObject pig)
    {
        //Debug.Log("OnPigRaceReady : " + pig.name);
        pigReadyCount++;
        if (pigReadyCount >= pigCount)
        {
            SetPigsRace();
            SetNextInteractable(true);
        }
    }

    protected override void NextBtnClicked()
    {
        Next();
    }

    protected override void CheckResultClicked()
    {
        Verify();
    }

    public void Test()
    {
        Debug.Log("=== SetPigsRacePoints");

        endPoints = SimpleRandom.GetRandomArray(5, 11, pigCount);
        midPoints = new int[pigCount];
        
        for (int i = 0; i < endPoints.Length; i++)
        {
            midPoints[i] = endPoints[i] / 2 + Random.Range(0, 3);
            
        }

        for (int i = 0; i < midPoints.Length; i++)
        {
            for (int k = 0; k < midPoints.Length; k++)
            {
                if (i == k) continue;
                if (midPoints[i] == midPoints[k]) midPoints[i] += 1;
            }
        }

        for (int i = 0; i < endPoints.Length; i++)
        {
            Debug.Log(" midPoint : " + midPoints[i] + "  endPoint : " + endPoints[i]);
        }
        
    }
}
