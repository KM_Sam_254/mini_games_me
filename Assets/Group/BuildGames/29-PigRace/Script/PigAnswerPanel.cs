﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PigAnswerPanel : MonoBehaviour
{
    public GameObject[] pigResults;
    public Text[] pigTexts;

	public void ShowAnswer(int[] pigEndpoints, int[] pigLabel)
    {
        SetLabels(pigEndpoints.Length);

        int[] endPoints = new int[pigEndpoints.Length];
        for (int i = 0; i < pigEndpoints.Length; i++)
        {
            endPoints[i] = pigEndpoints[i];
        }

        int[] label = new int[pigLabel.Length];
        for (int i = 0; i < pigLabel.Length; i++)
        {
            label[i] = pigLabel[i];
        }

        // BubbleSort labels by endPoints
        int[] lableArray = new int[pigEndpoints.Length];
        for (int i = 0; i < endPoints.Length; i++)
        {
            for (int k = 0; k < endPoints.Length - 1; k++)
            {
                if (endPoints[k] > endPoints[k + 1])
                {
                    // array
                    int temp = endPoints[k];
                    endPoints[k] = endPoints[k + 1];
                    endPoints[k + 1] = temp;

                    // label
                    temp = label[k];
                    label[k] = label[k + 1];
                    label[k + 1] = temp;
                }
            }
        }

        for (int i = 0; i < label.Length; i++)
        {
            pigTexts[i].text = "" + label[i];
        }
    }

    public void SetLabels(int resultCount)
    {
        for (int i = 0; i < pigResults.Length; i++)
        {
            pigResults[i].SetActive(false);
        }

        for (int i = 0; i < resultCount; i++)
        {
            pigResults[i].SetActive(true);
        }
    }
}
