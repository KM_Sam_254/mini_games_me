﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapleLeafPlay : MonoBehaviour {

	public Button next;
	public Button verify;
	public Dropdown dropdown;
	public MapleLeaf[] mapleLeaf;
	public static int total;
	public static int touchCount;
	public GameObject verifyPanel;
	public Text verifyText;
	public Text totalText;
	private List<int> no;

	public void Next () 
	{
		touchCount = 0;
		total = 0;
		Text touchCountText = GameObject.Find ("TextTouchCount")
			.GetComponent<Text> ();
		touchCountText.text = MapleLeafPlay.touchCount.ToString ();
		StartCoroutine (GamePlay ());
		next.interactable = false;
		dropdown.interactable = false;
		verifyPanel.SetActive (false);
	}
	
	public void Verify () 
	{
		verifyText.text = touchCount.ToString ();
		totalText.text = total.ToString ();
		next.interactable = true;
		dropdown.interactable = true;
		verifyPanel.SetActive (true);
	}

	IEnumerator GamePlay()
	{
		int leafNo = Random.Range (5, 10);
		for (int i = 0; i < leafNo + (dropdown.value * 10); i++) {
			if (no == null || no.Count < 1) {
				AddList ();
			}
			int rnd = Random.Range (0, no.Count);
			mapleLeaf [no [rnd]].gameObject.SetActive (true);
			mapleLeaf [no [rnd]].LeafFall ();
			no.RemoveAt (rnd);
			yield return new WaitForSeconds(4 - dropdown.value);
		}
		verify.interactable = true;
	}

	void AddList()
	{
		no = new List<int>
		{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
	}
}
