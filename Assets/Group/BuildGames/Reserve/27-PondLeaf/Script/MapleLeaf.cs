﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapleLeaf : MonoBehaviour {


	public void LeafFall()
	{
		MapleLeafPlay.total++;
		this.gameObject.transform.SetAsLastSibling ();
		float x = Random.Range (0f, 2000f);
		float y = Random.Range (0f, 1000f);
		float z = Random.Range (-270f, 270f);

		iTween.ScaleFrom (this.gameObject, iTween.Hash (
			"x", 3f, "y", 3f, "easeType", "linear", 
			"time", 3f
		));

		iTween.MoveFrom(this.gameObject, iTween.Hash (
			"x", x, "y", y,
			"easeType", "linear", 
			"time", 3f
		));
		iTween.RotateAdd(this.gameObject, iTween.Hash (
			"z", z, 
			"easeType", "linear", 
			"time", 3f
		));
		StartCoroutine (HideLeaf ());
	}

	IEnumerator HideLeaf()
	{
		yield return new WaitForSeconds (3f);
		this.GetComponent<Image> ().raycastTarget = true;
		yield return new WaitForSeconds (1f);
		this.GetComponent<Image> ().raycastTarget = false;
		this.gameObject.SetActive (false);
	}

	public void TouchLeaf()
	{
		MapleLeafPlay.touchCount++;
		Text touchCountText = GameObject.Find ("TextTouchCount")
			.GetComponent<Text> ();
		touchCountText.text = MapleLeafPlay.touchCount.ToString ();

		iTween.PunchScale(this.gameObject, iTween.Hash (
			"x", 1f, "y", 1f,
			"easeType", "linear", 
			"time", 0.5f
		));
	}
}
