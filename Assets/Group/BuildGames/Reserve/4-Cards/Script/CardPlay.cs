﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardPlay : MonoBehaviour {

	public RuntimeAnimatorController modeName;
	public RuntimeAnimatorController modeReset;
	private Image[] card;
    public Sprite[] sp;
    private Image question;
    private List<int> no;
    private List<int> show;

	void Start()
	{
		card = new Image[this.gameObject.transform.childCount];
        for (int i = 0; i < card.Length; i++)
        {
            card[i] = this.gameObject.transform.GetChild(i).GetComponent<Image>();
        }
        show = new List<int>();
	}

    void RandomList()
    {
        no = new List<int>(){ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        show.Clear();
    }

    public void AssignCard()
    {
        RandomList();
        question = GameObject.Find("Question").GetComponent<Image>();
        for (int i = 0; i < card.Length; i++) {
            int r = Random.Range(no[0], no.Count - 2);
            card [i].sprite = sp[no[r]];
            show.Add(no[r]);
            no.RemoveAt(r);
            question.sprite = card[Random.Range(0, card.Length)].sprite;
        }
        StartCoroutine(CoverNshuffle());
    }

	IEnumerator CoverNshuffle()
	{
		yield return new WaitForSeconds (2f);
        for (int i = 0; i < card.Length; i++) {
            card [i].sprite = sp[10];
		}
		this.GetComponent<Animator> ().enabled = true;
		this.GetComponent<Animator> ().runtimeAnimatorController = modeName;
		this.GetComponent<Animator> ().Play ("");
	}

	public void ResetAnim()
	{
		this.GetComponent<Animator> ().runtimeAnimatorController = modeReset;
		this.GetComponent<Animator> ().Play ("");
	}

	public void EndAnim()
	{
		this.GetComponent<Animator> ().StopPlayback();
		this.GetComponent<Animator> ().enabled = false;
	}

	public void ShowCard()
	{
        for (int i = 0; i < card.Length; i++) {
            card [i].sprite = sp[show[i]];
        }
	}

}
