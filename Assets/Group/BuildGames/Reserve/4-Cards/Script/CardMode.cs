﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardMode : MonoBehaviour {

	public GameObject[] cardMode;
	public Image qusCard;
	public Transform[] pos;
	public List<int> no;

    int difficulty = 0;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    void AddList()
	{
		no.Clear ();
		for (int i = 0; i < 10; i++) {
			no.Add (i);
		}
	}

	public void ChangeMode()
	{
		for (int i = 0; i < 4; i++) {
			iTween.MoveTo (cardMode [i], pos [i].position, 0f);
			if (Difficulty == i) {
				iTween.MoveTo (cardMode [i], pos [4].position, 1f);
			}
		}
	}

	public void Next()
	{
        ChangeMode();
		cardMode [Difficulty].GetComponent<CardPlay> ().ResetAnim ();
		cardMode [Difficulty].GetComponent<CardPlay> ().AssignCard ();

		iTween.PunchScale (cardMode [Difficulty], 
			                new Vector3 (0.5f, 0.5f, 0f),
                            1f);

		cardMode [Difficulty].transform.localScale = new Vector2 (1f, 1f);
	}

	public void Verify()
	{
		cardMode [Difficulty].GetComponent<CardPlay> ().ShowCard ();
		cardMode [Difficulty].transform.localScale = 
			new Vector2 (1.2f, 1.2f);
	}
}
