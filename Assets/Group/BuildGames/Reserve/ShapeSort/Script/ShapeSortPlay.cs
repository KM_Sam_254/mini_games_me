﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShapeSortPlay : MonoBehaviour {

	public Image[] shapeQus;
	public Image[] shapeAns;
	public Transform[] qusPos;
	public Sprite[] sprt;
	public Image timer;
	public GameObject panel;
	public Image percentage;
	public Text totalTxt;
	public Text correctTxt;
	public Text percentTxt;

	private List<int> temp_ans;
	private List<int> temp_qus;
	private int mode;
	private List<int> no;
	private List<Color> clr;
	private Image tempImg;
	private bool isFill;
	private bool isVerify;
	private int total;
	private int correct;

	public GameController_ShapeSort controller;
	int difficulty = 1;
	public int Difficulty { get { return difficulty; } set { difficulty = value; } }

	public void Next()
	{
		isVerify = false;
		mode = difficulty + 1;
		panel.SetActive (false);
		AddList ();
		ChangeQuestion ();
	}

	public void Verify()
	{
		isVerify = true;
		panel.SetActive (true);
		totalTxt.text = "Total : " + total.ToString ();
		correctTxt.text = "Correct : " + correct.ToString ();
		percentage.fillAmount = (float)correct / (float)total;
		percentTxt.text = percentage.fillAmount.ToString ("P0");
		isFill = false;
		timer.fillAmount = 1f;
		total = 0;
		correct = 0;
	}

	void Update()
	{
		if (isFill && isVerify == false) {
			timer.fillAmount -= Time.deltaTime / 3f;
			if (timer.fillAmount == 0f) {
				ChangeQuestion ();
				timer.fillAmount = 1f;
			}
		}
	}

	void AddList()
	{
		temp_qus = new List<int> ();
		no = new List<int> ();
		clr = new List<Color> ();
		for (int i = 0; i < 100; i++) {
			int rnd = Random.Range (0, sprt.Length);
			no.Add (rnd);
			Color c = new Color (Random.value, Random.value, Random.value);
			clr.Add (c);
		}
	}

	void HideAnswer()
	{
		for (int i = 0; i < shapeAns.Length; i++) {
			shapeAns [i].transform.parent.gameObject.SetActive (false);
		}
	}

	void ChangeQuestion()
	{
		if (isVerify == false) {
			for (int i = 0; i < shapeQus.Length; i++) {
				if (no.Count < 6) {
					AddList ();
				}
				shapeQus [i].sprite = sprt [no [i]];
				shapeQus [i].color = clr [i];
				iTween.MoveTo (shapeQus [0].gameObject, qusPos[0].position, 0f);
				iTween.ScaleTo (shapeQus [0].gameObject, new Vector3(1f, 1f, 0f), 0.5f);
				if (i < shapeQus.Length - 1 && i > 0) {
					iTween.MoveFrom (shapeQus [i].transform.parent.gameObject, qusPos [i + 1].position, 0.5f);
					iTween.ScaleFrom (shapeQus [i].transform.parent.gameObject, 
						qusPos [i + 1].transform.localScale, 0.5f);
				}
			}
			shapeQus [0].transform.parent.GetComponent<Image>().color
			= Color.white;

			HideAnswer ();
			int rnd = Random.Range (0, mode);
			for (int i = 0; i < mode; i++) {	
				shapeAns [i].transform.parent.gameObject.SetActive (true);
				shapeAns [i].sprite = shapeQus [(i + rnd) % mode].sprite;
				shapeAns [i].color = shapeQus [(i + rnd) % mode].color;
				shapeAns [i].raycastTarget = true;
				shapeAns [i].transform.parent.GetComponent<Image> ().color =
					Color.white;
			}
			isFill = true;
			no.RemoveAt (0);
			clr.RemoveAt (0);
			total++;
		}
	}

	public void TouchAnswer(Image img)
	{
		isFill = false;
		for (int i = 0; i < shapeAns.Length; i++) {
			shapeAns [i].raycastTarget = false;
		}
		if (shapeQus[0].sprite == img.sprite &&
			shapeQus[0].color == img.color) 
		{
			StartCoroutine (Place (true));
		} else {
			StartCoroutine (Place (false));
		}
		tempImg = img.transform.parent.GetComponent<Image> ();
		iTween.MoveTo (shapeQus [0].gameObject, 
			img.transform.position, 0.5f);
	}

	IEnumerator Place(bool right)
	{
		yield return new WaitForSeconds (0.6f);
		iTween.ScaleTo (shapeQus [0].gameObject, 
			new Vector3(0.7f, 0.7f, 0f), 0.5f);
		yield return new WaitForSeconds (0.6f);
		if (right) {
			tempImg.color = Color.green;
			shapeQus [0].transform.parent.GetComponent<Image>().color
			= Color.green;
			correct++;
		} else {
			tempImg.color = Color.red;
			shapeQus [0].transform.parent.GetComponent<Image>().color
			= Color.red;
		}
		yield return new WaitForSeconds (1f);
		ChangeQuestion ();
		timer.fillAmount = 1f;
		isFill = true;
	}
}
