﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SushiPlay : MonoBehaviour {

	public Button next;
	public Button verify;
	public Dropdown dropdown;
	public GameObject[] sushi;
	public Sprite[] sushiSp;
	private List<int> no;
	public List<int> ans;
	
	public void Next () 
	{
		next.interactable = false;
		dropdown.interactable = false;
		StartCoroutine (MoveSushi ());		
	}
	
	public void Verify () 
	{
		verify.interactable = false;
		StartCoroutine (VerifySushi ());
	}

	void AddList()
	{
		no = new List<int> (){ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	}

	IEnumerator MoveSushi()
	{
		AddList ();
		ans.Clear ();
		for (int i = 0; i < 4 * dropdown.value + 4; i++) {
			int rnd = Random.Range (0, no.Count);
			sushi [no [rnd]].GetComponent<Image> ().sprite = sushiSp [no [rnd]];
			iTween.MoveFrom (sushi [no [rnd]], iTween.Hash
				("x", 2500f, "easeType", "linear", "time",7f));
			ans.Add (no [rnd]);
			no.RemoveAt (rnd);
			yield return new WaitForSeconds (1f);
		}
		yield return new WaitForSeconds (5f);
		verify.interactable = true;
	}

	IEnumerator VerifySushi()
	{
		for (int i = 0; i < ans.Count; i++) {
			sushi [ans[i]].GetComponent<Image> ().sprite = sushiSp [ans[i]];
			iTween.MoveFrom (sushi [ans[i]], iTween.Hash
				("x", 2500f, "easeType", "linear", "time",7f));
			yield return new WaitForSeconds (1f);
		}
		yield return new WaitForSeconds (5f);
		next.interactable = true;
		dropdown.interactable = true;
	}
}
