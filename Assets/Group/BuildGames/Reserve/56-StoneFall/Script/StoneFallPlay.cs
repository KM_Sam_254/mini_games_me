﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoneFallPlay : MonoBehaviour {

	public Rigidbody2D stoneBall;
	public GameObject mountainRest;
	public GameObject mountainThrow;
	public Transform startPos;
	public GameObject verifyPanel;
	public LowerBoundary[] boundary;
	public bool isNext = false;
	public int totalRestCount;
	public Text totalRestTxt;
	private int index;

	public void Next()
	{
		stoneBall.velocity = Vector2.zero;
		stoneBall.angularVelocity = 0f;
		stoneBall.simulated = true;
		stoneBall.transform.position = startPos.position;
		verifyPanel.SetActive (false);
		if (index >= 10) {
			Verify ();
		} else {
			MoveMountain ();
		}
		index++;
	}

	public void Verify()
	{
		verifyPanel.SetActive (true);
		totalRestTxt.text = "You put the rock on the mountain <color=orange><b>"
			+ totalRestCount.ToString () + "</b></color> time out of <color=orange>"
		+ index.ToString () + "</color>.";
		index = 0;
	}

	public void MoveMountain()
	{
		float x = Random.Range (200f, 1000f);
		float y = Random.Range (0f, 300f);
		float y2 = Random.Range (0f, 300f);
		iTween.MoveTo (mountainRest, iTween.Hash ("x", x, "y", y, "time", 1f));
		iTween.MoveTo (mountainThrow, iTween.Hash ("y", y2, "time", 1f));
		boundary [0].isHit = true;
		boundary [1].isHit = true;
	}
}
