﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoneThrower : MonoBehaviour {

	public float speed = 100f;
	public Rigidbody2D rb;
	public GameObject panel;
	private float timer;
	private float start;
	private float end;
	public Image fill;
	private float force;
	private bool isFilling;

	void Update()
	{
		if (isFilling) {
			fill.fillAmount += Time.deltaTime / 4.5f;
		}
	}

	public void StartDraw()
	{
		panel.SetActive (false);
		start = Time.time;
		isFilling = true;
	}

	public void AddForce()
	{
		end = Time.time;
		float f = end - start;
		if (f > 1f && f <= 4f) {
			force = (f / 4f) * 100f;
			rb.AddForce (new Vector2 (-force * 10, force * 20), ForceMode2D.Impulse);
			iTween.RotateTo (this.gameObject, new Vector3 (0f, 0f, force / 5f), 1f);
		} else if (f > 4f) {
			force = 100f;
			rb.AddForce (new Vector2 (-force  * 10, force * 20), ForceMode2D.Impulse);
			iTween.RotateTo (this.gameObject, new Vector3 (0f, 0f, force / 5f), 1f);
		}
		isFilling = false;
		StartCoroutine (ResetRotation ());
	}

	IEnumerator ResetRotation()
	{
		yield return new WaitForSeconds (1.1f);
		fill.fillAmount = 0f;
		iTween.RotateTo (this.gameObject, new Vector3 (0f, 0f, -25f), 1f);
	}
}
