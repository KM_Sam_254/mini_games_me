﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowerBoundary : MonoBehaviour {

	public bool isHit = true;
	public bool isBoundary;
	public StoneFallPlay play;

	void Update()
	{
		if (play.isNext) {
			play.isNext = false;
			StopAllCoroutines ();
			play.Next ();
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (isHit && isBoundary == false) {
			isHit = false;
			play.isNext = false;
			StartCoroutine (Check ());
		}
		else if (isHit && isBoundary) {
			isHit = false;
			play.isNext = true;
		}
	}

	IEnumerator Check()
	{
		yield return new WaitForSeconds (2f);
		play.isNext = true;
		play.totalRestCount++;
	}		
}
