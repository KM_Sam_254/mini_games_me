﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_StoneFall : GameController {

	public StoneFallPlay play;

	public override void SetGame()
	{
		base.SetGame();

		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		Debug.Log("Pond Fish Start");
		SetGame();
		play.Next ();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
	}

	protected override void Next()
	{
	}

	protected override void Verify()
	{
	}

	public void ChangeMode(int stone)
	{
		
	}
}

	

