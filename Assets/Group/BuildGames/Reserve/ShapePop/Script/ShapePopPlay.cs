﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShapePopPlay : MonoBehaviour {

	public Image[] qus;
	public Image[] ans;
	public Sprite[] sprite;
	public Transform[] pos;
	public Image timer;
	public Image percent;
	public Image ansPanel;
	public GameObject verifyPanel;
	public Text correctTxt;
	public Text totalTxt;
	public Text percentTxt;

	private int correct;
	private int total;
	private bool isFill;
	private int inputCount;
	private int correctCount;
	private List<int> temp_qus;
	private List<int> temp_ans;
	private List<Color> clr;
	private List<int> no;

	public GameController_ShapePop gameController;
	int difficulty = 1;
	public int Difficulty { get { return difficulty; } set { difficulty = value; } }

	void Update()
	{
		if (isFill) {			
			timer.fillAmount += Time.deltaTime / difficulty;
		}
	}

	public void Next()
	{		
		verifyPanel.SetActive (false);
		gameController.SetDiffcultyBtnInteractable(false);
		AnswerAllowed (true);
		total = 0;
		correct = 0;
		AssignShape ();
	}

	public void Verify()
	{
		verifyPanel.SetActive (true);
		gameController.SetDiffcultyBtnInteractable(true);
		StopAllCoroutines ();
		AnswerAllowed (false);
		percent.fillAmount = (float)correct / (float)total;
		percentTxt.text = percent.fillAmount.ToString ("P0");
		totalTxt.text = "Total : " + total.ToString ();
		correctTxt.text = "Correct : " + correct.ToString ();
	}

	void AssignShape()
	{
		no = new List<int> (){ 0, 1, 2, 3, 4 };
		clr = new List<Color> ();
		temp_qus = new List<int> ();
		temp_ans = new List<int> ();
		ansPanel.color = Color.grey;
		inputCount = 0;
		timer.fillAmount = 0f;

		for (int i = 0; i < ans.Length; i++) {
			ans [i].raycastTarget = true;
			iTween.ScaleTo (ans [i].gameObject, new Vector3 (1f, 1f, 0f), 0.5f);
			ans [i].sprite = sprite[Random.Range (0, sprite.Length)];
			ans [i].color = new Color (Random.value, Random.value, Random.value);
			clr.Add (ans [i].color);
		}

		for (int i = 0; i < difficulty; i++) {
			qus [i].gameObject.SetActive (true);
			int rnd = Random.Range (0, no.Count);
			qus [i].sprite = ans [no[rnd]].sprite;
			qus [i].color = ans [no [rnd]].color;
			iTween.MoveFrom(qus [i].gameObject, iTween.Hash
				("position", pos[i].position,"easeType", "linear", "time", 3f));
			temp_qus.Add (no [rnd]);
			no.RemoveAt (rnd);
		}
		StartCoroutine (CheckAnswer ());
	}

	public void InputShape(int no)
	{
		ans [no].raycastTarget = false;
		iTween.ScaleTo (ans [no].gameObject, new Vector3 (1.2f, 1.2f, 0f), 0.5f);
		temp_ans.Add (no);
		inputCount++;
	}

	IEnumerator CheckAnswer()
	{
		yield return new WaitForSeconds (3f);
		isFill = true;
		yield return new WaitForSeconds (difficulty);
		isFill = false;
		ansPanel.color = Color.red;
		correctCount = 0;
		for (int i = 0; i < temp_ans.Count; i++) {
			if (inputCount == difficulty &&
				temp_ans [i] == temp_qus [i]) {
				correctCount++;
			}
		}
		if (correctCount == temp_qus.Count) {
			ansPanel.color = Color.green;
			correct++;
		}
		total++;
		yield return new WaitForSeconds (0.5f);
		AssignShape ();
	}

	void AnswerAllowed(bool isAllowed){
		for (int i = 0; i < ans.Length; i++) {
			ans [i].raycastTarget = isAllowed;
		}
	}
}
