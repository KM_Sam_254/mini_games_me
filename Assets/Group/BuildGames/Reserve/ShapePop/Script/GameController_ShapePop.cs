﻿

public class GameController_ShapePop : GameController {
	
	public ShapePopPlay gameScript;

	public override void SetGame()
	{
		base.SetGame();
		GameManager.Instance.OnClientGameReady();
	}

	public override void StartGame()
	{
		hasGameStarted = true;
	}

	protected override void RegistGameStartProcedure()
	{
		// Register tasks to be done before the game started
	}

	void Start ()
	{
		SetGame();
	}

	public override void HomeBtnClicked()
	{
		SceneMgr.Instance.GoGroupMenu();
	}

	protected override void SetDifficulty(int difficulty)
	{
		Debug.Log("Set difficulty : " + difficulty);
		this.difficulty = difficulty;
		gameScript.Difficulty = difficulty;
	}

	protected override void Next()
	{
		gameScript.Next();
	}

	protected override void Verify()
	{
		gameScript.Verify();
	}
}
