﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RodPlay : MonoBehaviour {

	public Rod[] rods;
	public Transform[] pos;
	public Image sign;
	public Sprite[] sp;
    int q1, q2;
	private int maxNo = 6;
	private int minNo = 1;

    int difficulty = 0;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    public GameController_RodEqu gameController;

    void Start()
	{
		AddNo ();
	}

	void AddNo()
	{
		maxNo = 5 + Difficulty * 5;
		minNo = 2 + Difficulty * 5;
	}

	public void Next()
	{
        gameController.SetNextInteractable(false);
        AddNo();
		HideRod ();
		StartCoroutine(AssignRod ());
        iTween.MoveTo(rods[2].gameObject, new Vector3(rods[2].gameObject.transform.position.x,
            2000f, 0f), 1f);
	}

	IEnumerator AssignRod()
	{
		for (int i = 0; i < rods.Length - 1; i++) {
			int r = Random.Range (minNo, maxNo - i);
			rods [i].rodNo = r;
			rods [i].ShowRod ();
			yield return new WaitForSeconds (1f);
		}
		sign.enabled = true;
		if (rods[0].rodNo < rods[1].rodNo ||
			rods[0].rodNo == rods[1].rodNo) {
			sign.sprite = sp [0];
		}else {
			sign.sprite = sp [1];
		}
        gameController.SetVerifyInteractable(true);
	}

    public void Verify()
    {
        gameController.SetVerifyInteractable(false);
        gameController.SetNextInteractable(true);
        iTween.MoveTo(rods[2].gameObject, new Vector3(rods[2].gameObject.transform.position.x,
                600f, 0f), 0.5f);
        if (sign.sprite == sp[0])
        {
			rods [2].rodNo = rods [0].rodNo + rods [1].rodNo;
        }
        else
        {
			rods [2].rodNo = rods [0].rodNo - rods [1].rodNo;
        }
        rods[2].ShowRod();
    }


	void HideRod()
	{
		rods [0].HideRod ();
		rods [1].HideRod ();
        rods [2].HideRod();
		sign.enabled = false;
	}
}
