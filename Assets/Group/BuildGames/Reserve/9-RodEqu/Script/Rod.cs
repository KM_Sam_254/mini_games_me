﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rod : MonoBehaviour {

	private Image[] rods;
    public Image ten;
	public Image twenty;
    private Text txt;
	public int rodNo;

	void Start()
	{
        txt = GameObject.Find("Ans").GetComponent<Text>();
		rods = new Image[10];
		for (int i = 0; i < rods.Length; i++) {
			rods [i] = this.gameObject.transform.GetChild (i).GetComponent<Image> ();
		}
	}

	public void ShowRod()
	{
		rods [rodNo % 10].enabled = true;
        iTween.PunchScale(rods[rodNo % 10].gameObject, new Vector3(1f, 1f, 0f), 1f);

		if (rodNo / 10 == 1)
        {
            ten.enabled = true;
            iTween.PunchScale(ten.gameObject, new Vector3(1f, 1f, 0f), 1f);
        }
		else if (rodNo / 10 == 2) {
			ten.enabled = true;
			twenty.enabled = true;
			iTween.PunchScale(ten.gameObject, new Vector3(1f, 1f, 0f), 1f);
			iTween.PunchScale(twenty.gameObject, new Vector3(1f, 1f, 0f), 1f);
		}
		txt.text = rodNo.ToString();
	}

	public void HideRod()
	{
		for (int i = 0; i < rods.Length; i++) {
			rods [i].enabled = false;
            txt.text = "";
		}
        ten.enabled = false;
		twenty.enabled = false;
	}
}
