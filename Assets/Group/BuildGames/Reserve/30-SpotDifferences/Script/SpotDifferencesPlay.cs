﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpotDifferencesPlay : MonoBehaviour {

	public Button next;
	public Button verify;
	public Dropdown dropdown;
	public GameObject[] pictures;
	private int index;
	private int no;
	
	public void Next () 
	{
		next.interactable = false;
		dropdown.interactable = false;
		HidePreviousPicture ();
		ChangePicture ();
	}
	
	public void Verify () 
	{
		verify.interactable = false;
		StartCoroutine (PictureVerify ());
	}

	void ChangePicture()
	{
		index = no++ % pictures.Length;
		iTween.MoveFrom (pictures [index], iTween.Hash
			("y", 1500f, "easeType", "easeOutBounce", "time", 1f));
		pictures [index].SetActive (true);	
		verify.interactable = true;
	}

	IEnumerator PictureVerify()
	{
		for (int i = 0; i < 
			pictures[index].transform.childCount; i++) 
		{
			pictures [index].transform.GetChild (i)
				.GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
			iTween.PunchScale (
				pictures [index].transform.GetChild (i).gameObject,
				new Vector3 (1f, 1f, 0f), 0.5f);
			if (i % 2 == 0) {				
				yield return new WaitForSeconds (0f);
			} else {
				yield return new WaitForSeconds (1f);
			}
		}
		next.interactable = true;
		dropdown.interactable = true;
	}

	void HidePreviousPicture()
	{
		for (int i = 0; i < pictures.Length; i++) {
			for (int ii = 0; ii <
				pictures [i].transform.childCount; ii++) {
				pictures [i].transform.GetChild (ii)
					.GetComponent<Image> ().color = new Color (1f, 1f, 1f, 0f);
				pictures [i].SetActive (false);
			}
		}
	}
}
