﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpotDifferencesAnsRing : MonoBehaviour {

	void Start()
	{
		this.GetComponent<Button> ().onClick.AddListener (ShowAns);
	}

	void ShowAns()
	{
		this.GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
		iTween.ScaleFrom (this.gameObject, iTween.Hash
			("x", 3f, "y", 3f, "easeType", "easeOutElastic", "time", 0.5f));
	}
}
