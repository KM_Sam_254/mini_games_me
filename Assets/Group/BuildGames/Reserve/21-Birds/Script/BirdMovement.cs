﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BirdMovement : MonoBehaviour {

	public Button next;
	public Button verify;
	public Dropdown dropdown;
	public GameObject[] bird;
	public List<Transform> fruitPos;
	public List<Transform> peoplePos;
	public List<int> ans;
	public string[] animName;
	public Sprite[] sitSp;
	public GameObject panel;
	public Text[] ansText;
    public GameObject shit;
	public Animator[] people;
	public Animator resetPpl;
	public int maxPos;
	private int r;
	private int p;
	public List<int> spriteAnimNo;
	private int temp;
	private bool isMove = false;

	void Start()
	{
		AddList ();
		SitPeople ();
	}

	void AddList()
	{
		fruitPos = new List<Transform>();
		GameObject fruits = GameObject.Find ("Fruits");
		for (int i = 0; i < fruits.transform.childCount; i++) {
			fruitPos.Add(fruits.transform.GetChild(i).GetComponent<Transform>());
			fruitPos[i].GetComponent<Image>().enabled = true;
		}
		peoplePos = new List<Transform>();
		GameObject pplPos = GameObject.Find ("PeoplePos");
		for (int i = 0; i < pplPos.transform.childCount; i++) {
			peoplePos.Add(pplPos.transform.GetChild(i).GetComponent<Transform>());
		}
		for (int i = 0; i < people.Length; i++) {
			people [i].GetComponent<Image> ().raycastTarget = false;
		}
	}

	public void GoToFruit()
	{	
		ResetPeople ();	
		maxPos = Random.Range (3, 5 + dropdown.value * 2);
		ans.Clear ();
		AddList ();
		StartCoroutine (Go ());
		panel.SetActive (false);
		next.interactable = false;
		dropdown.interactable = false;
		for (int i = 0; i < ansText.Length; i++) {
			ansText [i].text = "";
		}
	}

	public void Verify()
	{
		SitPeople ();
		panel.SetActive (true);
		iTween.ScaleFrom(panel, new Vector3(0f, 0f, 0f), 1f);
		for (int i = 0; i < ans.Count; i++) {
			ansText [i].text = (ans [i] + 1).ToString();
		} 
		next.interactable = true;
		dropdown.interactable = true;
		verify.interactable = false;
	}

	IEnumerator Go()
	{
		for (int i = 0; i < maxPos; i++) {
			r = Random.Range (0, fruitPos.Count);			
			TurnBird();
			iTween.MoveTo (bird[0], iTween.Hash (
				"position", fruitPos [r].position, 
				"time", 1f,"easeType", "linear"
			));		
			float wait = Random.Range (2f, 2.5f);
			yield return new WaitForSeconds (wait);
			p = Random.Range (0, peoplePos.Count);
			fruitPos[r].GetComponent<Image>().enabled = false;	
			TurnBird();
			iTween.MoveTo (bird[0], iTween.Hash (
				"position", peoplePos [p].position, 
				"time", 1f,"easeType", "linear"
			));	

			yield return new WaitForSeconds (2f);	
			isMove = false;
			people [p].GetComponent<Image> ().raycastTarget = true;
			shit.SetActive(true);
			iTween.MoveFrom (shit, iTween.Hash (
				"y", 1000f, 
				"time", 0.5f,"easeType", "linear"
			));

			yield return new WaitForSeconds(0.6f);
			people [p].GetComponent<Image> ().raycastTarget = false;
			shit.SetActive(false);
			if (isMove) {
				ans.Add (p);
			}
			else {
				LeavePeople (p);
			}
			fruitPos.RemoveAt (r);

			yield return new WaitForSeconds (2f);

		}
		verify.interactable = true;
	}

	public void MovePeople(int t)
	{
		iTween.MoveAdd(people[t].gameObject, iTween.Hash (
			"y", -70f,
			"time", 0.2f,"easeType", "linear",
			"oncomplete", "BackPeople", "oncompletetarget", this.gameObject
		));
		isMove = true;
		people [t].GetComponent<Image> ().raycastTarget = false;
	}

	void BackPeople()
	{
		iTween.MoveAdd(people[p].gameObject, iTween.Hash (
			"y", 70f,
			"time", 0.2f,"easeType", "linear"
		));
	}

	void SitPeople()
	{
		spriteAnimNo.Clear ();
		for (int i = 0; i < people.Length; i++) {
			people [i].enabled = false;
			int rnd = Random.Range (0, people.Length);
			people [i].GetComponent<Image> ().sprite = sitSp [rnd];
			spriteAnimNo.Add (rnd);
		}
	}

	void LeavePeople(int p)
	{
		resetPpl.enabled = false;
		people [p].enabled = true;
		people [p].Play (animName [spriteAnimNo[p]]);
		iTween.MoveAdd (people[p].gameObject, iTween.Hash (
			"y", -800f,
			"time", 1.5f,"easeType", "linear",
			"oncomplete", "StopAnim", "oncompletetarget", this.gameObject
		));
	}

	void StopAnim()
	{
		people [p].enabled = false;
		iTween.MoveAdd (people[p].gameObject, iTween.Hash (
			"y", 800f,
			"time", 0f,"easeType", "linear", "delay", 1f
		));
		people [p].GetComponent<Image> ().sprite = sitSp [spriteAnimNo[p]];
	}

	void ResetPeople()
	{
		resetPpl.enabled = true;
		resetPpl.Play ("");
	}
	
	void TurnBird()
	{
		int t = Random.Range (0, 2);
		if(t == 0)
		{
			iTween.ScaleTo(bird[0], new Vector3(-1f, 1f, 0f), 0f);
		}
		else{
			iTween.ScaleTo(bird[0], new Vector3(1f, 1f, 0f), 0f);
		}	
	}
}
