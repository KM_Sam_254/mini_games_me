﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorWordPlay : MonoBehaviour {

    public Image word;
    public Image[] wordsAns;
    public Sprite[] sp;
    public List<int> no;
    public Button next;
    public Button verify;
    public Dropdown dropdown;

    public void Next()
    {
        next.interactable = false;
        verify.interactable = true;
        dropdown.interactable = false;
        word.gameObject.SetActive(true);
        StartCoroutine(ShowWord());
        HideAns();
    }

    public void Verify()
    {
        next.interactable = true;
        verify.interactable = false;
        dropdown.interactable = true;
        word.gameObject.SetActive(false);
        ShowAns();
    }

    IEnumerator ShowWord()
    {
        for (int i = 0; i < dropdown.value + 3; i++)
        {
            int rand = Random.Range(0, sp.Length);
            word.sprite = sp[rand];
            no.Add(rand);
            iTween.ScaleFrom(word.gameObject, new Vector3(0f, 0f, 0f), 1f);
            yield return new WaitForSeconds(1f);
            iTween.PunchScale(word.gameObject, new Vector3(1f, 1f, 1f), 0.5f);
            yield return new WaitForSeconds(0.5f);
        }
    }

    void HideAns()
    {
        for (int i = 0; i < wordsAns.Length; i++)
        {
            wordsAns[i].gameObject.SetActive(false);
        }
    }

    void ShowAns()
    {
        for (int i = 0; i < dropdown.value + 3; i++)
        {
            wordsAns[i].gameObject.SetActive(true);
            wordsAns[i].sprite = sp[no[i]];
        }
    }

   

}
