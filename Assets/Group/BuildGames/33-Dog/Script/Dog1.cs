﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog1 : MonoBehaviour
{
    Animator animator;
    AudioSource audioSource;
    public AudioClip audioClip_Bark;

    void Start ()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    public void BarkDog()
    {
        animator.SetTrigger("bark");
        audioSource.PlayOneShot(audioClip_Bark);
    }
}
