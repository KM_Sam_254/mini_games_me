﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DogPlay : MonoBehaviour {

	public Dog1[] dog;
	public Image[] number;
	public Color[] color;
	private List<int> no;
	private List<int> check;
	private bool isCheck;

    int difficulty = 0;
    public int Difficulty { get { return difficulty; } set { difficulty = value; } }

    public GameController_Dog gameController;

    public GameObject myDog;

    public void Next()
	{
        no = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };
        check = new List<int>();
        check.Clear();
        isCheck = false;
        StartCoroutine(CheckColor());
        StartCoroutine(GameStart());
        gameController.SetNextInteractable(false);
    }

	public void Verify()
	{
        gameController.SetVerifyInteractable(false);
		StartCoroutine(CheckColor ());
	}

	IEnumerator CheckColor()
	{
		if (isCheck) {
			for (int i = 0; i < Difficulty + 3; i++) {
				number [check [i]].color = color [1];
				yield return new WaitForSeconds (1f);
			}
            gameController.SetNextInteractable(true);
        } else {
			for (int i = 0; i < 8; i++) {
				number [i].color = color [0];
			}
		}
	}

	void AddListNBark()
	{		
		int rnd = Random.Range (0, no.Count);
		dog [no[rnd]].BarkDog ();
		check.Add (no [rnd]);
		no.RemoveAt (rnd);
	}

	IEnumerator GameStart()
	{
		for (int i = 0; i < Difficulty + 3; i++) {
			AddListNBark ();
			yield return new WaitForSeconds(1f);
		}
        yield return new WaitForSeconds(3f);
        for (int i = 0; i < Difficulty + 3; i++) {
            dog [check[i]].BarkDog ();
            yield return new WaitForSeconds(1f);
        }
		no.Clear ();
		isCheck = true;
        gameController.SetVerifyInteractable(true);
	}
}
