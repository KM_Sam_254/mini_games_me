﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class GameController_Dog : GameController
{

    public DogPlay gameScript;

    void Start()
    {
        SetGame();
    }

    public override void SetGame()
    {
        base.SetGame();

        GameManager.Instance.OnClientGameReady();
    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    public override void StartGame()
    {
        hasGameStarted = true;
    }

    protected override void RegistGameStartProcedure()
    {
        // Register task that have to be excuted before the game start
    }
    
    protected override void Next()
    {
        StartCoroutine(StartNext());
    }

    IEnumerator StartNext()
    {
        yield return new WaitForSeconds(1f);
        gameScript.Next();
    }

    protected override void Verify()
    {
        gameScript.Verify();
    }

    protected override void SetDifficulty(int difficulty)
    {
        Debug.Log("Set difficulty : " + difficulty);
        this.difficulty = difficulty;
        gameScript.Difficulty = difficulty;
    }
}
