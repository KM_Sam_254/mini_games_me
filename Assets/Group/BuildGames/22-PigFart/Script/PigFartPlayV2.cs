﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PigFartPlayV2 : MonoBehaviour {

	public int fartCount;
	public int hitCount;
	public int pigCount;
	public GameObject next;
	public GameObject verify;
	public GameObject diff;
	public Animator pigVerify;
    public GameObject panelVerify;
	public List<Transform> wayPos;
	public List<int> no;
	public PigWalk[] pigWalk;
	public Text total_txt;
	public Text hit_txt;

    void Start()
    {
        AddList();
    }

    public void AddList()
    {
		wayPos = new List<Transform> ();
		no = new List<int> ();
		GameObject pos = GameObject.Find ("Pos");
		for (int i = 0; i < pos.transform.childCount; i++) {
			no.Add (i);
			wayPos.Add(pos.transform.GetChild(i).GetComponent<Transform>());
		}
        panelVerify.SetActive(false);
    }

    public void Next()
    {
		next.SetActive (false);
		diff.SetActive (false);
		verify.SetActive (true);
        AddList();
		Debug.Log("Dropdown value : " + pigCount + "  pigLength : " + pigWalk.Length);
        //for (int i = 0; i < Difficulty + 3; i++) {
        //	pigWalk [i].gameObject.SetActive (true);
        //	pigWalk [i].GoNextPos ();
        //}
        //dropdown.interactable = false;

        GetPigs();        
		fartCount = 0;
		hitCount = 0;
    }

    void GetPigs()
    {
        for (int i = 0; i < pigCount; i++)
        {
            pigWalk[i].gameObject.SetActive(true);
            pigWalk[i].GoNextPos();
        }
    }

    public void Verify()
    {
		next.SetActive (true);
		diff.SetActive (true);
		verify.SetActive (false);
		panelVerify.SetActive (true);
		total_txt.text = fartCount.ToString ();
		hit_txt.text = hitCount.ToString ();
		PigStop ();
    }

	public void TouchFartPig(int touch)
	{
		pigWalk [touch].GetComponent<Image> ().raycastTarget = false;
		if (pigWalk[touch].isFarting == true) {
			iTween.PunchScale (pigWalk [touch].gameObject,
				new Vector3 (1f, 1f, 0f), 0.5f);
			hitCount++;
		}
		if (pigWalk[touch].isFarting == false) {
			iTween.PunchScale (pigWalk [touch].gameObject,
				new Vector3 (1f, 1f, 0f), 0.5f);
		}
	}

	void PigStop()
	{
		for (int i = 0; i < pigWalk.Length; i++) {
			pigWalk [i].gameObject.SetActive (false);
		}
	}
}
