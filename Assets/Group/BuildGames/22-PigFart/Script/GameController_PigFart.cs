﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController_PigFart : GameController
{
    public PigFartPlayV2 gameScript;

    public override void SetGame()
    {
        base.SetGame();

        GameManager.Instance.OnClientGameReady();
    }

    protected override void RegistGameStartProcedure()
    {
        // Register tasks to be done before the game started
    }

    void Start ()
    {
        SetGame();
    }

    public override void StartGame()
    {
        Debug.Log("Start Game");
        hasGameStarted = true;
    }

    public override void HomeBtnClicked()
    {
        SceneMgr.Instance.GoGroupMenu();
    }

    protected override void Next()
    {
        gameScript.Next();
    }

    protected override void Verify()
    {
        gameScript.Verify();
    }

    protected override void SetDifficulty(int difficulty)
    {
        Debug.Log("Set pigScript difficulty : " + difficulty);
        this.difficulty = difficulty;
		gameScript.pigCount = difficulty;
    }
}
