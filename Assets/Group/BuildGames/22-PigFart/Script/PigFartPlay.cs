﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PigFartPlay : MonoBehaviour {

    public Button next;
    public Button verify;
    public Dropdown dropdown;
	public Animator[] pig;
	public Animator pigVerify;
    public GameObject[] number;
    public GameObject panelVerify;
    public List<int> no;
    public List<int> ans;

    void Start()
    {
        AddList();
    }

    void AddList()
    {
        no = new List<int>();
        ans.Clear();
        for (int i = 0; i < pig.Length; i++)
        {
            no.Add(i);
			pig[i].enabled = false;
            number[i].SetActive(false);
        }
        panelVerify.SetActive(false);
    }

    public void Next()
    {
        next.interactable = false;
        AddList();
        dropdown.interactable = false;
        StartCoroutine(PigJump());
    }

    public void Verify()
    {
        verify.interactable = false;
        StartCoroutine(PigVerify());
    }

    IEnumerator PigJump()
    {
        for (int i = 0; i <= (dropdown.value * 3) + 3; i++)
        {
            int rand = Random.Range(0, no.Count);
			pig [no [rand]].enabled = true;
			pig[no[rand]].Play("PigFartLoop");
            ans.Add(no[rand]);  
			yield return new WaitForSeconds(1f);
			pig [no [rand]].enabled = false;
			no.RemoveAt(rand);
			yield return new WaitForSeconds(1f); 
        }
        verify.interactable = true;
    }

    IEnumerator PigVerify()
    {
        panelVerify.SetActive(true);
		pigVerify.Play ("PigFartLoop");
        iTween.ScaleFrom(panelVerify, new Vector3(0f, 0f, 0f), 0.5f);
        for (int i = 0; i < ans.Count; i++)
        {
            number[ans[i]].SetActive(true);
            iTween.ScaleFrom(number[ans[i]], new Vector3(0f, 0f, 0f), 0.5f);
            yield return new WaitForSeconds(1f);
        }
        next.interactable = true;
        dropdown.interactable = true;
    }
}
