﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PigWalk : MonoBehaviour {

	public PigFartPlayV2 play;
	private int fartMode;
	public Animator anim;
	public bool isFarting;
	public int curntPos;
	private int prevPos;

	public void GoNextPos()
	{
		if (play.no.Count < 1) {
			play.AddList();
		}
		int rnd = Random.Range (0, play.no.Count);
		curntPos = play.no [rnd];
		if (prevPos > curntPos) {
			this.transform.localScale = new Vector2 (-1f, 1f);
			this.transform.GetChild (0).gameObject.transform.localScale =
				new Vector2 (-1.2f, 1.2f);
		} else {
			this.transform.localScale = new Vector2 (1f, 1f);
			this.transform.GetChild (0).gameObject.transform.localScale =
				new Vector2 (1.2f, 1.2f);
		}

		iTween.MoveTo (this.gameObject, iTween.Hash (
			"position", play.wayPos [curntPos].position, "easeType", "linear",
			"speed", 250f, "oncomplete", "CheckNext", 
			"oncompetetarget", this.gameObject));		
		this.transform.SetAsLastSibling();
	//	play.wayPos [curntPos].GetComponent<Image> ().raycastTarget = true;
		play.no.RemoveAt (rnd);
	}

	void CheckNext()
	{
		isFarting = false;
		fartMode = Random.Range (0, 2);
		anim.SetInteger ("PigMode", fartMode);
		if (fartMode == 1) {
			isFarting = true;
			play.fartCount++;
			this.GetComponent<Image> ().raycastTarget = true;
		}
		prevPos = curntPos;
		GoNextPos ();
	}

	public void RunAgain()
	{		
		anim.SetInteger ("PigMode", 0);
		this.GetComponent<Image> ().raycastTarget = false;
		isFarting = false;
	}
}
