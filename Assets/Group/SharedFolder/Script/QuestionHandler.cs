﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionHandler : MonoBehaviour {

    [HideInInspector]
    public List<string> current_string;
    public List<int> q;    
    private int temp;

    public void Split(string[] mode, int index)
    {
        current_string.Clear();
        q.Clear();

        if (!string.IsNullOrEmpty(mode[index]))
        {
            current_string.AddRange(mode[index].Split('.'));
            for (int j = 0; j < current_string.Count; j++)
            {
                int.TryParse(current_string[j], out temp);
                q.Add(temp);
            }
        }
    }
}
