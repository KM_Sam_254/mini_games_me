﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuShow : MonoBehaviour {

	public GameObject fan;
	public GameObject[] category;
	public GameObject mainMenuOne;
	public GameObject mainMenuTwo;
	public GameObject logo;
	public GameObject logoMod;
	//public GameObject exitBtn;

	public void ChangeCategroy(int cat)
	{
		for (int i = 0; i < category.Length; i++)
        {
			category [i].SetActive (false);
		}

		mainMenuOne.SetActive (false);
		mainMenuTwo.SetActive (false);
		category [cat].SetActive (true);
		iTween.ScaleFrom(category[cat], new Vector3(3f, 3f, 0f), 1f);
		//exitBtn.SetActive (false);

        SceneMgr.Instance.GroupCategory = cat;
	}

	public void ShowMenu()
	{
		for (int i = 0; i < category.Length; i++) {
			category [i].SetActive (false);
		}
		mainMenuOne.SetActive (true);
		mainMenuTwo.SetActive (true);
		iTween.ScaleFrom(mainMenuOne, new Vector3(0f, 0f, 0f), 1f);
		iTween.ScaleFrom(mainMenuTwo, new Vector3(0f, 0f, 0f), 1f);
		//exitBtn.SetActive (true);
	}

	void RotateFan()
	{
		iTween.RotateAdd (fan, iTween.Hash (
			"z", -360f, "easeType", "linear", "time", 3f,
			"loopType", "loop"));
		iTween.ScaleTo (logoMod, iTween.Hash (
			"x", -1f, "easeType", "linear", "time", 3f,
			"loopType", "loop", "delay", 1f));
		iTween.MoveAdd (logo, iTween.Hash (
			"y", 50f, "easeType", "linear", "time", 3f,
			"loopType", "pingPong", "delay", 0.5f));
	}
}

