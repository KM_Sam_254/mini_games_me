﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SoundHandler : MonoBehaviour
{
    public AudioClip[] soundTrack;
    public AudioClip[] effect;
   // public Dictionary<string, AudioClip> soundTrack;
    public AudioSource touch;
    public AudioSource bgMusic;

    void Update()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        if (Input.GetMouseButtonUp(0))
        {
            touch.clip = effect[0];
            touch.PlayOneShot(effect[0], 0.5f);
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {  
		if (scene.name == "GameChoice" || scene.name == "Menu"  ) {
			bgMusic.clip = soundTrack[0];
			bgMusic.PlayDelayed(0.5f);
		}
        else if (scene.buildIndex <= 27)
        {            
            bgMusic.clip = soundTrack[Random.Range(1, 3)];
            bgMusic.PlayDelayed(0.5f);
        }
        else if(scene.name == "MenuOld" || scene.name == "Complete")
        {
            bgMusic.clip = soundTrack[Random.Range(3, 5)];  
            bgMusic.PlayDelayed(0.5f);
        }		
        else if (scene.buildIndex > 27) {
            bgMusic.clip = soundTrack[5];
            bgMusic.PlayDelayed(0.5f);
        }  
    }

}
