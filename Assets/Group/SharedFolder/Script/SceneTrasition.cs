﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTrasition : MonoBehaviour {
    
    public void SceneTransitionByName(string scene)
    {
        SceneManager.LoadScene(scene);
    }

	public void SceneTransitionByIndex(int buildIndex)
	{
		SceneManager.LoadScene (buildIndex);
	}

	public void Exit()
	{
		Application.Quit ();
	}
}
