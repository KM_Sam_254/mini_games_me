﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFan : MonoBehaviour {

    private void OnEnable()
    {
        iTween.RotateAdd(gameObject, iTween.Hash(
                    "z", -360f, "easeType", "linear", "time", 3f,
                    "loopType", "loop"));
    }
}
