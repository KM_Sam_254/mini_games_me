﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonalMenuController : MonoBehaviour {


    public Button[] btns;
    public Button btnExit;

    void Start()
    {
        if (btns == null || btns.Length < 1)
        {
            Debug.LogError("Buttons in Group null");
            return;
        }

        for (int i = 0; i < btns.Length; i++)
        {
            btns[i].gameObject.GetComponent<ButtonEventDelegator>().AddListener((btnName) => {
                SceneMgr.Instance.ChangeScene(btnName);
            });
        }

        btnExit.onClick.AddListener(() => {
            SceneMgr.Instance.ChangeScene("GameStartMenu");
        });
    }
}
