﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartMenuController : MonoBehaviour {

    public Button btnGroup, btnPersonal;

	void Start ()
    {

        if (btnGroup != null)
        {
            btnGroup.onClick.AddListener(() =>
            {
                SceneMgr.Instance.ChangeScene("MenuGroupEducation");
            });
        }
        else Debug.LogError("Button for Group is null");

        if (btnPersonal != null)
        {
            btnPersonal.onClick.AddListener(() =>
            {
                SceneMgr.Instance.ChangeScene("MenuPersonal");                
            });
        }
        else Debug.LogError("Button for Personal edudation is null");
	}
}
