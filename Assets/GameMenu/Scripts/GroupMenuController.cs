﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroupMenuController : MonoBehaviour {

    public Button[] btns;
    public Button btnExit;
    public MenuShow groupDetailMenu;

	void Start ()
    {
        if(btns == null || btns.Length < 1)
        {
            Debug.LogError("Buttons in Group null");
            return;
        }

        for (int i = 0; i < btns.Length; i++)
        {
            btns[i].gameObject.GetComponent<ButtonEventDelegator>().AddListener((btnName) => {
                SceneMgr.Instance.ChangeScene(btnName);
                if (btnName.Equals("PigFartNew") || btnName.Equals("Drone")
                || btnName.Equals("MagicShaffle") || btnName.Equals("PondFish2"))
                    SceneMgr.Instance.GroupCategory = -1;
            });
        }

        btnExit.onClick.AddListener(()=> {
            if (SceneMgr.Instance.GroupCategory > -1)
            {
                groupDetailMenu.ShowMenu();
                SceneMgr.Instance.GroupCategory = -1;
            }
            else SceneMgr.Instance.ChangeScene("GameStartMenu");
        });

        if (SceneMgr.Instance.GroupCategory > -1)
            groupDetailMenu.ChangeCategroy(SceneMgr.Instance.GroupCategory);
	}
    
}
